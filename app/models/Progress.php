<?php

class Progress extends \Eloquent {
	protected $softDelete=true;
  	protected $table='progress';
  	public static $unguarded=true;
  	function f_status()
  	{
  		return $this->hasOne("Status","from_status","order");
  	}
  	function to_status()
  	{
  		return $this->hasOne("Status","to_status","order");
  	}
}