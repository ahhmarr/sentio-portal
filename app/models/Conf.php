<?php

class Conf extends \Eloquent {
	protected $fillable = [];
	protected $table='config';
	protected $softDelete=true;
	public static $unguarded=true;
	use SoftDeletingTrait;
	protected $dates = ['deleted_at'];
}