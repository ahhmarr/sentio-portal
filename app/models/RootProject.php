<?php 

class RootProject extends Eloquent{
  protected $table='root_project';
  public static $unguarded=true;
  use SoftDeletingTrait;
  protected $dates = ['deleted_at'];
 }
