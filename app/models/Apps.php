<?php 

class Apps extends Eloquent{
  protected $table='app';
  protected $softDelete=true;
  public static $unguarded=true;
  use SoftDeletingTrait;
  protected $dates = ['deleted_at'];
  public function sku()
  {
  	return $this->hasMany("SkuDetails","app_id");
  }
  public function iap()
  {
  	return $this->hasMany("AppIap","app_id");
  }
  public function game()
  {
  	return $this->hasMany("GameDetails","app_id");
  }
  function rootProject()
  {
    return $this->belongsTo("RootProject","root_project_id");
  }
  public function itunes()
  {
    return $this->belongsTo("Itune","itunes_account_id");
  }
  public function status()
  {
    return $this->belongsTo("Status","app_status","order");
  }
  public function up_user()
  {
    return $this->belongsTo("User","upload_user");
  }
  //filter function

  public  static function filter($apps)
  {
    // filters starts
    $filterRP=Input::get("filterRP");
    $filterIT=Input::get("filterIT");
    $filterR=Input::get("filterR");
    $filterUpload=Input::get("filterUpload");
    $filterQueue=Input::get("filterQueue");
    $filterReQueue=Input::get("filterReQueue");
    $filterPend=Input::get("filterPending");
    $filterGrap=Input::get("filterGrap");
    $filterDev=Input::get("filterDev");
    //popover filter starts
    $GrAs=Input::get("GrAs");
    $GrCom=Input::get("GrCom");
    $AsA=Input::get("AsA");
    $AsCom=Input::get("AsCom");
    // popover filter ends
    
    //sort filters
    $fSort=Input::get("fSort");
    $sortCol=Input::get("sortCol");
    $sortOrder=Input::get("sortOrder");
    // sort filters ends
    $devFilter=Input::get("devFilter");
    $filterStatus=Input::get("filterStatus");
    $filterFrom=Input::get("filterFrom");
    $filterTo=Input::get("filterTo");
    $filterMonth=Input::get("filterMonth");
    $filterYear=Input::get("filterYear");
    $filterUploadUser=Input::get("filterUploadUser");
    $filterUploadFrom=Input::get("filterUploadFrom");
    $filterUploadTo=Input::get("filterUploadTo");
    $filterUploadMonth=Input::get("filterUploadMonth");
    $filterUploadYear=Input::get("filterUploadYear");
    $hisotryStatus=Input::get("hisotryStatus");
    $statuses=Input::get("statuses"); //multiple statuses split by :
    $upload_status=Input::get("upload_status"); //multiple statuses split by :
    if($statuses)
    {
      $st=explode(":", $statuses);
      $apps=$apps->where(function($query) use ($st)
      {
        foreach($st as $s)
        {
          $query->Orwhere("ap.app_status",$s);
        }
      });
    }
    if($upload_status)
    {
      $apps=$apps->where("ap.upload_status",$upload_status);
    }
    $filterText="";
    // v1.9 statstics dev and pending assignment showing
    // mapping of filterLabels to their respective statuses
    $filterMap=["filterPending"=>16,"filterDev"=>17,"filterMetaData"=>1,
              "filterAddApp"=>0.1,"filterUpload"=>2,"filterQueue"=>3,
              "filterReQueue"=>19,"filterAprroved"=>21,"filterFix"=>20,
              "filterRej"=>6];
    if(array_search($filterStatus, $filterMap)!==false)
    {
      $filterR=array_search($filterStatus, $filterMap);
    }
    $assignT=Input::get("assign_to_check");
    /*only add app and reupoad metadata for when itunes assignto 
    me is checked*/
    if($assignT)
      {
        $apps=$apps->where(function($query)
        {
          $query->where("ap.app_status","0.1")
                ->orWhere("ap.app_status","1");
        });
      }
    $data=array(
      'fRP'   => $filterRP,
      'fIT'   => $filterIT,
      'fStatus' => $filterStatus,
      'fMonth'  => $filterMonth,
      'fYear'   => $filterYear,
      'fFrom'   => $filterFrom,
      'fTo'   => $filterTo,
      'filterText'=> $filterText,
      'devFilter' => 0,
      'devFilterV'=> $devFilter,
      'GrAs'    => $GrAs,
      'GrCom'   => $GrCom,
      'AsA'   => $AsA,
      'AsCom'   => $AsCom,
      'filterR' => $filterR,
      'fSort'   => $fSort,
      'sortCol' => $sortCol,
      'sortOrder' => $sortOrder,
      'assignT' => $assignT,
      'hisotryStatus' => $hisotryStatus,
      'filterUploadUser' => $filterUploadUser,
      'filterUploadMonth' => $filterUploadMonth,
      'filterUploadTo' => $filterUploadTo,
      'filterUploadFrom' => $filterUploadFrom,
      'filterUploadYear'=>$filterUploadYear,
      'statuses'=>$statuses,
      'upload_status'=>$upload_status
      );

    if($filterUploadUser || $filterUploadFrom || $hisotryStatus
      || $filterUploadTo || $filterUploadMonth || $filterUploadYear  )//for specific upload 99 ;)
    {
      $aph=AppStatusHistory::select("app_id");
      $fAppID=$FappID=$x=array();
      if($hisotryStatus)
      {
        $aph=$aph->where("status",$hisotryStatus);
        $st=($hisotryStatus==99)?"Uploaded":Status::whereOrder($hisotryStatus)->pluck("status");
        $filterText.=" status : ".$st;
      }
      if($filterUploadUser) //skipping upload user contraint
      {
        $aph=$aph->where("user_id",$filterUploadUser);
        $filterText.=" User : ".User::where("id",$filterUploadUser)
                ->pluck("username");
      }
      if($filterUploadFrom)
      {
        $aph=$aph->where("updated_at",">=",$filterUploadFrom);
        $filterText.=" From : ".date("D m-d-Y h:i:s A",strtotime($filterUploadFrom));
      }
      if($filterUploadTo)
      {
        $aph=$aph->where("updated_at","<=",$filterUploadTo);
        $filterText.=" To : ".date("D m-d-Y h:i:s A",strtotime($filterUploadTo));
      }
      if($filterUploadMonth)
      {
        $aph=$aph->where(DB::raw("month(updated_at)"),"=",$filterUploadMonth);
        $filterText.=" Month: ".date("M",strtotime("2014-".$filterUploadMonth."-01 0:0:0"));
      }
      if($filterUploadYear)
      {
        $aph=$aph->where(DB::raw("year(updated_at)"),"=",$filterUploadYear);
        $filterText.=" Year :  $filterUploadYear";
      }
      $aph=$aph->get();
      foreach($aph as $ap)
      {
        $x[]=$ap->app_id;
      }
      /*$queries = DB::getQueryLog();
      $last_query = end($queries);*/
      $x=array_unique($x);
      // dd($last_query);
      $apps=$apps->whereIn("ap.id",$x);
    }
    if($filterRP)
      {
        $apps=$apps->where("rp.id",$filterRP);
        $filterText.="";
      }
    if($filterIT)
      {
        $apps=$apps->where("it.id",$filterIT);
        $filterText.="";
      }
    if($filterR=="filterUpload")
      {
        $apps=$apps->where("ap.app_status","2");
        $filterText.="";
      }
    if($filterR=="filterQueue")
      {
        $apps=$apps->where("ap.app_status","3")
                ->where("ap.assign_to","!=",0);
        $filterText.="";
      }
    elseif($filterR=="filterMetaData")
    {
      $apps=$apps->where("ap.app_status","1");
        $filterText.="";
      // $data["devFilter"]=2;
    }
    elseif($filterR=="filterAddApp")
    {
      $apps=$apps->where("ap.app_status","0.1");
        $filterText.="";
      // $data["devFilter"]=2;
    }
    elseif($filterR=="filterAddApp")
    {
      $apps=$apps->where("ap.app_status","0.1");
        $filterText.="";
      // $data["devFilter"]=2;
    }
    elseif($filterR=="filterFix")
    {
      $apps=$apps->where("ap.app_status","20");
        $filterText.="";
      // $data["devFilter"]=2;
    }
    elseif($filterR=="filterAprroved")
    {
      $apps=$apps->where("ap.app_status","21");
        $filterText.="";
      // $data["devFilter"]=2;
    }
    elseif($filterR=="filterReQueue")
    {
      $apps=$apps->where("ap.app_status","19")
                ->where("ap.assign_to","!=",0);
        $filterText.="";
      // $data["devFilter"]=2;
    }
    elseif($filterR=="filterPending")
      {
        $apps=$apps->where("ap.app_status","16");
          $filterText.="";
        $data["devFilter"]=2;
      }
    elseif($filterR=="filterGrap")
      {
        $apps=$apps->where("ap.app_status","18");
          $filterText.="";
        $data["devFilter"]=2;
      }
    // v1.8
      elseif($filterR=="filterRej")
      {
        $apps=$apps->where("ap.app_status","6");
          $filterText.="";
      }
      elseif($filterR=="filterRev")
      {
        $apps=$apps->where("ap.app_status","4");
          $filterText.="";
      }
      elseif($filterR=="filterGraphic")
      {
        $apps=$apps->where(function($query)
        {
          $query->where("ap.app_status","3")
              ->orWhere("ap.app_status","19");

        })->where("ap.upload_status","2");
          $filterText.="";
      }
      elseif($filterR=="filterFlight")
      {
        $apps=$apps->where(function($query)
        {
          $query->where("ap.app_status","3")
              ->orWhere("ap.app_status","19");

        })->where("ap.upload_status","3");
      }
      elseif($filterR=="filterUpl")
      {
        $apps=$apps->where(function($query)
        {
          $query->where("ap.app_status","3")
              ->orWhere("ap.app_status","19");

        })->where("ap.upload_status","4");
      }
      elseif($filterR=="filterIntegrate") //not impl yet 
      {
        $apps=$apps->where(function($query)
        {
          $query->where("ap.app_status","3")
              ->orWhere("ap.app_status","19");

        })->where("ap.upload_status","1");
          $filterText.="";
      }
    elseif($filterR=="filterDev" || Auth::user()->account_type==6)
      {
        
        $apps=$apps->where("ap.app_status","17");
          $filterText.="";
        $data["devFilter"]=1;
        //more dev parameters
        if($GrAs==1)
        {
          $apps=$apps->where("ap.gd_assign","1");
        }
        elseif($GrAs==2)
        {
          $apps=$apps->where("ap.gd_assign","!=","1");
        }
        if($GrCom==1)
        {
          $apps=$apps->where("ap.graphic_assign","1");
        }
        elseif($GrCom==2)
        {
          $apps=$apps->where("ap.graphic_assign","0")
                ->where("ap.gd_assign","1");
        }
        if($AsA==1)
        {
          $apps=$apps->where("ap.resch_assigned","1");
        }
        elseif($AsA==2)
        {
          $apps=$apps->where("ap.resch_assigned","!=","1");
        }
        if($AsCom==1)
        {
          $apps=$apps->where("ap.app_info_complete","1");
        }
        elseif($AsCom==2)
        {
          $apps=$apps->where("ap.resch_assigned","1")
                ->where("ap.app_info_complete","0");
        }
              
      }

    if($filterStatus!="")
      {
        $apps=$apps->where("ap.app_status",$filterStatus);
        $filterText.=" Status: ".Status::whereOrder($filterStatus)->pluck("status");
      }
    if($filterFrom)
      {
        $apps=$apps->where("ap.status_updated_at",">=",$filterFrom);
        $filterText.=" From : ".date("D m-d-Y h:i:s A",strtotime($filterFrom));
      }
    if($filterTo)
      {
        $apps=$apps->where("ap.status_updated_at","<=",$filterTo);
        $filterText.=" To :  ".date("D m-d-Y h:i:s A",strtotime($filterTo));
      }
    if($filterMonth)
      {
        $apps=$apps->where(DB::raw("MONTH(ap.status_updated_at)"),"=","$filterMonth");
        $filterText.=" Month: ".date("M",strtotime("2014-".$filterMonth."-01 0:0:0"));
      }
    if($filterYear)
      {
        $apps=$apps->where(DB::raw("Year(ap.status_updated_at)"),"=","$filterYear");
        $filterText.=" Year : $filterYear ";
      }
    //sorting starts
      if($fSort && $sortCol && $sortOrder)
      {
        // if more than one sort cols
        $cl=explode(",", $sortCol);
        foreach($cl as $value)
        {
          $apps=$apps->orderBy($value,$sortOrder);
        }
      }
    Debugbar::addMessage($data);
    return array("object"=>$apps,"FilterParams"=>$data);
  }
 }
