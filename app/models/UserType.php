<?php 
class UserType extends Eloquent{
  protected $softDelete=true;
  protected $table='user_type';
  public static $unguarded=true;
  public function user()
  {
  	return $this->belongsTo("User");
  }
 }
