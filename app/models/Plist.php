<?php

class Plist extends \Eloquent {
	protected $fillable = ["file_name","ip","location","info","user_id"];
	protected $table="plist_logs";
	public function user()
	{
		return $this->belongsTo("User","user_id");
	}
}