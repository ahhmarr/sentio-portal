<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Filesystem\Filesystem;

class debugToggle extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'debugToggle';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'this will toggle app debug flag.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$appPath=app_path().'/config/app.php';
		$arg=$this->input->getOptions()["flag"];
		$debug=!Config::get("app.debug");
		if($arg)
			{
				$debug=($arg==="false")?false:true;
				$this->info($debug);
			}
		$val=($debug)?"false":"true";
		$change=($debug)?"true":"false";
		$file=new Filesystem;
		$content=$file->get($appPath);
		$this->comment((bool)$arg);
		preg_match("/'debug'[ ]*=>[ ]*(true|false|TRUE|FALSE)/",$content,$match);
		if(!isset($match[0]))
		{
			return $this->error("check the app config file first");
		}
		else
		{
			$content=str_replace($match[0], "'debug' => $change", $content);
			$this->comment("Debug set to ".$change);
			$file=new Filesystem;
			$file->put($appPath,$content);
		}
		
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('flag', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('flag', null, InputOption::VALUE_OPTIONAL, 'flag value ie true or false.', null),
		);
	}

}