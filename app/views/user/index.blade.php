@extends("master")
@section("content")

<script type="text/javascript">
	$(function(){
		$(".upload-user").click(function()
		{
			url='user/uploadUser';
			data={
				uploadID  		: $(this).parent().attr("data-userID")
			};
			console.log(url);
			console.log(data);
			$.ajax({method:'post',data:data,beforeSend:function(){
				$("#uploadItunes").html('<img src="img/loading/3.gif" class="col-xs-offset-5 loading-image">');
			},url:url,success:function(resp){
				console.log(resp);
/*				$("#uploadItunes").html(resp);
				$("#uploadItunes").slideDown("slow");*/
				$("#uploadUserBody").html(resp);
				$("#uploadUserModal").modal('show');
			}});
		})
	})
</script>
	<h2 class="capitalCase col-xs-offset-4 ">user accounts</h2>
	<div class="row top-buffer-large">
		<div class="col-xs-9">
			<div class="row">
				
					{{Form::open(array("url"=>"user/create"))}}
						<div class="col-xs-2">
							<select name="user_type" id="" class="axBlank form-control">
								<option value="">user type</option>
								@foreach($userType as $u)
									<option value="{{$u->type}}">{{$u->name}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-xs-2">
							<input type="text" name="username" id="" placeholder="user name" class="axBlank form-control">
						</div>
						<div class="col-xs-2">
							<input type="password" name="password" id="" placeholder="password" class="axBlank form-control">
						</div>
						<div class="col-xs-2">
							<input type="checkbox" name="rec_email" id="rec_email" placeholder="rec_email" class="hidden-x" value="1">
							<label for="rec_email" class="checkLabel col-xs-12">Recieve Email</label>
						</div>
						<div class="col-xs-3">
							<input type="email" name="email" id="" placeholder="email" class="form-control">
						</div>
						
						<input type="submit" value="submit" class="btn btn-default hide-submit">
					{{Form::close()}}
			</div>
			<div class="row strong border-bottom capitalCase top-buffer-large padding-small">
				<div class="col-xs-3">username</div>
				<div class="col-xs-2">account type</div>
				<div class="col-xs-2">email</div>
				<div class="col-xs-2 text-center">receieve email</div>
				<div class="col-xs-2">action</div>
			</div>
			@foreach($usr as $u)
			<div class="row top-buffer-small cursor-pointer" data-userID="{{$u->id}}">
				<div class="col-xs-3 {{($u->account_type==2)?' upload-user':''}}">{{$u->username}}</div>
				<div class="col-xs-2 {{($u->account_type==2)?' upload-user':''}}">
					{{$u->name}}
				</div>
				<div class="col-xs-2">{{$u->email}}</div>
				<div class="col-xs-2 text-center">{{($u->rec_email)?"Yes":"No"}}</div>
				<div class="col-xs-2">
					<div class="btn-group noPrint ">
						<button class="btn btn-info"  data-toggle="modal" data-target="#modal{{$u->id}}">
							<span class="glyphicon glyphicon-log-in"></span>
							 edit 
						</button>
						<button class="btn dropdown-toggle btn-info" data-toggle="dropdown">
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li>
								<a href="user/delete/{{$u->id}}" class="confirm">
								<span class="glyphicon glyphicon-remove-circle ui-text-red"></span>
									delete
								</a>
							</li>
						</ul>
					</div>
				</div>
				<!-- modal start -->
				<div class="modal fade" id="modal{{$u->id}}">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				        <h3 class="modal-title">Edit {{$u->username}}</h3>
				      </div>
				       {{Form::open(array('url'=>"user/update","class"=>"form-horizontal"))}}
				       	<input type="hidden" name="userID" value="{{$u->id}}">
					      <div class="modal-body">
								<div class="form-group">
									<label for="" class="col-xs-3 control-label">username</label>
									<div class="col-xs-6">
										<input type="text" name="username" id="" class="axBlank form-control" value="{{$u->username}}">
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-xs-3 control-label">password</label>
									<div class="col-xs-6">
										<input type="password" name="password" id="" placeholder="password" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-xs-3 control-label">user type</label>
									<div class="col-xs-6">
										<select name="account_type" id="" class="axBlank form-control">
											<option value="">user type</option>
											@foreach($userType as $ut)
												<option value="{{$ut->type}}" 
												{{($ut->type==$u->account_type)?'selected="selected"':''}}>
													{{$ut->name}}
												</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<input type="checkbox" name="rec_email" id="rec_email{{$u->id}}" placeholder="rec_email" class="hidden-x" {{($u->rec_email)?"checked":""}} value="1">
									<label for="rec_email{{$u->id}}" class="checkLabel col-xs-3 col-xs-offset-3">Recieve Email</label>
								</div>
								<div class="form-group">
									<label for="email_{{$u->id}}" class="control-label col-xs-3">email</label>
									<div class="col-xs-6">
										<input type="email" name="email" id="email_{{$u->id}}" placeholder="email" class="form-control"
										value="{{$u->email}}">
									</div>
								</div>
					      </div>
					      <div class="modal-footer">
					        <button type="submit" class="btn btn-primary">submit</button>
					        <button type="reset" class="btn btn-danger">reset</button>
					        <button type="button" class="btn btn-warning" data-dismiss="modal">close</button>
					      </div>
				       {{Form::close()}}
				    </div><!-- /.modal-content -->
				  </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
				<!-- modal ends -->
			</div>
			@endforeach
		</div>
		<div class="col-xs-6 well top-buffer-largest hidden-x" id="uploadItunes" class=""></div>
	</div>
	<!-- modal start -->
	<div class="modal fade capitalCase" id="uploadUserModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body" id="uploadUserBody">
				</div>
			</div>
		</div>
	</div>
	<!-- modal ends -->
@stop