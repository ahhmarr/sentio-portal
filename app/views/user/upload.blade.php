<script type="text/javascript">
	$(function(){
		$(".change-upload").change(function()
		{
			val=$(this).val();
			patt=/^([0-9a-zA-Z]+([_.-]?[0-9a-zA-Z]+)*@[0-9a-zA-Z]+[0-9,a-z,A-Z,.,-]*(.){1}[a-zA-Z]{2,4})+$/g;
			if(!patt.test(val) && val!="")
			{
				alertify.error("Invalid email address");
				return ;
			}
			data={
				itunesID  	: $(this).attr("data-itunesID"),
				uploadID    : $(this).attr("data-uploadID"),
				email		: val
			};
			url='user/changeUploadUSer';
			console.log(url);
			console.log(data);
			$.ajax({method:'post',data:data,url:url,success:function(resp){
				alertify.success("email updated");
				
			}});
		})
	});
</script>
<h3 class="capitalCase col-xs-offset-3 padding-small">{{$usr->username}} <span class="smallCase">i</span>Tunes email</h3>
<div class="row top-buffer-small strong capitalCase col-xs-offset-3 border-bottom">
	<div class="col-xs-4">itunes account</div>
	<div class="col-xs-5">email</div>
</div>
<div class="top-buffer-large"></div>
@foreach($up as $u)
	<div class="row top-buffer-small col-xs-offset-3">
		<div class="col-xs-4">{{$u->account}}</div>
		<div class="col-xs-5">
			<input type="email" class="form-control change-upload" 
			 value="{{$u->email}}" data-itunesID="{{$u->id}}" data-uploadID="{{$usr->id}}">
		</div>
	</div>
@endforeach
