<h2>Metadata Missing of the following Apps</h2>
@foreach($appErr as $ap)
<table>
	<tr>
		<td>root project</td>
		<td>{{$ap->project_name}}</td>
	</tr>
	<tr>
		<td>iTunes Account</td>
		<td>{{$ap->account}}</td>
	</tr>
	<tr>
		<td>App Name</td>
		<td>{{$ap->app_name}}</td>
	</tr>
	<tr>
		<td>Apple ID</td>
		<td>{{$ap->apple_id}}</td>
	</tr>
	<tr>
		<td>App URL</td>
		<td>{{$ap->itunes_url}}</td>
	</tr>
</table>
<br> <hr>
@endforeach