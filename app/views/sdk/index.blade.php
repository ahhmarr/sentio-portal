@extends("master")
@section("content")
<script type="text/javascript">
	$(function(){
		$("#import").change(function(ev)
		{
			patt=/(.xls|.xlsx)$/;
			val=$(this).val();
			if(!patt.test(val))
				{
					alertify.error("Only excel file allowed");
					return;
				}
			$(this).closest('form').submit();
				
			
			
		});
		$("#rootProject").change(function()
		{
			$("#import").removeAttr("disabled");
			$("label[for='import']").removeClass("disabled");
			val=$(this).val();
			if(!val)
				{
					$("label[for='import']").addClass("disabled");
					$("#import").attr("disabled","disabled");
					return;
				}
			url='sdk/rpXL';
			data={
				'rootProject':val
			};
			console.log(url);
			console.log(data);
			$.ajax({method:'post',beforeSend:function()
			{
				$("#sdkResponse").html('<img id="loadingSave" class="col-xs-offset-5 loading-image" src="{{asset("img/loading/3.gif")}}" />');
			},data:data,url:url,success:function(resp){
				$("#sdkResponse").html(resp);
			}});
		})
	})
</script>
	{{Form::open(array("url"=>"sdk/import","files"=>true))}}
	<div class="row top-buffer-large">
		<div class="col-xs-2 col-xs-offset-4">
			<select name="rootProject" id="rootProject" class="form-control">
			<option value="">root project</option>
				@foreach($rp as $r)
					<option value="{{$r->id}}" 
					{{($selectRP==$r->id)?'selected="selected"':''}}>{{$r->project_name}}</option>
				@endforeach
			</select>
		</div>
		<div class="col-xs-2">
			<label for="import" class="btn btn-success disabled">import</label>
			<input  name="import"  type="file" class="hide-submit" id="import" disabled="disabled"> <!--  type="file" --> 
		</div>
	</div>
	{{Form::close()}}
	{{HTML::script("js/rshny.min.js")}}
	{{HTML::script("js/sdk.table.js")}}
	<div id="sdkResponse" class="row top-buffer-large overflow-auto">
	@if(isset($sdkResponse))
		@include("sdk.table",array('sdkResponse'=>$sdkResponse,'head'=>$head,'sdk'=>$sdk))
	@endif
	</div>
@stop