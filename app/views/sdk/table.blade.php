<table border="1" id="importTable" class="font-smallest">
	<thead>
		<tr>
			<th>SDK App Name</th>
			@foreach($head as $h)
				<th>{{$h}}</th>
			@endforeach
		</tr>
	</thead>
	<tbody>
		@foreach ($sdk as $s)
			<tr>
				<td>{{$s}}</td>
				@foreach($head as $h)
					<td data-id="{{$id[$h][$s]}}" 
					class="importTableCell">{{$sdkResponse[$h][$s]}}</td>
				@endforeach
			</tr>
		@endforeach
	</tbody>
</table>
