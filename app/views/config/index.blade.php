@extends("master")
@section("content")
{{HTML::script("js/config.js")}}
	<h2 class="text-center">Account Coloring</h2>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-12">
					<!-- root project -->
					<input type="radio" class="hidden-x" name="config" id="rootProject" value="1" {{$config->account_coloring==1?"checked":""}}>
					<label class="checkLabel form-control text-center capitalCase padding-tiny" for="rootProject">Root Project</label>
				</div>
				<div class="col-xs-12">
					<!-- account specific	 -->
					<input type="radio" class="hidden-x" name="config" id="accountSpecific" value="2" {{$config->account_coloring==2?"checked":""}}>
					<label class="checkLabel form-control text-center capitalCase padding-tiny" for="accountSpecific">Account Specific</label>
				</div>
			</div>
		</div>
	</div>
@stop