@extends("master")
@section("content")
<script type="text/javascript">
	$(function(){
		
	})
</script>
	<h2 class="capitalCase col-xs-offset-4 ">status</h2>
	<div class="row top-buffer-large">
		<div class="col-xs-6">
			<div class="row">
					{{Form::open(array("url"=>"status/store"))}}
						<div class="col-xs-3">
							<input type="text" name="status" placeholder="status name" class="axBlank form-control">
						</div>
						<div class="col-xs-3">
							<input type="text" name="color" id="textColor"  placeholder="color" class="axBlank form-control color">
						</div>
						<div class="col-xs-3">
							<input type="text" name="order"  placeholder="order" class="axBlank form-control">
						</div>
						
						<input type="submit" value="submit" class="btn btn-default hide-submit">
					{{Form::close()}}
			</div>
			<div class="row strong border-bottom capitalCase top-buffer-large padding-small">
				<div class="col-xs-1">order</div>
				<div class="col-xs-2">col. order</div>
				<div class="col-xs-3">color</div>
				<div class="col-xs-3">status</div>
				<div class="col-xs-3">delete</div>
			</div>
			@foreach($st as $u)
			<div class="row top-buffer-small cursor-pointer" data-userID="{{$u->id}}">
				<div class="col-xs-1">{{$u->order}}</div>
				<div class="col-xs-2">{{$u->color_order}}</div>
				<div class="col-xs-3">{{$u->status}}</div>
				<div class="col-xs-3" style="background-color:{{$u->color}};">&nbsp;</div>
				<div class="col-xs-3">
					<a href="status/delete/{{$u->id}}" class="confirm">
						<span class="glyphicon glyphicon-remove-circle ui-text-red"></span>
						delete
					</a>
				</div>
			</div>
			@endforeach
		</div>
	</div>
@stop