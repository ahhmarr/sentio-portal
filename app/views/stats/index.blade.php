@extends("master")
@section("content")
{{HTML::script("js/filter.js")}}
<div class="hidden-x">
@include("app.filterForm")
</div>
<h1 class="text-center">Statistics Tab</h2>
<h2 class="capitalCase">currently</h2>
<div class="col-xs-12">
	<table class="table capitalCase">
	<?php $currently=[17,16,"0.1",2,3,19,4,21,6,22] ?>
	@foreach($currently as $x)
		<tr>
			<td class="width-40pc">{{$status[$x]}}</td>
			<td data-status="{{$x}}" {{($x==16)?"data-filterdevFilter='1'":""}}>
			{{isset($list[$x]["allTime"])?"<span class='filterClass'>".$list[$x]["allTime"]."</span>":0}}
			</td>
			<!-- print reupload metadata -->
			@if($x=="0.1")
				<td>{{$status[1]}}</td>
				<td data-status="{{1}}">
				{{isset($list[1]["allTime"])?"<span class='filterClass'>".$list[1]["allTime"]."</span>":0}}
				</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			@elseif($x==21)
				<td>{{$status[20]}}</td>
				<td data-status="{{20}}">
				{{isset($list[20]["allTime"])?"<span class='filterClass'>".$list[20]["allTime"]."</span>":0}}
				</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			@elseif($x==3 || $x==19)
				<!-- upload statuses -->
				<?php $upd=($x==3)?"_upload":"_reupload"; ?>
				<?php $currently=["us_1","us_3","us_4","us_2"] ?>
				@foreach($currently as $c)
					<td>{{$status[$c]}}</td>
					<td data-status="{{$x}}" data-upload-status="{{str_replace('us_','',$c)}}">
					{{isset($list[$c]["allTime$upd"])?"<span class='filterClass'>".$list[$c]["allTime$upd"]."</span>":0}}
					</td>
				@endforeach	
			@else 
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			@endif
		</tr>	
	@endforeach
	<!-- upload statuses -->
	<!-- <?php $currently=["us_2","us_3"] ?>
	@foreach($currently as $x)
		<tr>
			<td class="width-40pc">{{$status[$x]}}</td>
			<td data-statuses="19:3" data-upload-status="{{str_replace('us_','',$x)}}">
			{{isset($list[$x]["allTime"])?"<span class='filterClass'>".$list[$x]["allTime"]."</span>":0}}
			</td>
			<td></td>
		</tr>	
	@endforeach -->
</table>
</div>
<br>
<!-- progress tab -->
@include("app.progress",compact("progress"))
<!-- progress ends -->
<br>
<!-- upload history starts -->
<div class="col-xs-12">
	<h2>Uploaded history</h2>
	<table class="table capitalCase">
	<tr>
		<th>upload user</th>
		<th>
			today
			<i class="fa fa-info-circle ui-text-blue cursor-pointer" rel="tooltip" 
			title='{{"Today :" . date("D d-M")}}'></i>
		</th>
		<th>
			this week
			<i class="fa fa-info-circle ui-text-blue cursor-pointer" rel="tooltip" 
			title='{{date("D d-M h:i:s A",strtotime($weekStart))}} to  
			{{date("D d-M h:i:s A",strtotime($weekEnd))}}' id="blah"></i>
		</th>
		<th>
			last week
			<i class="fa fa-info-circle ui-text-blue cursor-pointer" rel="tooltip" 
			title='{{date("D d-M h:i:s A",strtotime($lastWeekStart))}} to  
			{{date("D d-M h:i:s A",strtotime($lastWeekEnd))}}' id="blah"></i>
		</th>
		<th>this month</th>
		<th>last month</th>
		<th>all time</th>
	</tr>
	@foreach($uploadUsers as $userID)
		<tr>
			<td>{{$username[$userID]}}</td>
			<td data-history-status="99" data-upload-user="{{$userID}}" data-upload-from="{{date('Y-m-d 00:00:00')}}" data-upload-to="{{date('Y-m-d 23:59:59')}}">
				{{el($uploadH,$userID,"today")}}
			</td>
			<td data-history-status="99" data-upload-user="{{$userID}}" data-upload-from="{{$weekStart}}" data-upload-to="{{$weekEnd}}">
				{{el($uploadH,$userID,"thisWeek")}}
			</td>
			<td data-history-status="99" data-upload-user="{{$userID}}" data-upload-from="{{$lastWeekStart}}" data-upload-to="{{$lastWeekEnd}}">
				{{el($uploadH,$userID,"lastWeek")}}
			</td>
			<td data-history-status="99" data-upload-user="{{$userID}}" data-upload-month="{{$thisMonth}}" data-upload-year="{{$thisYear}}">
				{{el($uploadH,$userID,"thisMonth")}}
			</td>
			<td data-history-status="99" data-upload-user="{{$userID}}" data-upload-month="{{$prevMonth}}" data-upload-year="{{$prevYear}}">
				{{el($uploadH,$userID,"prevMonth")}}
			</td>
			<td data-history-status="99" data-upload-user="{{$userID}}">
				{{el($uploadH,$userID,"allTime")}}
			</td>
		</tr>	
	@endforeach
	<tr class="strong ui-grey ui-text-white">
		<td>Total</td>
		<td>{{el($uploadH,"total","today")}}</td>
		<td>{{el($uploadH,"total","thisWeek")}}</td>
		<td>{{el($uploadH,"total","lastWeek")}}</td>
		<td>{{el($uploadH,"total","thisMonth")}}</td>
		<td>{{el($uploadH,"total","prevMonth")}}</td>
		<td>{{el($uploadH,"total","allTime")}}</td>
	</tr>
</table>
</div>
<!-- upload history ends -->
<br>
<!-- reupload history starts -->
<div class="col-xs-12">
	<h2>Re Uploaded history</h2>
	<table class="table capitalCase">
	<tr>
		<th>upload user</th>
		<th>
			today
			<i class="fa fa-info-circle ui-text-blue cursor-pointer" rel="tooltip" 
			title='{{"Today :" . date("D d-M")}}'></i>
		</th>
		<th>
			this week
			<i class="fa fa-info-circle ui-text-blue cursor-pointer" rel="tooltip" 
			title='{{date("D d-M h:i:s A",strtotime($weekStart))}} to  
			{{date("D d-M h:i:s A",strtotime($weekEnd))}}' id="blah"></i>
		</th>
		<th>
			last week
			<i class="fa fa-info-circle ui-text-blue cursor-pointer" rel="tooltip" 
			title='{{date("D d-M h:i:s A",strtotime($lastWeekStart))}} to  
			{{date("D d-M h:i:s A",strtotime($lastWeekEnd))}}' id="blah"></i>
		</th>
		<th>this month</th>
		<th>last month</th>
		<th>all time</th>
	</tr>
	@foreach($ruploadUsers as $userID)
		<tr>
			<td>{{$rusername[$userID]}}</td>
			<td data-history-status="98" data-upload-user="{{$userID}}" data-upload-from="{{date('Y-m-d 00:00:00')}}" data-upload-to="{{date('Y-m-d 23:59:59')}}">
				{{el($ruploadH,$userID,"today")}}
			</td>
			<td data-history-status="98" data-upload-user="{{$userID}}" data-upload-from="{{$weekStart}}" data-upload-to="{{$weekEnd}}">
				{{isset($ruploadH[$userID]["thisWeek"])?"<span class='filterClass'>".$ruploadH[$userID]["thisWeek"]."</span>":0}}
			</td>
			<td data-history-status="98" data-upload-user="{{$userID}}" data-upload-from="{{$lastWeekStart}}" data-upload-to="{{$lastWeekEnd}}">
				{{isset($ruploadH[$userID]["lastWeek"])?"<span class='filterClass'>".$ruploadH[$userID]["lastWeek"]."</span>":0}}
			</td>
			<td data-history-status="98" data-upload-user="{{$userID}}" data-upload-month="{{$thisMonth}}" data-upload-year="{{$thisYear}}">
				{{isset($ruploadH[$userID]["thisMonth"])?"<span class='filterClass'>".$ruploadH[$userID]["thisMonth"]."</span>":0}}
			</td>
			<td data-history-status="98" data-upload-user="{{$userID}}" data-upload-month="{{$prevMonth}}" data-upload-year="{{$prevYear}}">
				{{isset($ruploadH[$userID]["prevMonth"])?"<span class='filterClass'>".$ruploadH[$userID]["prevMonth"]."</span>":"0"}}
			</td>
			<td data-history-status="98" data-upload-user="{{$userID}}">
				{{isset($ruploadH[$userID]["allTime"])?"<span class='filterClass'>".$ruploadH[$userID]["allTime"]."</span>":0}}
			</td>
		</tr>	
	@endforeach
	<tr class="strong ui-grey ui-text-white">
			<td>Total</td>
			<td>{{el($ruploadH,"total","today")}}</td>
			<td>{{el($ruploadH,"total","thisWeek")}}</td>
			<td>{{el($ruploadH,"total","lastWeek")}}</td>
			<td>{{el($ruploadH,"total","thisMonth")}}</td>
			<td>{{el($ruploadH,"total","lastMonth")}}</td>
			<td>{{el($ruploadH,"total","allTime")}}</td>
	</tr>
</table>
</div>
<!-- re upload history ends -->
<br><br>
<h2 class="capitalCase">all time</h2>
<table class="table capitalCase">
	<tr>
		<th></th>
		
		<th>this week
		<i class="fa fa-info-circle ui-text-blue cursor-pointer" rel="tooltip" 
		title='{{date("D d-M h:i:s A",strtotime($weekStart))}} to  
		{{date("D d-M h:i:s A",strtotime($weekEnd))}}' id="blah"></i>
		</th>
		<th>
			last week
			<i class="fa fa-info-circle ui-text-blue cursor-pointer" rel="tooltip" 
			title='{{date("D d-M h:i:s A",strtotime($lastWeekStart))}} to  
			{{date("D d-M h:i:s A",strtotime($lastWeekEnd))}}' id="blah"></i>
		</th>
		<th>this month</th>
		<th>last month</th>
		<th>all time</th>
	</tr>
	{{Debugbar::addMessage($list)}}
	@foreach([17,16,"0.1",1,2,99,98,4,21] as $x)
		<tr>
			<td>
				@if($x==99)
					Uploaded
				@elseif($x==98)
					Re Uploaded
				@else
					{{$status[$x]}}
				@endif
			</td>
			<td>{{printStatsCells($listH,$x,"thisWeek",$timeDetails)}}</td>
			<td>{{printStatsCells($listH,$x,"lastWeek",$timeDetails)}}</td>
			<td>{{printStatsCells($listH,$x,"thisMonth",$timeDetails)}}</td>
			<td>{{printStatsCells($listH,$x,"lastMonth",$timeDetails)}}</td>
			<td>{{printStatsCells($listH,$x,"allTime",$timeDetails)}}</td>
		</tr>	
	@endforeach
</table>
@stop