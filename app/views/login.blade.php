@extends("master")
@section("content")
    @if(Session::has('message'))
    	<div class="error rounded-all">{{Session::get('message')}}</div>
    @endif
	<div class="row top-buffer-largest">
		<div class="container col-xs-offset-3 top-buffer-largest">
			{{ Form::open(array('url'=>'login','role'=>'form','class'=>'form-horizontal')) }}
			<div class="form-group">
				<label for="username" class="col-xs-2">username</label>
				<div class="col-xs-2">
					<input type="text" name="username" class="form-control" id="username" autofocus>
				</div>
			</div>
			<div class="form-group">
				<label for="password" class="col-xs-2">password</label>
				<div class="col-xs-2">
					<input class="form-control" type="password" name="password" id="password">
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-2 col-xs-offset-2">
					<input type="submit" name="" id="submit" class="btn btn-primary">
				</div>

			</div>
		{{Form::close()}}
		</div>
	</div>
@stop