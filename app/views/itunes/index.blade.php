@extends("master")
@section("content")
	<script type="text/javascript">
	$(function(){
		$("#plus").click(function()
		{
			$("#itunesForm").slideToggle();
		})
	})
	$(function(){
		$("input[type^='radio']").change(function()
		{
			name=$(this).attr("name");
			val=$("input[name='"+name+"']:checked").val();
			id=$(this).attr("data-id");
			type=$(this).attr("data-type");
			url='itunes/fullToggle';
			data={
				id 		: id,
				flag	: val,
				type    : type
			};

			console.log(url);
			console.log(data);
			$.ajax({
					method			:'post',
					data			:data,
					beforeSend		:function()
									{
										al=alertify.log('saving ....');	
									},
					url				:url,
					success			:function(resp)
									{
										$("article").hide();
										alertify.success('saved <i class="fa fa-check"></i>');
									}
				});
		})
	})
	</script>
	
<?php $sn=0; ?>
@if(Session::has("message"))
<div class="error rounded-all">
	{{Session::get("message")}}
</div>
@endif
	<h2 class="col-xs-offset-5"><span class="smallCase">i</span>Tunes account</h2>
	<table class="table top-buffer-large">
		<thead class="capitalCase">
			<tr>
				<th>account no</th>
				<th>account</th>
				<th>bundle prefix</th>
				<th>IAP</th>
				<th>full</th>
				<th>app store notice</th>
				<th>created on</th>
				<th>delete</th>
			</tr>
		</thead>
		<tbody>
		
		@foreach($list as $l)
			<tr>
				<td>{{++$sn}}</td>
				<td>{{$l->account}}</td>
				<td class="noneCase">{{$l->bundlePrefix}}</td>
				<td>
					
					<input type="radio" class="hide-submit" value="1" {{($l->iap_inherit)?'checked="checked"':''}} name="iap{{$l->id}}" 
					data-type="iap" data-id="{{$l->id}}" id="iapY{{$l->id}}">
					<label class="checkLabel capitalCase padding-tiny" for="iapY{{$l->id}}">yes</label>
					<input type="radio" class="hide-submit" value="0" name="iap{{$l->id}}" {{(!$l->iap_inherit)?'checked="checked"':''}} 
					data-type="iap" data-id="{{$l->id}}" id="iapN{{$l->id}}">
					<label class="checkLabel capitalCase padding-tiny" for="iapN{{$l->id}}">no</label>
				</td>
				<td>
					
					<input type="radio" class="hide-submit" value="1" {{($l->full)?'checked="checked"':''}} name="full{{$l->id}}" 
					data-type="full" data-id="{{$l->id}}" id="itY{{$l->id}}">
					<label class="checkLabel capitalCase padding-tiny" for="itY{{$l->id}}">yes</label>
					<input type="radio" class="hide-submit" value="0" name="full{{$l->id}}" {{(!$l->full)?'checked="checked"':''}} 
					data-type="full" data-id="{{$l->id}}" id="itN{{$l->id}}">
					<label class="checkLabel capitalCase padding-tiny" for="itN{{$l->id}}">no</label>
				</td>
				<td>
					<!-- app store notice -->
					<input type="radio" class="hide-submit" value="1" {{($l->app_notice)?'checked="checked"':''}} name="appNotice{{$l->id}}" 
					data-type="app_notice" data-id="{{$l->id}}" id="appNoticeY{{$l->id}}">
					<label class="checkLabel capitalCase padding-tiny" for="appNoticeY{{$l->id}}">yes</label>
					<input type="radio" class="hide-submit" value="0" name="appNotice{{$l->id}}" {{(!$l->app_notice)?'checked="checked"':''}} 
					data-type="app_notice" data-id="{{$l->id}}" id="appNoticeN{{$l->id}}">
					<label class="checkLabel capitalCase padding-tiny" for="appNoticeN{{$l->id}}">no</label>
				</td>
				<td>{{$l->created_at}}</td>
				<td>
					<a class="confirm" href="{{URL::to('itunes/rem/'.$l->id)}}">
						<i class=" ui-text-red fa fa-minus-circle"></i>
					</a>
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
	{{Form::open(array('url'=>'itunes/save','class'=>'border-bottom'))}}
		<input type="text" name="account" id="newAccount" 
		class="border-none axBlank" placeholder="&lt; new account &gt;" />
		<input type="text" name="_xrtx_bundlePrefix_xrtx" id="newBundle" 
		class="border-none axBlank" placeholder="&lt; new bundle prefix &gt;" />
		<input type="submit" value="submit" class="hide-submit" />

	{{Form::close()}}
@stop