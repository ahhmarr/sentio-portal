@extends("master")
@section("content")
{{HTML::script("js/rootProject.js")}}

	<div class="row top-buffer-small">
		@include("root.projectList",compact('list'))
		<div class="col-xs-10">
			<!-- Nav tabs -->
			@if($proj)
			<ul class="nav nav-tabs capitalCase">
				  <li class="{{($active)?'active':''}}"><a href="#detials" data-toggle="tab">details</a></li>
				  <li><a href="#report" data-toggle="tab">report</a></li>
			</ul>
			@endif
			<!-- Tab panes -->
			<div class="tab-content">
			  <div class="tab-pane active" id="detials">
			  	@if($active)
					<h2>{{($active->project_name)?$active->project_name:''}}</h2>
					<div class="row capitalCase">
						<div class="col-xs-4">game center</div>
						<div class="col-xs-4 col-xs-offset-2">
							
							<input type="radio" {{($proj->game_center)?"checked":''}} name="gameCenter" class="gameCenter" value="1">
							<label for="gameCenterY">yes</label>
							
							<input type="radio" {{(!$proj->game_center)?'checked':''}} name="gameCenter" class="gameCenter" value="0">
							<label for="gameCenterN">no</label>
						</div>
					</div>
					<div class="row capitalCase">
						<div class="col-xs-4">IAP</div>
						<div class="col-xs-4 col-xs-offset-2">
							
							<input type="radio" name="iap" id="iapY" value="1"
							{{($proj->iap)?'checked':''}}>
							<label for="iapY">yes</label>
							<input type="radio" name="iap" id="iapN" value="0"
							{{(!$proj->iap)?'checked':''}}>
							<label for="iapN">no</label>
						</div>
					</div>
						<div class="row capitalCase strong top-buffer-large iap-rows border-bottom {{(!$proj->iap)?'hidden-x':''}}">
							<div class="col-xs-2">type</div>
							<div class="col-xs-4">product ID</div>
							<div class="col-xs-2">price tier</div>
							<div class="col-xs-2">display name</div>
							<div class="col-xs-2">description</div>
						</div>
					@if($iap && $proj->iap )
						@foreach($iap as $i)
							<div class="row border-bottom top-buffer-small padding-small iap-rows">
								<div class="col-xs-2">{{($i->type)?'Non-Consumable':'Consumable'}}</div>
								<div class="col-xs-4">{{$i->product_id}}</div>
								<div class="col-xs-2">{{$i->price_tier}}</div>
								<div class="col-xs-2">{{$i->display_name}}</div>
								<div class="col-xs-2">
									{{$i->description}}
									<span class="pull-right">
										<a href="{{Url::to('iap/delete/'.$i->id.'/'.$active->id)}}">
											<i class="fa fa-times-circle-o"></i>
										</a>
									</span>
								</div>
							</div>
						@endforeach
					@endif
						<div class="row iap-rows padding-small {{(!$proj->iap)?'hidden-x':''}}">
						{{Form::open(array('url'=>'iap/save','class'=>'placeholder-blue'))}}
						 <input type="hidden" name="projectID" id="" value="{{$active->id}}">

							<div class="col-xs-2">
								<input type="text" name="iapType" 
								placeholder="type" class="axBlank border-none iapType" id="">
							</div>
							<div class="col-xs-4">
								<input type="text" name="iapProdID" 
								placeholder="product ID" class="axBlank border-none iapProdID" id="">
							</div>
							<div class="col-xs-2">
								<input type="text" name="iapPriceTier" 
								placeholder="price tier" class="axBlank border-none iapPriceTier" id="">
							</div>
							<div class="col-xs-2">
								<input type="text" name="iapDisplayName" 
								placeholder="display name" class="axBlank border-none iapDisplayName" id="">
							</div>
							<div class="col-xs-2">
								<input type="text" name="iapDescription" 
								placeholder="description" class="axBlank border-none iapDescription" id="">
																
							</div>
							<input type="submit" value="save" class="hide-submit">
						{{Form::close()}}
					</div>
				<script type="text/javascript">
					$(function(){
						$("input[name='facebook'],input[name='next_peer'],input[name='primary']")
								.click(function()
						{
							var th=$(this);
							name=th.attr("name");
							flag=th.val();
							projID=$('input[name="projectID"]').val();
							url='toggle';
							data={
								type:name,
								flag:flag,
								projID:projID
							};
							console.log(url);
							console.log(data);
							$.ajax({method:'post',data:data,url:url,success:function(resp){
								console.log(resp);
								
							}});
						});
					})
				</script>
					
					<div class="row top-buffer-largest capitalCase">
						<div class="col-xs-4">free and pro version</div>
						<div class="col-xs-4 col-xs-offset-2">
							<input type="radio" name="freePro" id="freeProY" value="1" {{($active->free_n_pro)?'checked="checked"':''}}>
							<label for="freeProY">yes</label>
							<input type="radio" name="freePro" id="freeProN" value="0" {{(!$active->free_n_pro)?'checked="checked"':''}}>
							<label for="freeProN">no</label>
						</div>
					</div>
					<!-- facebook flag -->
					<div class="row top-buffer-largest capitalCase">
						<div class="col-xs-4">facebook</div>
						<div class="col-xs-4 col-xs-offset-2">
							<input type="radio" name="facebook" id="fbY" value="1" {{($active->facebook)?'checked="checked"':''}}>
							<label for="fbY">yes</label>
							<input type="radio" name="facebook" id="fbN" value="0" {{(!$active->facebook)?'checked="checked"':''}}>
							<label for="fbN">no</label>
						</div>
					</div>
					<!-- next peer flag -->
					<div class="row top-buffer-largest capitalCase">
						<div class="col-xs-4">next peer</div>
						<div class="col-xs-4 col-xs-offset-2">
							<input type="radio" name="next_peer" id="nextY" value="1" {{($active->next_peer)?'checked="checked"':''}}>
							<label for="nextY">yes</label>
							<input type="radio" name="next_peer" id="nextN" value="0" {{(!$active->next_peer)?'checked="checked"':''}}>
							<label for="nextN">no</label>
						</div>
					</div>

					<!-- primary flag -->
					<div class="row top-buffer-largest capitalCase">
						<div class="col-xs-4">primary</div>
						<div class="col-xs-4 col-xs-offset-2">
							<input type="radio" name="primary" id="primaryY" value="1" {{($active->primary)?'checked="checked"':''}}>
							<label for="primaryY">yes</label>
							<input type="radio" name="primary" id="primaryN" value="0" {{(!$active->primary)?'checked="checked"':''}}>
							<label for="primaryN">no</label>
						</div>
					</div>

					<!-- plist file upload -->
					<div class="row top-buffer-largest capitalCase">
						<div class="col-xs-4 top-buffer-large">reUpload plist</div>
						<div class="col-xs-4 col-xs-offset-2">
							<img src="{{asset('img/loading/3.gif')}}" alt="" height="10" id="loading" class="invisible">
							<input type="file" name="primary" id="plist" data-pid="{{$active->id}}" class="{{($active->plist_path)?'hidden-xx':''}} form-control">
							<div id="downloadLink" class="{{(!$active->plist_path)?'hidden-xx':''}}">
								<div class="col-xs-8">
									<a class="form-control" href="{{$active->plist_path}}" download id="downloadPlist">{{$active->plist_name}}</a>
								</div>
								<button class="btn btn-small ui-red" rel="tooltip" title="" id="removePlist" data-pid="{{$active->id}}" data-original-title="remove file">
									<i class="fa  fa-times ui-text-black"></i>
								</button>
							<!-- <i id="removePlist" data-pid="{{$active->id}}"  class="fa fa-times ui-text-red"></i> -->
							</div>
						</div>
						 
					</div>

				@endif
			  </div>
			  <div class="tab-pane" id="report">
			  	
					@if($active)
					<h2>{{$active->project_name}}</h2>

						<div class="btn-group col-xs-2">
							<button class="pull-right" id="setColor">
							<i class="fa  fa-bitbucket"></i>
							</button>
							<input type="text" name="" id="textColor" class="pull-right color">
							<button class="pull-right" id="cancelColor">
							<i class="fa  fa-arrows-alt"></i>
						</button>
						</div>
						
						
						
						<table id="rootReportTable" data-projectID="{{$proj->id}}" class="table table-bordered excel-table">
							
							<tr>
								@for($y=1;$y<=$proj->cols;$y++)
								<td>
									<button class="col-add">
										<i class="fa fa-plus"></i>
									</button>
									<button class="col-remove">
										<i class="fa fa-minus"></i>	
									</button>
									
								</td>
								@endfor
								<td></td>
							</tr>
							@for($x=1;$x<=$proj->rows;$x++)
								<tr>
									@for($y=1;$y<=$proj->cols;$y++)
									<td>
										<input type="text" class="border-none cell-box" 
										value="{{@$cell[$x.''.$y]}}" style="{{@$style[$x.''.$y]}}" />
									</td>
									
									@endfor
										<td class="border-none width-5pc">
											<button class="row-add">
												<i class="fa fa-plus"></i>
											</button>
											<button class="row-remove">
												<i class="fa fa-minus"></i>	
											</button>
										</td>
								</tr>
							@endfor
						</table>
					@endif
					
					<!-- edit text box begin -->
					{{HTML::style("css/redactor.css")}}
					{{HTML::script("js/redactor.min.js")}}
							<script type="text/javascript">
								$(document).ready(function(){
										$('#reportMessage').redactor({
											minHeight	: 200,
											changeCallback: function(html)
												{
													$("#reportMessage").val(html);
													$("#reportMessage").change();
												}
										});
									}
								);
								$(function(){
									$("#reportMessage").change(function()
									{
										url='updateRootText';
										data={
											value  : $(this).val(),
											projID : {{isset($proj->id)?$proj->id:0}}
										};
										console.log(url);
										console.log(data);
										$.ajax({method:'post',data:data,url:url,success:function(resp){
											console.log(resp);
											
										}});
									})
								})
						</script>
						@if(isset($proj->id))
							@include("root.globalUploadUser",compact("uploadUsers"))
						@endif
						<div class="clearfix"></div>
					  	<div class="row top-buffer-large">
					  		<textarea name="" class="form-control" id="reportMessage" cols="30" rows="40">{{isset($proj->report_text)?$proj->report_text:''}}</textarea>
					  	</div>
					<!-- edit text box end -->
			  </div>
			</div>
		</div>
	</div>
@stop