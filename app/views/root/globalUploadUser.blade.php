<script type="text/javascript">
	$(function(){
		$("#globalUploadUser").change(function()
		{
			var val=$(this).val();
			var rpID=$(this).data("rpid");
			url='updateFields/'+rpID;
			data={
				field : 'default_user',
				val   : val	
			};
			console.log(url);
			console.log(data);
			$.ajax({method:'post',data:data,url:url,success:function(resp){
				console.log(resp);
			}});
		});
	});
</script>
<div class="col-xs-12">
	<div class="col-xs-3 text-right">
		<label for="globalUploadUser">global Upload User</label>
	</div>
	<div class="col-xs-3">
		<select data-rpID="{{$proj->id}}" name="globalUploadUser" id="globalUploadUser" class="form-control">
			<option value="" {{(!$proj->default_user)?"selected":""}}>All Users</option>
			@foreach($uploadUsers as $user)
				<option value="{{$user->id}}"
				{{($proj->default_user==$user->id)?"selected":""}}>{{$user->username}}</option>
			@endforeach
		</select>
	</div>
	<div class="col-xs-1">
		<!-- <input type="button" class="btn btn-primary" value="Assign User"> -->
	</div>
</div>