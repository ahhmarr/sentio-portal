<div class="col-xs-2 container">
	<div id="toggleForm">
	<button class="btn btn-default pull-right">
		+
	</button>
	</div>
	<div class="clearfix"></div>
	<div id="formProject" class="hidden-x">
		{{Form::open(array('url'=>'root/save'))}}
			<input type="text" name="rootProjectName" id="" 
					placeholder="new project" class=" axBlank form-control top-buffer-small">
			<input type="submit" value="save" class="btn btn-primary hidden-x">
		{{Form::close()}}
	</div>
	<?php $sn=0; ?>
	@foreach($list as $l)
		<?php $color=(++$sn%2==1)?'light-blue ':''; ?>
		<div class="ui-{{$color}} col-xs-12 padding-small top-buffer-small">
			<a href="{{URL::to('/root').'/'.$l->id}}">{{$l->project_name}}</a>
			<span class="pull-right">
				<a class="confirm" href="{{URL::to('/root/disableProject').'/'.$l->id}}">
					<i class="fa fa-minus-circle ui-text-red"></i>
				</a
				<a href="{{URL::to('/root/disableProject').'/'.$l->id}}">
					<i class="fa fa-edit editName cursor-pointer"></i>
				</a>					
			</span>
			<div class="hidden-x">
				{{Form::open(array('url'=>"root/ChangeProjectName"))}}
					<input type="hidden" name="id" value="{{$l->id}}">
					<input type="text" name="name" value="{{$l->project_name}}" 
					placeholder="new project" class=" axBlank form-control top-buffer-small">
					<input type="submit" value="save" class="btn btn-primary hidden-x">
				{{Form::close()}}
			</div>
		</div>
	@endforeach
</div>