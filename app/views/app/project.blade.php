<script type="text/javascript">
	$(function(){
		$(".filterClass").click(function()
		{
			st=$(this).attr("data-status");
			it=$(this).attr("data-it");
			rp=$(this).attr("data-rp");
			$("#filterStatus").val(st);
			$("#filterIT").val(it);
			$("#filterRP").val(rp);
			$("#filter-form").submit();
			
		})
	})
</script>
{{Form::open(array("url"=>"app","class"=>"hidden-x","id"=>"filter-form"))}}
 	<input type="hidden" name="filterStatus" id="filterStatus" value="">
 	<input type="hidden" name="filterIT" id="filterIT" value="">
 	<input type="hidden" name="filterRP" id="filterRP" value="">
{{Form::close()}}
<table class='table'>
	<tr>
		<th></th>
		@foreach($it as $i)
			<th>{{$i->account}}</th>
		@endforeach
		<th>total</th>
		
	</tr>
	@foreach($status as $order=>$s)
		<tr>
			<td>{{$status[$order]["status"]}}</td>
			<?php $totx=0 ?>
			@foreach($it as $i)
				<?php $mx=isset($max[$i->id]['max'])?$max[$i->id]['max']:"0";
				$mx=((string)$mx=="0.10000000149012")?"0.1":(string)$mx;
				?>
				<td style="background-color:{{$status[$mx]['color']}}" data-mx="{{$mx}}">
					@if(isset($tot[$order][$i->id]))
						<?php $totx+=$tot[$order][$i->id] ?>
						<span class="filterClass cursor-pointer" data-status="{{$order}}"
						 data-it="{{$i->id}}" data-rp="{{$projectID}}">{{$tot[$order][$i->id]}}</span>
					@endif
				</td>
			@endforeach
			<td>{{$totx}}</td>
		</tr>
	@endforeach
</table>
