@foreach($apps as $a)
		@if(($a->app_status==17 || $a->app_status==16) 	&& ($filterR!="filterPending" || $a->app_status==17))
			<?php continue; //skipping dev nd pending assignment ?>
		@endif
	<tr class="noneCase cursor-pointer width-1pc"   data-account="{{$a->account}}">
		@if($ut==1 || $ut==5)
		<td class="width-1pc">
			<input type="checkbox" name="appID[]" data-Project="{{$a->root_project_id}}" data-appID="{{$a->id}}" class="app-toggle">
		</td>
		@endif
		<td class="app-detail app_name" data-appID="{{$a->id}}">{{$a->app_name}}</td>
		<td class="app-detail" data-appID="{{$a->id}}" class="rootProjectCol">{{$a->project_name}}</td>
		<td class="app-detail" data-appID="{{$a->id}}">{{($a->unassign)?'':$a->account}}</td>
		<td class="app-detail" data-appID="{{$a->id}}">
			<span class="pull-left">{{trim($a->bundlePrefix)}}.</span>
			<span class="app_compress pull-left">{{$a->bun}}</span>
		</td>
		<td class="app-detail" data-appID="{{$a->id}}">{{($a->priority)?$a->priority:''}}</td>
		<td class="app-detail assign-user" data-appID="{{$a->id}}">{{$a->username}}</td>
		<td class="app-detail" data-appID="{{$a->id}}">{{$a->apple_id}}</td>
		<td class="app-detail hidden-x" data-appID="{{$a->id}}">{{$a->itunes_url}}</td>
		
		<?php $clrx=((string)$a->app_status=="0.10000000149012")?"0.1":(string)$a->app_status; ?>
		<td class="app-detail" data-appID="{{$a->id}}" style="background-color:{{$status[$clrx]['color']}};!important">
			{{$status[$clrx]['status']}}
		</td>
			<td>
				@if($ut==1 || $ut==5)
					<a class="confirm pull-left" href="{{URL::to('app/del/'.$a->id)}}">
						<i class="fa fa-minus-circle ui-text-red"></i>
					</a>
				@endif
				@if($ut==1 || $ut==5 || $ut==3)
				<i data-toggle="modal" id="editButton" 
				data-appname="{{$a->app_name}}" data-appid="{{$a->id}}"
				data-rpID="{{$a->root_project_id}}" data-itID="{{$a->itunes_account_id}}" data-prty="{{$a->priority}}" data-asignTo="{{$a->assign_to}}"	data-appleID="{{$a->apple_id}}" data-itURL="{{$a->itunes_url}}"	data-pappleID="{{$a->Papple_id}}" 
				data-pitURL="{{$a->Pitunes_url}}" data-freenpro="{{$a->free_n_pro}}" class="fa fa-edit left-buffer"></i>
				@endif
			</td>
			<td>
				@if($a->is_live==2)
					<span class="ui-text-green" title="online">
						<i class="fa fa-circle"></i>
					</span>
				@elseif($a->is_live==1)
					<span class="ui-text-red" title="offline">
						<i class="fa fa-circle"></i>
					</span>
				@endif
			</td>
	</tr>
@endforeach
