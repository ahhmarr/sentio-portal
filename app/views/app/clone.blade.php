{{HTML::script("js/cloneTable.js")}}
<div class="modal fade" id="cloneModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title" id="cloneEditName"></h3>
      </div>
      <div class="modal-body">
        {{Form::open(array('url'=>'app','class'=>"form-horizontal",'role'=>"form"))}}
          <input type="hidden" name="app_name" id="cloneAppName">
          <input type="hidden" name="root_project_id" id="cloneRP">
          <input type="hidden" name="appStatus" value="0.1">
          <input type="hidden" name="assign_to" value="22">
          <input type="hidden" name="clone" value="1">
          <input type="hidden" name="cloneAppID" id="cloneAppID">
          <div class="form-group">
            <label for="editIT" class="col-xs-4 control-label">account</label>
            <div class="col-xs-6">
              <select name="itunes_account_id" id="cloneIT" class="form-control">
                @foreach($ac as $r)
                  <option value="{{$r->id}}">{{$r->account}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-3  col-xs-offset-3">
              <button type="button" class="btn form-control btn-default" data-dismiss="modal">Close</button>
            </div>
            <div class="col-xs-3">
              <input type="submit" class="btn form-control btn-warning" value="Clone App">
            </div>
          </div>
        {{Form::close()}}
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->