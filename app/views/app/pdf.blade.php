<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{$ap->app_name}}</title>
	{{HTML::style("css/pdf.css")}}
</head>
<body>
	@include("app.reportContent",compact("ap","utype","apRep","sty","id","pdf"))
	<br><br>
	{{$ap->report_text}}
	<div class="break"></div>
	{{$ap->upload_note}}
</body>
</html>