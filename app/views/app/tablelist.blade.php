<?php $rowNo=0; ?>
@foreach($apps as $a)
				@if(($a->app_status==17 || $a->app_status==16) && ($filterR!=="filterPending" || $a->app_status==17))
					<?php continue; //skippking dev nd pending assignment ?>
				@endif
			<tr class="noneCase cursor-pointer width-1pc"   data-account="{{$a->account}}">
				<td class="app-detail" data-appID="{{$a->id}}">{{++$rowNo}}</td>
				@if($ut==1 || $ut==5 || $ut==7)
				<td class="width-1pc">
					<input type="checkbox" name="appID[]" data-Project="{{$a->root_project_id}}" data-appID="{{$a->id}}" class="app-toggle">
				</td>
				@endif
				<td class="app-detail app_name" data-appID="{{$a->id}}">{{$a->app_name}}</td>
				<td class="app-detail" data-appID="{{$a->id}}" class="rootProjectCol">{{$a->project_name}}</td>
				<td class="app-detail" data-appID="{{$a->id}}">{{($a->unassign)?'':$a->account}}</td>
				<td class="app-detail" data-appID="{{$a->id}}">
					<span class="pull-left">{{trim($a->bundlePrefix)}}.</span>
					<span class="app_compress pull-left">{{$a->bun}}</span>
				</td>
				<td class="app-detail" data-appID="{{$a->id}}">{{($a->priority)?$a->priority:''}}</td>
				@if($upload_status_show)
					<td class="app-detail assign-user" data-appID="{{$a->id}}">
					 <!-- show only upload or reupload -->
						@if($a->app_status==19 || $a->app_status==3)
							{{$a->username}}
						@endif
					</td>
				@endif
				<td class="app-detail" data-appID="{{$a->id}}">{{$a->apple_id}}</td>
				<td class="app-detail hidden-x" data-appID="{{$a->id}}">{{$a->itunes_url}}</td>
				<?php $clrx=((string)$a->app_status=="0.10000000149012")?"0.1":(string)$a->app_status; ?>
				<td class="smaller-text" data-appID="{{$a->id}}" style="background-color:{{$status[$clrx]['color']}};!important">
					{{$status[$clrx]['status']}}
					@if($a->app_status==20 && $filterR=="filterFix")
						<i class="fa font-smaller pull-right fa-level-up update_app_stats" data-appid="{{$a->id}}"
						 data-name="app_status" data-val="1" data-reload="1"
						 data-status="1" title="ASO Fixed" data-assignto="22"></i>
					@endif
				</td>
				@if($upload_status_show)
				<td class="smaller-text capitalCase" data-appID="{{$a->id}}" style="background-color:{{$a->usColor}};!important">
					{{$a->upStatus}}
					<!-- only testFlight -->
					@if($a->upload_status==3) 
					 <i class="fa font-smaller pull-right fa-plane update_app_stats" data-appid="{{$a->id}}"
					 data-name="upload_status" data-val="4" data-reload="1"
					 data-status="upload status  : Upload&nbsp;"></i>
					@endif
				</td>
				@endif
					<td class="width-5pc">
						@if($ut==1 || $ut==5 || $ut==7)
							<a class="confirm pull-left"  title="delete" href="{{URL::to('app/del/'.$a->id)}}">
								<i class="fa fa-minus-circle ui-text-red"></i>
							</a>
						@endif
						@if($ut==1 || $ut==5 || $ut==3 || $ut==7)
						<i data-toggle="modal" title="edit app" id="editButton" 
						data-appname="{{$a->app_name}}" data-appid="{{$a->id}}"
						data-rpID="{{$a->root_project_id}}" data-itID="{{$a->itunes_account_id}}" data-prty="{{$a->priority}}" data-asignTo="{{$a->assign_to}}"	data-appleID="{{$a->apple_id}}" data-itURL="{{$a->itunes_url}}"	data-pappleID="{{$a->Papple_id}}" 
						data-pitURL="{{$a->Pitunes_url}}" data-freenpro="{{$a->free_n_pro}}" data-autoprocess="{{$a->auto_process}}" class="fa fa-edit left-buffer"></i>
						@endif
						@if($ut==1 || $ut==7)
							<i data-toggle="modal" data-appname="{{$a->app_name}}" data-appid="{{$a->id}}"
						data-rpID="{{$a->root_project_id}}" data-itID="{{$a->itunes_account_id}}" data-prty="{{$a->priority}}" data-asignTo="{{$a->assign_to}}"	data-appleID="{{$a->apple_id}}" data-itURL="{{$a->itunes_url}}"	data-pappleID="{{$a->Papple_id}}" 
						data-pitURL="{{$a->Pitunes_url}}" data-freenpro="{{$a->free_n_pro}}" data-autoprocess="{{$a->auto_process}}" data-appID="{{$a->id}}" title="clone app" class="cloneButton fa fa-copy left-buffer"></i>
						@endif
						
					</td>
					<td>
						@if($a->is_live==2)
							<span class="ui-text-green" title="online">
								<i class="fa fa-circle"></i>
							</span>
						@elseif($a->is_live==1)
							<span class="ui-text-red" title="offline">
								<i class="fa fa-circle"></i>
							</span>
						@endif
					</td>
						<td class="app-detail" data-appid="{{$a->id}}">
							{{last_modified($a)}}
						</td>
			</tr>
@endforeach
			