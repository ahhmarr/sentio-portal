<h2>Progress</h2>
<div class="col-xs-12">
	<table class="table table-condensed">
		<thead>
			<tr>
				<th>Status</th>
				<th></th>
				<th>T</th>
				<th>TW</th>
				<th>LW</th>
				<th>TM</th>
				<th>LM</th>
				<th>2M</th>
			</tr>
		</thead>
	<!-- v1.12 presenting graphics complete and aso complete rows -->
	@foreach($cProgress as $key=>$current )
		<tr>
			<td>{{$key}}</td>
			<td></td>
			<td>{{$current["today"]}}</td>
			<td>{{$current["thisWeek"]}}</td>
			<td>{{$current["lastWeek"]}}</td>
			<td>{{$current["thisMonth"]}}</td>
			<td>{{$current["lastMonth"]}}</td>
			<td>{{$current["lastTwoMonths"]}}</td>
		</tr>
	@endforeach
	@foreach($progress as $key=>$value)
		<tr>
			<td>{{$value["from"]}}</td>
			<td> --> {{$value["to"]}}</td>
			<td>{{$value["today"]}}</td>
			<td>{{$value["thisWeek"]}}</td>
			<td>{{$value["lastWeek"]}}</td>
			<td>{{$value["thisMonth"]}}</td>
			<td>{{$value["lastMonth"]}}</td>
			<td>{{$value["lastTwoMonths"]}}</td>
		</tr>
	@endforeach
	</table>
</div>