<script type="text/javascript">
		$(document).ready(function(){
				$('#uploadNote').redactor({
					minHeight	: 200,
					changeCallback: function(html)
						{
							$("#uploadNote").val(html);
							$("#uploadNote").change();
						}
				});
			}
		);
		$(function(){
			$("#uploadNote").change(function()
			{
				url='app/updateKeys';
				data={
					appID : $(this).attr("data-appID"),
					field : $(this).attr("name"),
					val   : $(this).val()
				};
				data.val=data.val.replace(/(http:|https:)\/\//,'');
				$(this).val(data.val);
				console.log(url);
				console.log(data);
				$.ajax({method:'post',data:data,url:url,success:function(resp){
					console.log(resp);
					
				}});
			});
			
			
		})
</script>
<div class="row top-buffer-large">
	<span class="strong">upload user notes </span>
	<a class="pull-right" href="pdf/{{$id}}" target="_blank" title="export to PDF">
		<img src="{{asset('img/pdf-icon.png')}}" height="20" hei alt="">
	</a>
	 <div class="top-buffer-small"></div>
	<textarea data-appID="{{$id}}"  class="form-control" id="uploadNote" name="upload_note" cols="30" rows="40">{{$ap->upload_note}}</textarea>
</div>