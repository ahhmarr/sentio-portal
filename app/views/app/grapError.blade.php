<script type="text/javascript">
	$(function(){
		$("#inTestFlight").click(function()
		{
			var th=$(this);
			var prev=th.data("prevval");
			if(prev!=1)
			{
				var ans=confirm("The Status is not Integrate Do you want to continue?");
				if(!ans)
					return;
			}
			$("#inTestFlight").off("click");
			th.addClass("update_app_stats").click();
		});
		$("#upApStore").click(function()
		{
			var th=$(this);
			var prev=th.data("prevval");
			console.log(prev);
			if(prev!=4) //not in upload
			{
				var ans=confirm("The App is not in Upload status. Are you Sure?");
				if(!ans)
					return;
			}
			$("#upApStore").removeAttr("id").off("click");
			th.addClass("update_app_stats").click();
		});
	})
		
</script>
<div class="col-xs-2 top-buffer-small">
	<button  class="update_app_stats btn btn-danger" data-appid="{{$appID}}"
	data-name="upload_status" data-val="2" data-status="Upload  Status : Graphic bug"
	data-reload="1">
	<i class="fa fa-bug"></i> Graphic Bug</button>
</div>
<div class="col-xs-3 top-buffer-small">
	<button id="inTestFlight" class="btn btn-warning" data-appid="{{$appID}}"
	data-name="upload_status" data-prevval="{{$ap->upload_status}}" data-val="3" data-status="Upload Status : Test Flight"
	data-reload="1" data-noconfirm="1">
	<i class="fa fa-plane"></i> Integrated to Test Flight</button>
</div>
<div class="col-xs-3 top-buffer-small">
	<button id="upApStore" class="btn btn-success" data-appid="{{$appID}}"
	data-name="app_status" data-prevval="{{$ap->upload_status}}" data-val="4" data-status="Upload Status : Upload"
	data-reload="1" data-noconfirm="1" data-assignto="NULL">
	<i class="fa fa-upload" data-clicked="0"></i> uploaded to App Store</button>
</div>