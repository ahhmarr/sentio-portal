@if($ut!=6)
	<script type="text/javascript">

		$(window).load(function()
		{
			$(".filterForm").fadeIn();
			
		})
		</script>
@endif
@if($ut==3)
	<script type="text/javascript">
	$(function(){
		var filtrR=$("input[name='filterR']:not(.forItunes)");
		filtrR.hide();
		filtrR.next().hide();
	})
	</script>
@endif
@if($devFilter==1 && ($GrAs || $GrCom|| $AsA|| $AsCom))
	<script type="text/javascript">
		$(window).load(function()
		{
			$("#popITOver").click();
			$("label[for='filterDev']").data("click","2");
			
		})
	
	</script>
@endif
<script type="text/javascript">
	$(function(){
		$("#submitFilter").click(function()
			{
				$("#filterFOrm").submit();
			});
		$("#filterFOrm input[type='radio']").change(function()
		{
			ob=$("#filterDev");
			ch=ob.is(":checked");
			//checking for the click count on the dev label
			var labelID=$(this).attr("id");
			var filLabel=$("label[for='filterDev']");
			if(labelID=="filterDev" && ch)
			{
				
				clickCount=filLabel.data("click");
				clickCount++;
				console.log(clickCount);
				if(clickCount<2)
				{
					filLabel.popover("hide");
					$(".devFl").prop("checked",false);
				}
				filLabel.data("click",clickCount);
			}
			if(!ch)
			{
				filLabel.popover("hide");
				$(".devFl").prop("checked",false);
				filLabel.data("click","0");
			}
			id=$(this).attr("id");
			// alert(id);
			nm=$(this).attr("name");
			$("input[name='"+nm+"']").each(function()
			{
				th=$(this);
				i=th.attr("id");
				$("label[for='"+i+"']").removeClass("activeLabel");
			})
			if($(this).is(":checked"))
			{
				
				$("label[for='"+id+"']").addClass("activeLabel");
			}
			
			/*else
				$("label[for='filterDev']").popover("show");*/
		});
		$(".devFl").each(function()
		{
			th=$(this);
			ch=th.is(":checked");
			id=th.attr("id");
			if(ch)
			{
              $("label[for='"+id+"']").addClass("activeLabel");
			}
		});
		$("body").on("click","#resetDevFil",function()
		{
			$(".devFl").each(function()
			{
				th=$(this);
				th.prop("checked",false);
				id=th.attr("id");
	            $("label[for='"+id+"']").removeClass("activeLabel");
			});
			// $(".popver").popover("hide");
		});
		
	})
</script>
<div class="row top-buffer-small">
	{{Form::open(array('url'=>"app","id"=>"filterFOrm","class"=>"form-inline filterForm hidden-x"))}}
			<input type="hidden" id="assign_to_check" name="assign_to_check">
			<input type="hidden" id="status_filter" name="status_filter">
		
		<!-- new filter starts -->
				
				<input class="hidden" type="radio" name="filterR" id="filterDev" {{($filterR=="filterDev")?"checked":""}} value="filterDev" />
			  	<label class="popver padding-small pointer" for="filterDev"
			  	id="popITOver"   data-placement="{{($ut=='6')?'top':'top'}}" data-container="body"	data-content=''
				data-html='true' data-click="0">Development</label>
				<input class="hidden" type="radio" name="filterR" id="filterPending" {{($filterR=="filterPending")?"checked":""}} value="filterPending" />
				<label class="padding-small pointer {{$hid}}" for="filterPending">Pending Assignment</label>
				<div class="checkbox padding-small {{$hid}}">
					<input type="radio" name="filterR" value="filterAddApp" 
						class="filterChange hidden-x forItunes" 
						id="filterAddApp"  {{($filterR=="filterAddApp")?"checked":""}}>
					<label class="padding-small pointer" for="filterAddApp">Add App</label>
				</div>
				<div class="checkbox padding-small {{$hid}}">
					<input type="radio" name="filterR" value="filterMetaData" 
						class="filterChange hidden-x forItunes" 
						id="filterMetaData"  {{($filterR=="filterMetaData")?"checked":""}}>
					<label class="padding-small pointer" for="filterMetaData">ReUpload Metadata</label>
				</div>
				<div class="checkbox padding-small {{$hid}}">
					<input type="radio" name="filterR" value="filterUpload" 
						class="filterChange hidden-x" 
						id="filterUpload"  {{($filterR=="filterUpload")?"checked='checked'":""}}>
					<label class="padding-small pointer" for="filterUpload">Waiting For Upload</label>
				</div>
				<div class="checkbox padding-small">
					<input type="radio" name="filterR" value="filterQueue" 
						class="filterChange hidden-x"
						 id="filterQueue"  {{($filterR=="filterQueue")?"checked='checked'":""}}>
					<label class="{{$hid}} padding-small pointer" for="filterQueue">
						Upload Queue
					</label>
				</div>
				<div class="checkbox padding-small">
					<input type="radio" name="filterR" value="filterReQueue" 
						class="filterChange hidden-x"
						 id="filterReQueue"  {{($filterR=="filterReQueue")?"checked='checked'":""}}>
					<label class="{{$hid}} padding-small pointer" for="filterReQueue">
						ReUpload Queue
					</label>
				</div>
				<input class="hidden" type="radio" name="filterR" id="filterGraphic" 
							 	{{($filterR=="filterGraphic")?"checked='checked'":""}} value="filterGraphic" />
				<label class="padding-small pointer" for="filterGraphic">Graphics Bug</label>

				@if($ut!=6 && $ut!=3)
				<input class="hidden" type="radio" name="filterR" id="filterFlight" 
							 	{{($filterR=="filterFlight")?"checked='checked'":""}} value="filterFlight" />
				<label class="padding-small pointer" for="filterFlight">Test Flight</label>

				<input class="hidden" type="radio" name="filterR" id="filterUpl" 
							 	{{($filterR=="filterUpl")?"checked='checked'":""}} value="filterUpl" />
				<label class="padding-small pointer" for="filterUpl">Upload</label>

				<input class="hidden" type="radio" name="filterR" id="filterRev" 
							 	{{($filterR=="filterRev")?"checked='checked'":""}} value="filterRev" />
				<label class="padding-small pointer" for="filterRev">In Review</label>

				<input class="hidden" type="radio" name="filterR" id="filterRej" 
							 	{{($filterR=="filterRej")?"checked='checked'":""}} value="filterRej" />
				<label class="padding-small pointer" for="filterRej">Rejection</label>
				<input class="hidden" type="radio" name="filterR" 
				id="filterAprroved" {{($filterR=="filterAprroved")?"checked='checked'":""}} value="filterAprroved" />
				<label class="padding-small pointer" for="filterAprroved">Approved</label>
				<input class="hidden" type="radio" name="filterR" id="filterFix" 
							 	{{($filterR=="filterFix")?"checked='checked'":""}} value="filterFix" />
				<label class="padding-small pointer" for="filterFix">Approved ASO Fix</label>
				<div class="form-group {{$hid}}" ><!-- id="customFilter" -->
			<div class="col-xs-2">
				{{$rootFilter}}
			<!-- </select> -->
			</div>
		</div>
		<div class="form-group {{$hid}}" ><!-- id="customFilter2" -->
			<select name="filterIT" id="filterIT" class="form-control filterChange">
				<option value="">Account</option>
				@foreach($account as $r)
					<option value="{{$r->id}}"  {{($r->id==$fIT)?"selected='selected'":""}}>
						{{$r->account}}
					</option>
				@endforeach
			</select>
		</div>
		<!-- <div class="form-group">
			<button id="" type="submit" class="btn btn-small"><i class="fa fa-search"></i></button>
			<a href="{{URL::to('app')}}" class="btn btn-small ui-red" rel="tooltip" title="reset" id=""><i class="fa  fa-times ui-text-black"></i></a>
		</div> -->
				
				@endif
		<div class="form-group">
			<button id="" type="submit" class="btn btn-small"><i class="fa fa-search"></i></button>
			<a href="{{URL::to('app')}}" class="btn btn-small ui-red" rel="tooltip" title="reset" id=""><i class="fa  fa-times ui-text-black"></i></a>
		</div>
		<!-- new filter ends -->
		<!-- hidden filters such as sort etc -->
		<input type="hidden" name="fSort" id="fSort">
		<input type="hidden" name="sortCol" id="sortCol">
		<input type="hidden" name="sortOrder" id="sortOrder">
		
		<!-- <button id="{{($ut!=6)?'customFilter3':''}}" type="submit" class="btn btn-small"><i class="fa fa-search"></i></button>
		<a href="{{URL::to('app')}}" class="btn btn-small ui-red" rel="tooltip" title="reset" id="{{($ut!=6)?'customFilter4':''}}"><i class="fa  fa-times ui-text-black"></i></a> -->

		<!-- dev filter radios starts-->
		<input class="devFl hidden-x" type="radio"  id="filterGAssign" 
				 	{{($GrAs=="1")?"checked='checked'":""}} name="GrAs" 
				 	value="1" />
		<input class="devFl hidden-x" type="radio"  id="filterGNAssign" 
				 	{{($GrAs=="2")?"checked='checked'":""}} name="GrAs" 
				 	value="2" />
		<input class="devFl hidden-x" type="radio"  id="filterGComplete" 
				 	{{($GrCom=="1")?"checked='checked'":""}} 
				 	name="GrCom" 
				 	value="1" />
		<input class="devFl hidden-x" type="radio"  id="filterGNComplete" 
				 	{{($GrCom=="2")?"checked='checked'":""}} 
				 	name="GrCom" 
				 	value="2" />
		<input class="devFl hidden-x" type="radio"  id="filterAsoAssign" 
				 	{{($AsA=="1")?"checked='checked'":""}} 
				 	name="AsA" value="1" />
		<input class="devFl hidden-x" type="radio"  id="filterAsoNAssign" 
				 	{{($AsA=="2")?"checked='checked'":""}} 
				 	name="AsA" value="2" />
		<input class="devFl hidden-x" type="radio"  id="filterAsoComplete" 
				 	{{($AsCom=="1")?"checked='checked'":""}} 
				 	name="AsCom" 
				 	value="1" />
		<input class="devFl hidden-x" type="radio"  id="filterAsoNComplete" 
				 	{{($AsCom=="2")?"checked='checked'":""}} name="AsCom" 
				 	value="2" />
		<!-- extra filters for statistics starts -->
		<input type="hidden" name="filterStatus" id="filterStatus" 
		value="{{sh($fStatus)}}">
	 	<input type="hidden" name="filterFrom" id="filterFrom" 
	 	value="{{sh($fFrom)}}">
	 	<input type="hidden" name="filterTo" id="filterTo" 
	 	value="{{sh($fTo)}}">
	 	<input type="hidden" name="filterMonth" id="filterMonth" 
	 	value="{{sh($fMonth)}}">
	 	<input type="hidden" name="filterYear" id="filterYear" 
	 	value="{{sh($fYear)}}">
	 	<input type="hidden" name="filterUploadUser" id="filterUploadUser" 
	 	value="{{sh($filterUploadUser)}}">
	 	<input type="hidden" name="filterUploadMonth" id="filterUploadMonth" 
	 	value="{{sh($filterUploadMonth)}}">
	 	<input type="hidden" name="filterUploadTo" id="filterUploadTo" 
	 	value="{{sh($filterUploadTo)}}">
	 	<input type="hidden" name="filterUploadFrom" id="filterUploadFrom" 
	 	value="{{sh($filterUploadFrom)}}">
	 	<input type="hidden" name="filterUploadYear" id="filterUploadYear" 
	 	value="{{sh($filterUploadYear)}}">
	 	<input type="hidden" name="hisotryStatus" id="hisotryStatus" 
	 	value="{{sh($hisotryStatus)}}">
	 	<input type="hidden" name="statuses" id="statuses" 
	 	value="{{sh($statuses)}}">
	 	<input type="hidden" name="upload_status" id="upload_status" 
	 	value="{{sh($upload_status)}}">
	 	<input type="hidden" name="devFilter" id="filterdevFilter" 
	 	value="{{sh($devFilter)}}">
		<!-- extra filter for statistics end -->
	</form>
</div>
<!-- dev filter radios starts -->
<div id="devFilter" class="hidden-x">
	
	<div class="row">

		<div class="col-xs-6">
			
			<label for="filterGAssign">Graphics Assigned</label>
		</div>
		<div class="col-xs-6">
			
			<label for="filterGNAssign">Graphics Not Assigned</label>
			<span id="resetDevFil">
				<i rel="tooltip" title="reset"
		class="fa fa-chain-broken ui-text-red col-xs-1 pull-right"></i>
			</span>
			
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			
			<label for="filterGComplete">Graphic Complete</label>
		</div>
		<div class="col-xs-6">
			
			<label for="filterGNComplete">Graphic not complete</label>
		</div>
	</div>
	<div class="row {{$hid}}">
		<div class="col-xs-6">
			
			<label 
			{{($devFilterV=="filterAsoAssign")?"class='act'":""}} 
			for="filterAsoAssign">ASO Assigned</label>
		</div>
		<div class="col-xs-6">
			
			<label for="filterAsoNAssign">ASO Not Assigned</label>
		</div>
	</div>
	<div class="row {{$hid}}">
		<div class="col-xs-6">
			
			<label for="filterAsoComplete">ASO Complete</label>
		</div>
		<div class="col-xs-6">
			
			<label for="filterAsoNComplete">ASO Not Complete</label>
		</div>
	</div>
</div>		