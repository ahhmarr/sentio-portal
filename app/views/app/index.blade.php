@extends("master")
@section("content")
<script type="text/javascript">
	$(function(){
		$("body").on('click','.app-detail',function()
		{
			appID=$(this).attr("data-appID");
			url='app/view';
			data={appID:appID};
			console.log(url);
			console.log(data);
			$.ajax({method:'post',data:data,beforeSend:function()
			{
				$("#appContent").html('<img class="col-xs-offset-6 loading-image" src="img/loading/3.gif" />');
			},url:url,success:function(resp){
				//console.log(resp);
				resp=$.trim(resp);
				$("#appContent").html(resp);
				var scrollTo=$("#details");
				if(!scrollTo.is(":visible"))
					scrollTo=$("#reports");
				$('html, body').animate({
				        scrollTop: scrollTo.offset().top-250
				    }, 1000);
				
			}});
		});
	});
	$(function(){
		$('body').on("click",".app-toggle",function()
		{
			ch=$(this).prop('checked');
			val=$(this).attr("data-appID");
			if(ch)
			{
				$(this).parent().parent().addClass("ui-green");
				$("#xlAppID").prepend('<input type="hidden" name="app[]" value="'+val+'" />');
			}
			else
			{
				$(this).parent().parent().removeClass("ui-green");
				$('input[value="'+val+'"').remove();
			}
		});
		$('body').on('click',"#xlSDK",function()
		{
			form=$(this).closest('form');
			thisHTML=$(this).clone();
			prevName='';
			app=[];
			flag=true
			$('.app-toggle:checked').each(function()
			{
				th=$(this);
				name=th.attr("data-Project");
				if(name!=prevName && prevName!='')
					{
						flag=false;

				}
				
				prevName=name;
				app.push(th.attr("data-appID"));

			});
			if(!flag)
				alert('not the same root project');
			else if(prevName=='')
				alert('nothing selected');
			else
			{
				form.html('');
				html='';
				app.forEach(function(i,e,arr)
				{
					html+='<input type="hidden" name="app[]" value="'+i+'">';
				});
				form.append(html);
				form.append(thisHTML);
				form.submit();
				//alert(html+thisHTML);
			}

		});
	});

	$(function(){
		$("#updateStatus").click(function()
		{
			status=$("#statusList").val();
			if(!status)
				{
					alertify.error("select a status first");
					return;
				}
			if(!$(".app-toggle:checked").length)
				{
					alertify.error("no app selected");
					return;
				}
			appID=[];
			$(".app-toggle:checked").each(function()
			{
				appID.push($(this).attr("data-appID"));
			});
			up="no"; //for changing to dorect upload
			if(status==4)
			{
				ans=confirm("Do you want to set the apps status to 'Uploaded' before 'In Review' ");
				if(ans)
					up="yes";
			}
			data={
				appID 	: appID,
				status  : status,
				up      : up
			};
			console.log(data);
			url='app/changeStatus';
			console.log(url);
			console.log(data);
			$.ajax({method:'post',beforeSend: function()
			{
				$("#loadingImage").show();	
			},data:data,url:url,success:function(resp){
				location.reload();
				console.log(resp);
				$("#loadingImage").hide();	
			}});
		})
	});
	$(function(){
		$("select.custom").each(function() {					
					var sb = new SelectBox({
						selectbox: $(this),
						height: 400,
						width: 150,
						rgbToHex :function(rgb)
						{
							rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
							 return (rgb && rgb.length === 4) ? "#" +
							  ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
							  ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
							  ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : '';
						},
						changeCallback:function(n,m)
						{
							
							$("input[name='redAccount']").remove();
							id=m.attr("id");
							if(id!="statusList")
							{
								rgb=m.find("option:selected").css("background-color");
								hex=(this.rgbToHex(rgb));
								// v1.11 diff message for red color
								if(hex=="#ff0102")
								{
									var ans=confirm("This account appears busy. Are you sure?")
									if(!ans)
										return;
									else
									{
										
										$("#appForm").append("<input type='hidden' name='redAccount' value='1' />");
									}
								}
								if(hex!="#26a522" && hex!="#ffffff" && hex!="#ff0102") //green or white
								{
									if(!confirm("Are you Sure ?"))
										return;
								}
								td=m.parent().parent();
								if(td.is("td"))
								{
									url='app/assignIT';
									data={
										appID : td.data("appid"),
										itID  : n
									};
									console.log(url);
									console.log(data);
									console.log('im here');
									$.ajax({method:'post',data:data,url:url,success:function(resp){
										console.log(resp);
										$("#filterFOrm").submit();
									}});
									
								}
							}

						}
					});
				});
	})
</script>
	@if(Auth::user()->account_type==1 || $ut==5 || $ut==6 || $ut==7)
		@if($ut!=6)
			<script type="text/javascript">
	$(function(){
		$("#rp_select").change(function()
		{
			id=$(this).val();
			sl=$("select[name='itunes_account_id']:not(#filterTable *)");
			sl.parent().parent().hide();
			sl.removeAttr("name");
			sl=$("select[data-projID='"+id+"']");
			sl.attr("name","itunes_account_id");
			sl.parent().parent().show();

		})
	});
	//app creation validation
	$(function(){
		$("#appForm").submit(function(e)
		{
			flag=true;
			sl=$("select[name='itunes_account_id']").val();
			rp=$("#rp_select").val();
			val=$("#new_app_name").val();
			if(!sl || !rp || !val)
				return;
			url='app/appValid';
			data={
				rp   : rp,
				it 	 : sl
			};
			console.log(url);
			console.log(data);
			$.ajax({method:'post',data:data,async:false,url:url,success:function(resp){
				if(resp.app_name) //has an app
				{
					/*alertify.confirm("An app <b>"+resp.app_name+"</b> already exists,do you want to continue ?",function(re)
					{
						if(!re)
						{
							flag=false;
						}
					})*/
					flag=confirm("An app "+resp.app_name+" already exists,do you want to continue ?");
				}
			}});
		if(!flag)
			e.preventDefault();
		})
	})
	</script>
		@endif
	
	<div class="row top-buffer-small">
		<div class="col-xs-10">
			{{Form::open(array("url"=>"app",'class'=>'','id'=>'appForm'))}}
		<div class="col-xs-2">
			{{$rp}}
		</div>
		<div class="col-xs-2">
			<!-- <span>{{$act["default"]}}</span> -->
			@foreach($act as $key=>$a)
				<span class="hidden-x">{{$a}}</span>
			@endforeach
		</div>
		<div class="col-xs-2">
			<input type="text" name="app_name" id="new_app_name" placeholder="project name" class="axBlank form-control">
		</div>
		<div class="col-xs-2">{{$pr}}</div>
		<div class="col-xs-2">
			<input type="text" name="assigned_user_id" placeholder="assigned to" class="hidden-x form-control">
		</div>
		<div class="col-xs-1">
			<input type="text" name="apple_id" placeholder="apple ID" class="hidden-x form-control">
		</div>
		<div class="col-xs-1">
			<input type="text" name="itunes_url" placeholder="iTunes URL" class="hidden-x form-control">
		</div>
		<div class="col-xs-2 {{($ut==6)?'hidden':''}}">
			<select name="appStatus" id="statusList" class="form-control custom">
				<option value="">select status</option>
				@foreach($status as $order=>$s)
					@if($order!=15)
						<option value="{{$order}}" class="ui-text-black" style="background-color:{{$status[$order]['color']}}">
							{{$status[$order]['status']}}
						</option>
					@endif
				@endforeach
			</select>
		</div>
		<div class="col-xs-2 {{($ut==6)?'hidden':''}}">
			<input type="button" id="updateStatus" 
			value="change status" class="btn btn-warning form-control">
			<img height="20" src="{{asset('img/loading/3.gif')}}" alt="" class="hidden-x" id="loadingImage">
		</div>
		<input type="submit" value="" class="hide-submit">
		{{Form::close()}}
		</div>
		<div class="col-xs-2 {{($ut==6)?'hidden':''}}">
				{{Form::open(array('url'=>'app/exportToXl','id'=>"xlAppID","class"=>"pull-right"))}}
				<input type="submit" value="APPID xls" class="btn btn-success col-xs-11 ">
			{{Form::close()}}
			{{Form::open(array('url'=>'app/exportToXlSDK',"class"=>"pull-right"))}}
				<input type="button" value="SDK xls" id="xlSDK" class="btn btn-primary col-xs-11">
			{{Form::close()}}
		</div>
	</div>
	@elseif(Auth::user()->account_type==3)
		<div class="row">
			<label for="assignToCheck">Relevent to Me</label>
			<input type="checkbox" name="assignTo" id="assignToCheck" data-userID="{{Auth::user()->name}}"
			{{($assignT)?"checked":""}}>
		</div>
	@endif
	@if(isset($apps))
		@if(Auth::user()->account_type==1 || $ut==5 || $ut==6 || $ut==7)
		<script type="text/javascript">
		$(window).load(function()
		{
			$(".filterForm,.filterBar").fadeIn();
		})
		</script>
		<div class="col-xs-10 padding-small transition ui-text-white strong  hidden-x top-buffer-small rounded-all
		text-center {{(!$filterText)?'':'filterBar'}}" style="position:absolute;background-color:#00CA44;">
			{{$filterText}}
		</div>
		@endif
		@if($ut!=2)
			<!-- filter form open -->
		<div class="col-sm-12" style="">
			@include("app.filterForm",array("root"=>$root,"account"=>$account,"fRP"=>$fRP,"fIT"=>$fIT,"filterR"=>$filterR,'GrAs'=> $GrAs,'GrCom'=> $GrCom,'AsA'=> $AsA,'AsCom'=> $AsCom,'hid'=>($ut==6)?'hidden':''))
		</div>
		<!-- filter form ends -->
		@else
			<!-- filter form open -->
		@include("app.filterFormUpload",array("root"=>$root,"account"=>$account,"fRP"=>$fRP,"fIT"=>$fIT,"filterR"=>$filterR,'GrAs'=> $GrAs,'GrCom'=> $GrCom,'AsA'=> $AsA,'AsCom'=> $AsCom,'hid'=>($ut==6)?'hidden':''))
		<!-- filter form ends -->
		@endif
		<div class="clearfix"></div>
	<div class="row">
	<script type="text/javascript">
	$(function(){
		pop=$("label[for='filterDev']");
		pop.attr("data-content",$("#devFilter").html());
	});
	</script>
	</div>
	@if($devFilter)
		@include("app.devFilter",array("apps"=>$apps,"act"=>$act))
	@elseif(!$devFilter)
		<!-- <div class="pagination pagination-centered hide-if-no-paging"></div> -->
		@include("app.mainTable")
	@endif
	@endif
	<div class="row padding-mid transition" id="appContent">
	</div>
@stop