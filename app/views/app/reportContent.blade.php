<table class="table table-bordered reportContent top-buffer-large excel-table" data-appID="{{$id}}">
	<tbody>
		@for($x=1;$x<=$ap->rows;$x++)
		<tr>
			@for($y=1;$y<=$ap->cols;$y++)
				<td>
				@if(isset($apRep[$x][$y]))
						@if(!isset($pdf))
              <input type="text" {{($utype==4)?"disabled":''}} class="border-none app-cell" value="{{$apRep[$x][$y]}}" style="{{$sty[$x][$y]}}">
            @else
              <span style="{{$sty[$x][$y]}}">{{htmlspecialchars($apRep[$x][$y])}}</span>
            @endif
				@else
          @if(!isset($pdf))
					 <input type="text" {{($utype==4)?"disabled":''}} class="border-none app-cell" value="">
          @endif
				@endif
				</td>
			@endfor
		</tr>
		@endfor
	</tbody>
</table>