@extends("master")
@section("content")
	<script type="text/javascript">
		$(function(){
			$("#tootProject").change(function()
			{
				if(!$(this).val())
					return ;
				url='project';
				data={
					projectID 	: $(this).val()
				};
				console.log(url);
				console.log(data);
				$.ajax({method:'post',data:data,beforeSend:function()
				{
					$("#appContent").html('<img src="{{asset("img/loading/3.gif")}}" class="loading-image col-xs-offset-5" /> ');
				},
				url:url,success:function(resp){
					$("#appContent").html(resp)
				}});
			})
		})
	</script>
{{HTML::script("js/filter.js")}}
@include("stats.filterForm")
	<div class="row padding-mid">
		<div class="col-xs-6 col-xs-offset-3">
		<div class="col-xs-3 capitalCase">
			<label for="">root project</label>
		</div>
		<div class="col-xs-4">
			<select name="" id="tootProject" class="form-control">
				{{$rpOption}}				
			</select>
		</div>
	</div>
	</div>
	<div class="row padding-mid" id="appContent">
	
	</div>
	<div class="col-xs-12" style="overflow:auto;">
		<table class="table">
			<thead>
				<tr>
					<th>root project</th>
					@foreach($it as $i)
						<th>{{$i->account}}</th>
					@endforeach
					<th>total</th>
				</tr>
			</thead>
			<tbody>
			<?php $totZ=0 ?>
				@foreach($rp as $r)
				<tr>
					<td>{{$r->project_name}}</td>
					<?php $totx=0 ?>
					@foreach($it as $i)
						<td  data-filterIT="{{$i->id}}"  data-filterIT="{{$i->id}}" 
						data-filterRP="{{$r->id}}" >
							@if(isset($rpH[$r->id][$i->id]))
								<?php 
									$totx+=$rpH[$r->id][$i->id];
									$totZ+=$rpH[$r->id][$i->id]; 
								?>
								<span class='filterClass'>
									{{$rpH[$r->id][$i->id]}}
								</span>
							@endif
						</td>
					@endforeach
					<td>{{$totx}}</td>
				</tr>
				@endforeach
				<tr class="strong ui-black ui-text-white">
					<td>Total</td>
					@foreach($it as $i)
						<td data-filterIT="{{$i->id}}">
						{{isset($itTot[$i->id])?
								"<span class='filterClass'>".$itTot[$i->id]."</span>":""}}
						</td>
					@endforeach
					<td>{{$totZ}}</td>
				</tr>
			</tbody>
		</table>
	</div>
@stop