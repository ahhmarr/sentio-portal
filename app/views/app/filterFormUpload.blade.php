@if($ut!=6)
	<script type="text/javascript">

		$(window).load(function()
		{
			$(".filterForm").fadeIn();
			
		})
		</script>
@endif
@if($devFilter==1 && ($GrAs || $GrCom|| $AsA|| $AsCom))
	<script type="text/javascript">
		$(window).load(function()
		{
			$("#popITOver").click();
			$("label[for='filterDev']").data("click","2");
			
		})
	
	</script>
@endif
<script type="text/javascript">
	$(function(){
		$("#submitFilter").click(function()
			{
				$("#filterFOrm").submit();
			});
		$("#filterFOrm input[type='radio']").change(function()
		{
			ob=$("#filterDev");
			ch=ob.is(":checked");
			//checking for the click count on the dev label
			var labelID=$(this).attr("id");
			var filLabel=$("label[for='filterDev']");
			if(labelID=="filterDev" && ch)
			{
				
				clickCount=filLabel.data("click");
				clickCount++;
				console.log(clickCount);
				if(clickCount<2)
				{
					filLabel.popover("hide");
					$(".devFl").prop("checked",false);
				}
				filLabel.data("click",clickCount);
			}
			if(!ch)
			{
				filLabel.popover("hide");
				$(".devFl").prop("checked",false);
				filLabel.data("click","0");
			}
			id=$(this).attr("id");
			// alert(id);
			nm=$(this).attr("name");
			$("input[name='"+nm+"']").each(function()
			{
				th=$(this);
				i=th.attr("id");
				$("label[for='"+i+"']").removeClass("activeLabel");
			})
			if($(this).is(":checked"))
			{
				
				$("label[for='"+id+"']").addClass("activeLabel");
			}
			
			/*else
				$("label[for='filterDev']").popover("show");*/
		});
		$(".devFl").each(function()
		{
			th=$(this);
			ch=th.is(":checked");
			id=th.attr("id");
			if(ch)
			{
              $("label[for='"+id+"']").addClass("activeLabel");
			}
		});
		$("body").on("click","#resetDevFil",function()
		{
			$(".devFl").each(function()
			{
				th=$(this);
				th.prop("checked",false);
				id=th.attr("id");
	            $("label[for='"+id+"']").removeClass("activeLabel");
			});
			// $(".popver").popover("hide");
		});
		
	})
</script>
<div class="row top-buffer-small">
	{{Form::open(array('url'=>"app","id"=>"filterFOrm","class"=>"form-inline filterForm hidden-x"))}}
		<!-- new filter starts -->
				
				<input class="hidden" type="radio" name="filterR" id="filterGraphic" 
							 	{{($filterR=="filterGraphic")?"checked='checked'":""}} value="filterGraphic" />
				<label class="padding-small pointer" for="filterGraphic">Graphics Bug</label>

				<input class="hidden" type="radio" name="filterR" id="filterFlight" 
							 	{{($filterR=="filterFlight")?"checked='checked'":""}} value="filterFlight" />
				<label class="padding-small pointer" for="filterFlight">Test Flight</label>

				<input class="hidden" type="radio" name="filterR" id="filterUpl" 
							 	{{($filterR=="filterUpl")?"checked='checked'":""}} value="filterUpl" />
				<label class="padding-small pointer" for="filterUpl">Upload</label>

				
		<!-- new filter ends -->
		
		
		<button  type="submit" class="btn btn-small"><i class="fa fa-search"></i></button>
		<a href="{{URL::to('app')}}" class="btn btn-small ui-red" rel="tooltip" title="reset"><i class="fa  fa-times ui-text-black"></i></a>

		
	</form>
</div>
