{{HTML::script("js/editTable.js")}}
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title" id="editEditName"></h3>
      </div>
      <div class="modal-body">
        {{Form::open(array('url'=>'app/update','class'=>"form-horizontal",'role'=>"form"))}}
          <input type="hidden" name="appID" id="editAppID">
        	
          <div class="form-group">
            <label for="editAppName" class="col-xs-4 control-label">app name</label>
            <div class="col-xs-6">
              <input type="text" class="form-control axBlank" id="editAppName" name="app_name"  placeholder="app name">
            </div>
          </div>
          
          <div class="form-group">
            <label for="editRP" class="col-xs-4 control-label">root project</label>
            <div class="col-xs-6">
              <select name="root_project_id" id="editRP" class="form-control">
                @foreach($root as $r)
                  <option value="{{$r->id}}">{{$r->project_name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          
          <div class="form-group">
            <label for="editIT" class="col-xs-4 control-label">account</label>
            <div class="col-xs-6">
              <select name="itunes_account_id" id="editIT" class="form-control">
                @foreach($ac as $r)
                  <option value="{{$r->id}}">{{$r->account}}</option>
                @endforeach
              </select>
            </div>
          </div>
          
          <div class="form-group">
            <label for="editPrty" class="col-xs-4 control-label">priority</label>
            <div class="col-xs-6">
             <select name="priority" id="editPrty" class="form-control">
               <option value="0">select</option>
               @for($x=1;$x<=5;$x++)
                <option value="{{$x}}">{{$x}}</option>
               @endfor
              </select>  
            </div>
          </div>
          
          <div class="form-group">
            <label for="editAT" class="col-xs-4 control-label">assigned to</label>
            <div class="col-xs-6">
              <select name="assignedTo" id="editAT" class="form-control">
                <option value="">select user</option>
                @foreach($usr as $r)
                  <option value="{{$r->id}}">{{$r->username}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="editAppleID" class="col-xs-4 control-label">auto process</label>
            <div class="col-xs-6">
              <input type="radio" class="hide-submit" id="editAutoProcessY" name="autoProcess"  placeholder="Auto Process" value="1">
              <label for="editAutoProcessY" class="checkLabel">yes</label>
               <input type="radio" class="hide-submit" id="editAutoProcessN" name="autoProcess"  placeholder="Auto Process" value="0">
              <label for="editAutoProcessN" class="checkLabel">no</label>
            </div>
          </div>
          <div class="form-group">
          <h3 class="col-xs-offset-1">Free:</h3>
            <label for="editAppleID" class="col-xs-4 control-label">apple ID</label>
            <div class="col-xs-6">
              <input type="text" class="form-control" id="editAppleID" name="appleID"  placeholder="apple ID">
            </div>
          </div>
          <div class="form-group">
            <label for="editItunesURL" class="col-xs-4 control-label">iTunes URL</label>
            <div class="col-xs-6">
              <input type="text" class="form-control" id="editItunesURL" name="itunesURL"  placeholder="iTunes URL">
            </div>
          </div>
          <!-- pro app id and url begins -->
            <div class="form-group pro-show">
            <h3 class="col-xs-offset-1">Pro:</h3>
              <label for="editPappleID" class="col-xs-4 control-label">apple ID</label>
              <div class="col-xs-6">
                <input type="text" class="form-control" id="editPappleID" name="PappleID" placeholder="apple ID">
              </div>
            </div>
            
            <div class="form-group pro-show">
              <label for="editPitunesURL" class="col-xs-4 control-label">iTunes URL</label>
              <div class="col-xs-6">
                <input type="text" class="form-control" id="editPitunesURL" name="PitunesURL"  placeholder="iTunes URL">
              </div>
            </div>
            <!-- pro and app id ends -->
          <div class="form-group">
            <div class="col-xs-3  col-xs-offset-3">
              <button type="button" class="btn form-control btn-default" data-dismiss="modal">Close</button>
            </div>
            <div class="col-xs-3">
              <input type="submit" class="btn form-control btn-warning" value="update">
            </div>
          </div>
        {{Form::close()}}
      </div>
      <div class="modal-footer">
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->