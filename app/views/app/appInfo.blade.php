<input type="hidden"  id="appAbsID" value="{{$id}}">
{{HTML::script("js/appInfo.js")}}
<div class="row top-buffer-small">
	<div class="col-xs-2">
		<span for="gDesigner">Graphic Designer</span>
	</div>
	<div class="col-xs-2">
		<input type="text" name="gDesigner" id="gDesigner" class="form-control
		{{($ap->graphic_designer)?'AIC':''}}"
		value="{{htmlentities($ap->graphic_designer)}}" {{(!$adrt)?"disabled='disabled'":""}}>
	</div>
	<div class="col-xs-1">
		<input type="checkbox" {{(!$adrt)?"disabled='disabled'":""}}
		{{($ap->gd_assign)?"checked='checked'":""}}
		name="gAssign" id="gAssign" class="hidden-x">
		<label for="gAssign"  class="checkLabel AICLabel padding-small">assigned</label>
	</div>
	<div class="col-xs-1">
		<label for="graphics">Graphics</label>
	</div>
	
	<div class="col-xs-5">
		<input type="text" name="graphics" id="graphics" class="form-control AIC"
		value="{{htmlentities($ap->graphics)}}" data-unqiue="{{$id}}-{{$ap->app_name}}-">
	</div>
	
	<div class="col-xs-1">
		<input type="checkbox" {{(!$adrt)?"disabled='disabled'":""}}
		{{($ap->graphic_assign)?"checked='checked'":""}}
		name="grapAssign" id="grapAssign" class="hidden-x">
		<label for="grapAssign"  class="checkLabel AICLabel padding-small">complete</label>
	</div>
</div>
<div class="row top-buffer-small">
	<div class="col-xs-2">
		<label for="aspRsch">Aso Researcher</label>
	</div>
	<div class="col-xs-2">
		<input type="text" name="aspRsch" id="aspRsch" class="form-control 
		{{($ap->aso_resch)?'AIC':''}}"
		value="{{htmlentities($ap->aso_resch)}}" {{(!$adso)?"disabled='disabled'":""}}>
	</div>
	<div class="col-xs-1">
		<input type="checkbox" {{(!$adso)?"disabled='disabled'":""}}
		{{($ap->resch_assigned)?"checked='checked'":""}}
		name="asoAssign" id="asoAssign" class="hidden-x">
		<label for="asoAssign"  class="checkLabel AICLabel padding-small">assigned</label>
	</div>
	<div class="col-xs-1">
		<label for="aso">Aso</label>
	</div>
	
	<div class="col-xs-5">
		<input type="text" name="aso" id="aso" class="form-control AIC"
		value="{{htmlentities($ap->aso)}}" {{($utype=="6")?"disabled='disabled'":""}}>
	</div>
	
	<div class="col-xs-1">
		
	</div>
</div>
<div class="row top-buffer-small">
	<div class="col-xs-2">
		<label for="appNmFree">App name Free</label>
	</div>
	<div class="col-xs-9">
		<input type="text" name="appNmFree" id="appNmFree" class="form-control AIC"
		value="{{htmlentities($ap->app_info_name)}}" {{(!$adso)?"disabled='disabled'":""}}>
	</div>
	<div class="col-xs-1">
		<input type="checkbox" {{(!$adso)?"disabled='disabled'":""}}
		{{($ap->app_info_complete)?"checked='checked'":""}}
		name="apInforComplete" id="apInforComplete" class="hidden-x">
		<label for="apInforComplete"  class="checkLabel AICLabel padding-small">
			complete
		</label>
	</div>
</div>
@if($rp->free_n_pro)
<div class="row top-buffer-small">
	<div class="col-xs-2">
		<label for="appNmPro">App name Pro</label>
	</div>
	<div class="col-xs-9">
		<input type="text" name="appNmPro" id="appNmPro" class="form-control AIC"
		value="{{htmlentities($ap->app_pro_info_name)}}" {{(!$adso)?"disabled='disabled'":""}}>
	</div>
</div>
@endif