<script type="text/javascript">
	$(window).load(function()
	{
		$(".itm-0").remove();
		
	});
	$(function(){
			$("#filterTable").on("click",".delAppDev",function()
			{
				
				url=$(this).data("url");
				if(!confirm("Are you Sure?"))
					return;
				var tr=$(this).closest("tr");
				data='data';
				console.log(url);
				console.log(data);
				$.ajax({method:'get',data:data,url:url,success:function(resp){
					tr.fadeOut();
					alertify.sucess("Deleted Successfully");
					
				}});
			});
		});	
</script>
<!-- plugin for datetime sort -->
{{HTML::script("js/dataTableDate.js")}}
<table class="table dataTableDate" id="filterTable">
	<thead>
		<tr>
			<th>#</th>
			<th>App Name</th>
			<th>Root Project</th>
			<th>Graphic Designer</th>
			<th>Graphics Complete</th>
			<th>ASO Researcher</th>
			<th>ASO Complete</th>
			<th>{{($filterR=="filterPend")?"Assign":"Status"}}</th>
			<th>action</th>
			<th>modified</th>
		</tr>
	</thead>
	<?php $row=0 ?>
	<tbody>
		@foreach($apps as $a)
			<tr>
				<td>{{++$row}}</td>
				<td  class="app-detail pointer" data-appID="{{$a->id}}">{{$a->app_name}}</td>
				<td  class="app-detail pointer" data-appID="{{$a->id}}">{{$a->project_name}}</td>
				<td  class="app-detail pointer" data-appID="{{$a->id}}">{{$a->graphic_designer}}</td>
				<td  class="app-detail pointer" data-appID="{{$a->id}}">{{($a->graphic_assign)?"YES":"NO"}}</td>
				<td  class="app-detail pointer" data-appID="{{$a->id}}">{{($a->aso_resch)}}</td>
				<td  class="app-detail pointer" 
					data-appID="{{$a->id}}">{{($a->app_info_complete)?"YES":"NO"}}</td>
				<td data-appID="{{$a->id}}" style="background-color:{{$status[(string)$a->app_status]['color']}};!important">
					{{($a->app_status==16)?$act[$a->root_project_id]:$status[(string)$a->app_status]['status']}}
					
				</td>
				<td>
					<span class="delAppDev" data-url="{{'app/del/'.$a->id}}">
						<i class="fa fa-minus-circle ui-text-red"></i>
					</span>
				</td>
				<td>
					{{last_modified($a)}}
				</td>
			</tr>
		@endforeach
	</tbody>
</table>