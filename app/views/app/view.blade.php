<script type="text/javascript" src="js/spectrum.js"></script>
<link rel="stylesheet" href="css/spectrum.css" type="text/css" />
{{HTML::style("css/redactor.css")}}
{{HTML::script("js/redactor.min.js")}}
<!-- dropbox api -->
<script type="text/javascript">
	$(function(){
		$(".dropbox-saver").each(function(i,th)
		{
			url=$(this).attr("href");
			buttn=Dropbox.createSaveButton(url);
			$(this).before(buttn);
			$(this).remove();
			// resetting dropbox button
			if(url=="#" || url=="")
				$(".dropbox-dropin-btn").hide();
			else
				$(".dropbox-dropin-btn").html('<img src="img/dropbox.png" />');
		});
	})
</script>
<script type="text/javascript">

	$(function(){
		$("#textColor").spectrum({
			change 	:function(color)
			{
				$("#textColor").val(color.toHexString());
			}
		});
		saveProcess=function()
		{
			if(!$("#loadingSave").length)
				$("#saveButton").after('<img id="loadingSave" class="loading-image" src="img/loading/3.gif" />');	
		};
		changeDetail=function()
		{
			
			th=$(this);
			table=th.closest('table');

			data={
				tableIndex	: table.attr("data-table"),
 				rowIndex	: th.parent().parent().index()+1,
				colIndex	: th.parent().index()+1,
				appID 		: table.attr("data-appID"),
				val			: th.val(),
				attrib		: th.parent().parent().attr("data-row-atrrib")
				

			};
			url='app/changeAppDetails';
			console.log(url);
			console.log(data);
			th.before($.trim(data.val));
			th.remove();
			$.ajax({method:'post',beforeSend:saveProcess,data:data,url:url,success:function(resp){
				console.log(resp);
				$("#loadingSave").remove();
				


			}});
		};
		
		$(".app-cell").change(function()
		{
			th=$(this);
			tr=$(this).parent().parent();
			td=$(this).parent();
			url='app/changeReportCell';
			data={
				appID 		: {{$id}},
				rowIndex	: tr.index()+1,
				colIndex	: td.index()+1,
				value		: th.val(),
				style		: th.attr("style")
			};
			console.log(url);
			console.log(data);
			$.ajax({method:'post',data:data,url:url,success:function(resp){
				console.log(resp);
				
			}});
		})
	});
$(function(){
	$(".reportContent td").click(changeColor);
});

//saving app details
$(function(){
	$(".app-cell-detail").dblclick(function()
	{
	  th=$(this);
	  if(th.children(":first").is("span"))
	  	{
	  		th=th.children(":last");
	  		if(!th.hasClass("app_other") && !th.hasClass("app_name") && !th.hasClass("app_select") )
			  {
			  	th=th.prev();
			  }
	  	}

	  html=th.html();
	  //remving any current id textbox
	  $("#appCurrentIndex").removeAttr("id");
	  th.attr("id","appCurrentIndex");
	  if(th.hasClass("app_select")) //if select box is to be displayed
	  {
	  	inp=$('<select id="app_other" data-val="'+th.html()+'"></select>');
	  	inp.addClass("form-control");
	  	opt=th.attr("data-opt");
	  	op=opt.split("|");
	  	length=(th.attr("data-col")=="price_tier")?51:op.length;
	  	inp.append('<option value=""></option>');
	  	for(x=0;x<length;x++)
	  	{
	  		val=x;
	  		text=(!op[x])?x:op[x];
	  		if(text.toString().indexOf(":")!==-1)
	  		{
	  			ox=op[x].split(":");
	  			text=(!ox[1])?x:ox[1];
	  			val=(!ox[0])?x:ox[0];

	  		}
	  		
	  		inp.append('<option value="'+val+'">'+text+"</option>");
	  	}
	  }
	  else if(th.hasClass("app_other"))
	  {
	  	bund='data-col="'+th.attr("data-col")+'" ';
	  	inp='<input type="text" id="app_other" data-val="'+th.html()+'" class="border-none width-100 padding-small font-bigger strong" value="'+th.html()+'" '+bund+' />';
	  }
	  else if(th.hasClass("app_name"))
	  {
	  	inp='<input type="text" id="app_name" data-val="'+th.html()+'" class="border-none width-100 padding-small font-bigger strong" value="'+th.html()+'" />';
	  }
	  lightBox.show(inp);

	});
	$("body").on("change","#app_name",function()
	{
		old_name=$(".app_name").html();
		old_bund=old_name.replace(" ","");
		new_name=$(this).val();
		new_bund=new_name.replace(/ /g,"");
		/*$(".app_bundle,.app_compress").each(function()
		{
			val=$(this).html();
			val=val.replace(old_bund,new_bund);
			$(this).html(val);
		})*/
		$("#appContent .app_name").html($(this).val());
		app_detail_form_add({
			"name"		: "app_name",
			"val"		: $(this).val(),
			"old_name"  : old_name
		});
	});
	//app_other change
	$("body").on("change","#app_other",function()
	{
		th=$(this);
		v=(th.attr("data-val"));
		v2=(th.is("input"))?th.val():th.children("option").filter(":selected").text();
		if(v2=='')
			return;
		if($("#appCurrentIndex").prev().hasClass("app_compress"))
			v2=v2.replace(/ /g ,"");
		if(v2.indexOf(".")!==0 && th.is("input") && $("#appCurrentIndex").prev().hasClass("app_compress") ) //if dot is removed from its position and the value is a textbox
			v2="."+v2;
		th=$("#appCurrentIndex");
		if(th.attr("data-col")=="bundle_id")
		{
			v2=v2.replace(/ /g ,"");
			$("#appContent span.app_compress").each(function()
			{
				htm=$(this).html();
				$(this).html(v2);
			});

		}

		tb=th.parent().parent();
		// alertify.log($(this).is("select"))
		app_detail_form_add({
			name 		: "field[]",
			tbl			: tb.attr("data-tb"),
			index		: tb.attr("data-index"),
			col 		: th.attr("data-col"),
			val         : $(this).is("select")?$(this).val():v2
		});
		th.html(v2);
		if(th.attr("data-dup")=="game-dup")
		{
			col=th.attr("data-col");
			//getting index of both the rows
			$('div[data-tb="app_game_details"]').each(function()
			{
				app_detail_form_add({
				name 		: "field[]",
				tbl			: "app_game_details",
				index		: $(this).attr("data-index"),
				col 		: col,
				val         : v2
				});
				$("span[data-col='"+col+"']").html(v2);
			})
		}
		// $("#app_other").removeAttr("id");
	});
});
var app_detail_form_add=function(obj)
{
	part="_~@~_";
	if(obj.name=="app_name")
		{
			val=obj.val;
			val2=obj.old_name;
		}
	else
		{
			val=obj.tbl+part+obj.index+part+obj.col+part+obj.val;
			val2=obj.tbl+part+obj.index+part+obj.col+part;
		}
	inp="#app_detail_form input[value^='"+val2+"']";
	input='<input type="hidden" name="'+obj.name+'" value="'+val+'" />';
	if($(inp).length)
		$(inp).val(val);
	else
		$("#app_detail_form").append(input);
};

var lightBox={
	show : function(th)
	{
		if(!$(".black_overlay").length)
		{
			$('body').append('<div class="black_overlay"><span class="close_overlay">&times;</span></div>');
		}
		if(!$(".content_overlay").length)
		{
			$('body').append('<div class="content_overlay"></div>');

		}
		$(".content_overlay").html(th);
		$(".black_overlay,.content_overlay").fadeIn("slow");
		$(".content_overlay").children(":first").select();
		$(".close_overlay,.black_overlay").click(this.hide);
		$(".black_overlay,.content_overlay").keydown(this.keyBind);
	},
	hide : function()
	{
		$(".black_overlay,.content_overlay").fadeOut("slow");
		lightBox.callback();
	},
	keyBind	:function(e)
	{
		var code = e.keyCode || e.which;
			 if(code == 27) { 

				lightBox.hide();
			 }

	},
	callback:function()
	{
		return $(".content_overlay").children().clone();
	},
	escapeKeys: [13,27]
};


</script>
<div class="row padding-mid ui-white top-buffer-largest" id="appContent">
@if($utype!=2)
	@include("app.appInfo",array("utype"=>$utype,"ap"=>$ap,"id"=>$id,"rp"=>$rp,"adrt"=>($utype==1 || $utype==6 || $utype==7),"adso"=>($utype==1 || $utype==5 || $utype==7)))
@endif
<div class="clearfix"></div>
							
@if(!$ap->itunes_account_id)
	<div id="details"></div>
	<div id="statusHistory">
		@include("app.statusHistory",array('history'=>$history))
	</div>  
	{{exit}}
@endif
@if($utype==6)
	@include("app.graphFixed")
@endif
		<!-- Nav tabs -->
<ul class="nav nav-tabs">
  @if($utype==3 || $utype==1 || $utype==5 || $utype==7)
  <li class="{{($utype==3 || $utype==1 || $utype==5 || $utype==7)?'active':''}}">
  	<a href="#details" data-toggle="tab">details</a>
  </li>
  @endif
  @if($utype==2 || $utype==1 || $utype==5 || $utype==4 || $utype==7) 
  <li class="{{($utype==2 || $utype==4)?'active':''}}">
  	<a href="#reports" data-toggle="tab" class="{{(false)?'hidden-x':''}}">reports</a>
  </li>
  @endif
</ul>

<!-- Tab panes -->
<?php  ?>
<div class="tab-content top-buffer-small noneCase">
  <div class="tab-pane  fade in {{($utype==3 || $utype==1 || $utype==5 || $utype==7 )?'active':''}} top-buffer-large" id="details">
  
	<div class="row strong border-bottom padding-small capitalCase">
		<div class="col-xs-3">app name</div>
		<div class="col-xs-3">sku</div>
		<div class="col-xs-3">bundle ID</div>
		<div class="col-xs-1">price tier</div>
		<div class="col-xs-2">enable <span class="smallCase">i</span>Ad</div>
	</div>
	@foreach($sku as $sk)
		@if(!$rp->free_n_pro && $sk->free_pro==2)
		<?php continue ?>
		@endif
		<div class="row padding-small font-smaller cursor-pointer" data-index="{{$sk->id}}" data-tb="app_sku_details"> 
			<div class="col-xs-3">
				<span class="app_name app-cell-detail">{{$ap->app_name}}</span><span>{{$sk->app_name}}</span>
			</div>
			<div class="col-xs-3 app-cell-detail">
				<span class="app_bundle pull-left">{{$ap->bundlePrefix}}.</span>	
				<span class="app_compress pull-left">{{$apc}}</span>
				<span class="app_other pull-left" data-col="sku">{{$sk->sku}}</span>
			</div>
			<div class="col-xs-3  {{($sk->free_pro==1)?'app-cell-detail':''}}">
				<span class="budle_id pull-left">{{$ap->bundlePrefix}}.</span>	
				<span class="app_other pull-left app_compress" data-col="bundle_id">{{$apc}}</span>
				<span class="pull-left">{{$sk->app_name}}</span>
			</div>
			<div class="col-xs-1 app-cell-detail">
				<span class="app_select"  data-col="price_tier" data-opt="free|1|2|3|4|5">{{$sk->price_tier}}</span>
			</div>
			<div class="col-xs-2 app-cell-detail">
				<span class="app_select" data-col="enable_iad" data-opt="yes|no">{{$sk->enable_iad}}</span>
			</div>
		</div>
	@endforeach
  @if($rp->game_center)
  	<h2>Game center info</h2>
	<div class="row strong border-bottom capitalCase padding-small">
		<div class="col-xs-2">app name</div>
		<div class="col-xs-2">group name</div>
		<div class="col-xs-3">leaderboard reference name</div>
		<div class="col-xs-3">leaderboard ID</div>
		<div class="col-xs-2">language name</div>
	</div>
	@foreach($game as $sk)
		@if(!$rp->free_n_pro && $sk->free_pro==2)
		<?php continue ?>
		@endif
		<div class="row padding-small font-smaller cursor-pointer" data-tb="app_game_details" data-index="{{$sk->id}}">
			<div class="col-xs-2">
				<span class="app_name app-cell-detail">{{$ap->app_name}}</span><span> {{$sk->app_name}}</span>
			</div>
			<div class="col-xs-2 app-cell-detail">
				<span class="app_other" data-col="group_name" data-dup="game-dup" > {{$sk->group_name}}</span>
			</div>
			<div class="col-xs-3 app-cell-detail app_bundle">
					<span class="pull-left">grp.</span>
					<span class="pull-left app_bundle">{{$ap->bundlePrefix}}.</span>
					<span class="pull-left app_compress">{{$apc}}</span>
					<span class="app_other pull-left" data-col="lead_name" data-dup="game-dup">{{$sk->lead_name}}</span>
			</div>
			<div class="col-xs-3 app-cell-detail app_bundle">
					<span class="pull-left">grp.</span>
					<span class="pull-left app_bundle">{{$ap->bundlePrefix}}.</span>
					<span class="pull-left app_compress">{{$apc}}</span>
					<span class="app_other pull-left pull-left" data-col="lead_id" data-dup="game-dup">{{$sk->lead_id}}</span>
			</div>
			<div class="col-xs-2 app-cell-detail">
				<span class="app_other pull-left" data-col="lang" data-dup="game-dup">{{$sk->lang}}</span>	
			</div>
		</div>
	@endforeach
  @endif
	
	@if($rp->iap)

 	 <h2>IAP info</h2>
		<div class="row padding-small capitalCase border-bottom strong">
			<div class="col-xs-2">app name</div>
			<div class="col-xs-1">type</div>
			<div class="col-xs-2">reference name</div>
			<div class="col-xs-2">product ID</div>
			<div class="col-xs-1">price tier</div>
			<div class="col-xs-1">display name</div>
			<div class="col-xs-2">description</div>
			<div class="col-xs-1">action</div>
		</div>
		@foreach($iap as $sk)
  			@if(!$rp->free_n_pro && $sk->free_pro==2)
				<?php continue ?>
  			@endif
  			<div class="row font-smaller cursor-pointer border-bottom" data-tb="app_iap_details" data-index="{{$sk->id}}">
  				<div class="col-xs-2">
  					<span class="app_name app-cell-detail">{{$ap->app_name}}</span><span>{{$sk->app_name}}</span>
  				</div>
  				<div class="col-xs-1 app-cell-detail">
  					<span class="app_select pull-left" data-col="type" data-opt="0:cosumable|1:non-consumable">{{$sk->con}}</span> 
  				</div>
  				<div class="col-xs-2 app-cell-detail word-wrap app_bundle">
  					<span class="pull-left app_bundle">{{$ap->bundlePrefix}}.</span>
					<span class="pull-left app_compress">{{$apc}}{{$sk->free_pro==2?'Pro':''}}</span>
					<span class="app_other pull-left" data-col="ref_name">.{{trim($sk->ref_name,".")}}</span>	
  				</div>
  				<div class="col-xs-2  word-wrap app_bundle">
  					<span class="pull-left app_bundle">{{$ap->bundlePrefix}}.</span>
					<span class="pull-left app_compress">{{$apc}}{{$sk->free_pro==2?'Pro':''}}</span>
					<span class="app_other pull-left" data-col="prod_id">.{{$sk->prod_id}}</span>
  				</div>
  				<div class="col-xs-1 app-cell-detail">
					<span class="app_select"  data-col="price_tier" data-opt="free|1|2|3|4|5">{{$sk->price_tier}}</span>
  				</div>
  				<div class="col-xs-1 app-cell-detail">
  					<span class="app_other pull-left" data-col="dis_name">{{$sk->dis_name}}</span>
  				</div>
  				<div class="col-xs-2 app-cell-detail">
  					<span class="app_other pull-left" data-col="desc">{{$sk->desc}}</span>
  				</div>
  				<div class="col-xs-1 app-cell-detail">
  					<a href="app/delIap/{{$sk->id}}" class="confirm">
	  					<i class="fa fa-minus-circle ui-text-red"></i>
	  				</a>
  				</div>
  			</div>
  		@endforeach
				<!-- <td class="no-update">
						  				<button class="confirm delIap" data-appID="{{$id}}" data-iap="" data-free="{{0}}">
						  					<i class="fa fa-minus-circle ui-text-red"></i>
						  				</button>
						  			</td> -->
	@endif
		{{Form::open(array("url"=>"app/changeAppDetails","id"=>"app_detail_form","class"=>"top-buffer-large"))}}
	    	<input type="hidden" name="appID" value="{{$id}}">
	    	<input type="submit" id="saveButton" class="btn btn-primary" value="save" />
	    {{Form::close()}}
		
	 <!-- && $ap->assign_to==Auth::user()->id -->
  	@if($utype==3 )
		{{Form::open(array('url'=>"app/unassign",'class'=>'form-horizontal pull-left col-xs-12',"accept-charset"=>"UTF-8"))}}
			<input type="hidden" name="appID" value="{{$id}}">
			<div class="row pull-right">
				<div class="form-group pull-left">
					<div class="form-group">
						<label for="provProf" class="col-xs-5 control-label font-smaller">Provisioning Profiles</label>
						<div class="col-xs-6">
							<input type="text" autocomplete="off" name="provProf" id="provProf" class="axBlank form-control" placeholder="Provisioning Profiles"
							value="{{$ap->prov_profile}}" value="{{$ap->fbID}}"
							onkeyup="this.value=this.value.replace(/(http:|https:)\/\//,'')">
						</div>
					</div>
					<!-- before v1.8 $rp->id==4 || $rp->id==13 || $rp->id==26 -->
					@if($rp->facebook) <!-- for Dress Up || Mastache Bash || Casino -->
						<div class="form-group">
							<label for="fbID" class="col-xs-5 control-label font-smaller">Facebook AppID</label>
							<div class="col-xs-6">
								<input type="text" autocomplete="off" name="fbID" id="fbID" class="axBlank form-control" placeholder="Facebook AppID" value="{{$ap->fbID}}">
							</div>
						</div>
					@endif
				</div>
				@if($rp->next_peer) <!-- for Jewel blitz-->
					<div class="form-group pull-left">
						<div class="form-group">
							<label for="nxtPeer" class="col-xs-5 control-label font-smaller">NextPeer Key</label>
							<div class="col-xs-6">
								<input type="text" autocomplete="off" name="nxtPeer" id="nxtPeer" class="axBlank form-control" placeholder="NextPeer Key"
								value="{{$ap->nxt_key}}">
							</div>
						</div>
						<div class="form-group">
							<label for="nxtSSo" class="col-xs-5 control-label font-smaller">NextPeer SSO</label>
							<div class="col-xs-6">
								<input type="text" autocomplete="off" name="nxtSSo" id="nxtSSo" class="axBlank form-control" placeholder="NextPeer SSO" value="{{$ap->nxt_key}}">
							</div>
						</div>
					</div>
				@endif
				<div class="form-group pull-left">
					<div class="form-group">
						<label for="appleID" class="col-xs-5 control-label font-smaller">Free apple ID</label>
						<div class="col-xs-6">
							<input type="number" autocomplete="off" name="appleID" id="appleID" class="axBlank form-control" placeholder="apple ID"
							value="{{$ap->apple_id}}">
						</div>
					</div>
					<div class="form-group">
						<label for="itunesURL" class="col-xs-5 control-label font-smaller">Free iTunes URL</label>
						<div class="col-xs-6">
							<input type="text" autocomplete="off" name="itunesURL" id="itunesURL" class="axBlank form-control" placeholder="iTunes URL"
							value="{{$ap->itunes_url}}">
						</div>
					</div>
				</div>
				@if($rp->free_n_pro==1)
					<div class="form-group pull-left">
						
						<div class="form-group">
							<label for="PappleID" class="col-xs-5 control-label font-smaller">Pro apple ID</label>
							<div class="col-xs-6">
								<input type="number" autocomplete="off" name="PappleID" id="PappleID" class="axBlank form-control" placeholder="apple ID"
								value="{{$ap->Papple_id}}">
							</div>
						</div>
						<div class="form-group">
							<label for="PitunesURL" class="col-xs-5 control-label font-smaller">Pro iTunes URL</label>
							<div class="col-xs-6">
								<input type="text" autocomplete="off" name="PitunesURL" id="PitunesURL" class="axBlank form-control" placeholder="iTunes URL"
								value="{{$ap->Pitunes_url}}">
							</div>
						</div>
					</div>
				@endif
				<div class="form-group">
					<div class="col-xs-6 col-xs-offset-1">
						<input type="submit" value="unassign from me" class="btn btn-success pull-right">
					</div>
				</div>
			</div>
		{{Form::close()}}
	@endif
	<div class="clearfix"></div>
		<div id="statusHistory">
			@include("app.statusHistory",array('history'=>$history))
		</div>  	
  </div>
  <div class="tab-pane {{($utype==2 || $utype==4)?'active':''}}" id="reports">
  	@if($utype==2)
  		<div class="row">
  			@if($ap->app_status==19)
  				<h3 class="col-xs-offset-1">ReUpload Report</h3>
  			@elseif($ap->app_status==3)
  				<h3 class="col-xs-offset-1">Upload Report</h3>
  			@endif
  		</div>
  	@endif
  	@if($utype==1 || $utype==5 || $utype==7)
  	<div class="btn-group top-buffer-large width-5pc">
		<button class="pull-right" id="setColor">
		<i class="fa  fa-bitbucket"></i>
		</button>
		<input type="text" name="" id="textColor" class="pull-right color">
		<button class="pull-right" id="cancelColor">
		<i class="fa  fa-arrows-alt"></i>
	</button>
	</div>
	@endif
  		@include("app.reportContent",compact("ap","utype","apRep","sty","id"))
  	<script type="text/javascript">
  	$(function(){
  		$("#assignClick").click(function()
  		{
  			url='app/assign';
  			// v1.10 reminind that this ac has no email for this itunes acc
  			var hasEmail=$("#assign_to option:selected").data("hasemail");
  			if(!hasEmail)
  			{
  				if(!confirm("Are You Sure? User Has no email for this itunes Account"))
  					{
  						return;
  					}
  			}
  			if(!$("#assign_to").val())
  			{
  				alertify.alert('incomplete selection');
  				return;
  			}
  			tr=$(".excel-table");
  			appID=(tr.attr("data-appID"));
  			data={
  				assign_to   : $("#assign_to").val(),
  				appID       : appID,
  				reupload	: $("#reupload").is(":checked")?1:0

  			};
  			console.log(url);
  			console.log(data);
  			var th=$(this);
  			$.ajax({method:'post',beforeSend:function()
  			{
  				th.after("<img class='padding-small' id='loadingImg' src='img/loading/3.gif' height='40'>");
  			},data:data,url:url,success:function(resp){
  				location.reload();
  				console.log(resp);
  			}});
  		})
  	});
  	$(function(){
  		$("#reportMessage").change(function()
  		{
  			tr=$(".excel-table");
  			appID=(tr.attr("data-appID"));
  			url='app/reportText';
  			data={
  				appID       : appID,
  				report_text : $(this).val()  
  			};
  			console.log(url);
  			console.log(data);
  			$.ajax({method:'post',data:data,url:url,success:function(resp){
  				// alertify.log("updated");
  				
  			}});
  		})
  	})
  	</script>
  	@if($utype==1 || $utype==5 || $utype==7)
  		<div class="row">
  		<div class="col-xs-6 pull-right">
			<label for="provProDown" class="col-xs-5 text-right control-label font-smaller">Provisioning Profile</label>
			<div class="col-xs-4">
					<input type="text" name="boxProf" id="adminProvProDown" 
				value="{{$ap->prov_profile}}" class="form-control changeKeys" 
				data-appID="{{$id}}">
			</div>
			<button class="btn"><a target="_blank" id="adminProvProKeys"href="{{$ap->prov_profile}}"><i class="fa fa-download"></i></a></button>
		</div>
	  		@if($ap->assign_to)
	  			Already Assiged to : <strong>{{$ap->username}} </strong> <br><br>
	  		@endif
	  		<input type="checkbox" class="hidden-x" name="reupload" id="reupload">
		  		<label for="reupload">reupload</label>
	  		<button id="assignClick" class="btn btn-primary">Assign to User</button>
	  		<div class="col-xs-2">
	  			<select id="assign_to" class="form-control">
	  				<option value="">select user</option>
		  			@foreach($uploadUsersU as $u)
		  				<option value="{{$u->id}}"
		  				data-hasemail="{{$u->hasEmail}}">
		  				@if(!$u->hasEmail)
		  				 <strong>[!No Email]</strong>
		  				@endif
		  					{{$u->username}}
		  				</option>
		  			@endforeach
		  		</select>
	  		</div>

	  		<div class="col-xs-2 hidden-x">
	  			<select id="freeSDK" class="form-control">
	  				<option value="">select free SDK</option>
		  			@foreach($sdk as $s)
		  				<option value="{{$s->id}}">{{$s->sdk_name}}</option>
		  			@endforeach
		  		</select>
	  		</div>
	  		@if($rp->free_n_pro)
	  			<div class="col-xs-2 hidden-x">
		  			<select id="proSDK" class="form-control">
		  				<option value="">select Pro SDK</option>
			  			@foreach($sdk as $s)
			  				<option value="{{$s->id}}">{{$s->sdk_name}}</option>
			  			@endforeach
			  		</select>
		  		</div>
	  		@endif
	  	</div>
		

		<script type="text/javascript">
		$(document).ready(function(){
				$('#reportMessage').redactor({
					minHeight	: 200,
					changeCallback: function(html)
						{
							$("#reportMessage").val(html);
							$("#reportMessage").change();
						}
				});
			}
		);
		$(function(){
			$(".changeKeys").change(function()
			{
				url='app/updateKeys';
				data={
					appID : $(this).attr("data-appID"),
					field : $(this).attr("name"),
					val   : $(this).val()
				};
				data.val=data.val.replace(/(http:|https:)\/\//,'');
				$(this).val(data.val);
				console.log(url);
				console.log(data);
				$.ajax({method:'post',data:data,url:url,success:function(resp){
					console.log(resp);
					
				}});
			});
			$("#adminProvProDown").change(function()
			{
				$("#adminProvProKeys").attr("href","https://"+$(this).val());
			});
			
		})
		</script>
	  	<div class="row top-buffer-large">
	  		<textarea name="" class="form-control" id="reportMessage" cols="30" rows="40">{{$ap->report_text}}</textarea>
	  	</div>
	  	
	  	<div class="row top-buffer-large">

	  	
	  		@if($rp->facebook)<!-- for Dress Up || Mastache Bash || Casino -->
	  			<div class="col-xs-12" id="well-text">
		  			<label for="fbIDBox" class="col-xs-2 control-label font-smaller padding-small">facebook AppID</label>
		  			<div class="col-xs-4">
		  				<input type="text" name="boxFb" id="" value="{{$ap->fbID}}" 
		  				class="form-control changeKeys" data-appID="{{$id}}">
		  			</div>
		  		</div>
	  		@endif
	  		@if($rp->next_peer) <!-- before $rp->id==6 -->
	  			<div class="col-xs-12 top-buffer-small" id="well-text">
		  			<label for="fbIDBox" class="col-xs-2 control-label font-smaller padding-small">NextPeer Key</label>
		  			<div class="col-xs-4">
		  				<input type="text" name="boxKey" id="" value="{{$ap->nxt_key}}" 
		  				class="form-control changeKeys" data-appID="{{$id}}">
		  			</div>
		  		</div>
	  		@endif
	  		@if($rp->next_peer)
	  		<div class="col-xs-12 top-buffer-small" id="well-text">
	  			<label for="fbIDBox" class="col-xs-2 control-label font-smaller padding-small">NextPeer SSO</label>
	  			<div class="col-xs-4">
	  				<input type="text" name="boxSso" id="" value="{{$ap->nxt_sso}}" 
	  				class="form-control changeKeys" data-appID="{{$id}}">
	  			</div>
	  		</div>
	  		@endif
	  		
	  	</div>
	@endif
  	
	@if($utype==2)
		<script type="text/javascript">
		
		</script>
		<div class="row">
			<div class="col-xs-6 pull-right">
				<label for="provProDown" class="col-xs-5 control-label font-smaller">
					Provisioning Profile
				</label>
				<div class="col-xs-4">
						<input type="text" name="" id="provProDown" 
					value="{{$ap->prov_profile}}" readonly="readonly" class="form-control">
				</div>
				<!-- <a class="dropbox-saver" href="{{$ap->prov_profile}}">
					<img src="{{asset('img/dropbox.png')}}" alt="">
				</a> -->
				<button class="btn"><a target="_blank" id="adminProvProKeys" href="{{$ap->prov_profile}}"><i class="fa fa-download"></i></a></button>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 pull-right">
				<label for="" class="col-xs-5 control-label font-smaller">
					Graphics
				</label>
				<div class="col-xs-4">
						<input type="text" name="" id="" 
					value="{{$ap->graphics}}" readonly="readonly" class="form-control">
				</div>
			</div>
		</div>
		<div class="row top-buffer-large">
	  		<div class="col-xs-12" id="well-text">{{$ap->report_text}}</div>
	  	</div>
	  	<div class="row top-buffer-large">
	  		@if($ap->fbID)
	  			<div class="col-xs-12" id="well-text">
		  			<label for="fbIDBox" class="col-xs-2 control-label font-smaller padding-small">facebook AppID</label>
		  			<div class="col-xs-4">
		  				<input type="text" disabled="disabled" name="boxFb" id="" value="{{$ap->fbID}}" 
		  				class="form-control" data-appID="{{$id}}">
		  			</div>
		  		</div>
	  		@endif
	  		@if($ap->nxt_key)
	  			<div class="col-xs-12 top-buffer-small" id="well-text">
		  			<label for="fbIDBox" class="col-xs-2 control-label font-smaller padding-small">NextPeer Key</label>
		  			<div class="col-xs-4">
		  				<input type="text" disabled="disabled" name="boxKey" id="" value="{{$ap->nxt_key}}" 
		  				class="form-control" data-appID="{{$id}}">
		  			</div>
		  		</div>
	  		@endif
	  		@if($ap->nxt_sso)
	  		<div class="col-xs-12 top-buffer-small" id="well-text">
	  			<label for="fbIDBox" class="col-xs-2 control-label font-smaller padding-small">NextPeer SSO</label>
	  			<div class="col-xs-4">
	  				<input type="text" disabled="disabled" name="boxSso" id="" value="{{$ap->nxt_sso}}" 
	  				class="form-control" data-appID="{{$id}}">
	  			</div>
	  		</div>
	  		@endif
	  		<div class="col-xs-12 top-buffer-small" id="well-text">
	  			<label for="" class="col-xs-2 control-label font-smaller padding-small"> Apple ID</label>
	  			<div class="col-xs-4">
	  				<input type="text" disabled="disabled" name="" id="" value="{{$ap->apple_id}}" 
	  				class="form-control" >
	  			</div>
	  		</div>
	  		<div class="col-xs-12 top-buffer-small" id="well-text">
	  			<label for="" class="col-xs-2 control-label font-smaller padding-small"> Apple URL</label>
	  			<div class="col-xs-8">
	  				<input type="text" name="" readonly id="" value="{{$ap->itunes_url}}" 
	  				class="form-control">
	  			</div>
	  		</div>
	  		
	  	</div>
	  	@if($ap->free_sdk)
	  		<table border="1" id="importTable" class="font-smallest top-buffer-small hidden-x">
	  			<thead>
	  				<tr>
	  					<th>sdk name</th>
	  					@foreach($sdkHeading as $head)
	  						<th>{{$head->header_name}}</th>
	  					@endforeach
	  				</tr>
	  			</thead>
	  			<tbody>
	  				<!-- if any sdk key specified -->
	  				@if($ap->free_sdk) 
	  					<tr>
	  						<td>{{$sdkName[0]}}</td>
	  					@foreach($sdkHeading as $head)
	  						<td>{{$sdkRow[$ap->free_sdk][$head->heading_id]}}</td>
	  					@endforeach
	  					</tr>
	  				@endif
	  				<!-- if pro is allowed  and pro sdk is also specfied-->
  					@if($rp->free_n_pro && $ap->pro_sdk)
  						<tr>
	  						<td>{{$sdkName[1]}}</td>
		  					@foreach($sdkHeading as $head)
		  						<td>{{$sdkRow[$ap->pro_sdk][$head->heading_id]}}</td>
		  					@endforeach
	  					</tr>
  					@endif
	  			</tbody>
	  		</table>

	  	@endif
	@endif

	<div class="row">
	@if($utype==2 && ($ap->app_status<4 || $ap->app_status==19))
		<!-- <div class="col-xs-2 pull-right">
			<a class="confirm btn btn-success top-buffer-small" href="app/complete/{{$id}}" data-placement="top" data-toggle="tooltip" title="mark as complete">
				<i class="fa fa-foursquare"></i> finished
			</a>
		</div> -->
	@endif
	@if(($utype==2 || $utype==1 || $utype==7) && $ap->app_status!=18)
		@include("app.grapError",array("appID"=>$id))
	@endif
	</div>
	@if($utype==2 || $utype==1 || $utype==7)
  		@include("app.uploadNote",array("ap"=>$ap,"id"=>$id))
  	@endif
  </div>
</div>
  
</div>
