<?php $x=0 ?>
<h3 class="capitalCase">status history</h3>
<table class="table table-condensed">
	<thead>
		<tr>
			<th>s.no</th>
			<th>status</th>
			<th>date time</th>
			<th>by user</th>
		</tr>
	</thead>
	<tbody>
@foreach($history as $hist)
		<tr>
			<td>{{++$x}}</td>
		<td class="capitalCase">
				@if($hist->status_id==99)
					{{'Uploaded'}}
				@elseif($hist->status_id==98)
					{{'ReUploaded'}}
				@elseif($hist->status)
					{{ucfirst($hist->status)}}
				@elseif(!$hist->status)
					{{ucfirst($hist->text_history)}}
				@endif
			</td>
			<td>{{date('d/m/Y',strtotime($hist->created_at))}}</td>
			<td>{{$hist->username}}</td>
		</tr>
@endforeach
	</tbody>
</table>
