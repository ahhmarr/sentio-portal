{{HTML::script("js/mainTable.js")}}
<div class="col-xs-4 pull-right">
<img id="loadingImageKey" class="invisible col-xs-2 top-buffer-large" src="{{asset('img/loading/3.gif')}}" width="20-pc" > 
<div class="col-xs-10">
	<input type="text" name="" id="watchFace" class="top-buffer-large form-control pull-right" placeholder="search" >
</div>
</div>
<input type="hidden" name="" id="watchBack" class="top-buffer-large" >
		<table class="pointer table table-compact"  data-filter="#watchBack" data-page-size="30" data-page-navigation=".pagination" id="mainTable" >
			<thead class="capitalCase">
				<tr>
					<th>#</th>
					@if($ut==1 || $ut==5 || $ut==7)
						<th class="width-1pc"></th>
					@endif
					<th class="sort" data-order="asc" data-col="app_name">Appname</th>
					<th class="sort" data-order="asc" data-col="project_name">Root project</th>
					<th class="sort" data-order="asc" data-col="account">account</th>
					<th class="sort" data-order="asc" data-col="bundlePrefix,bun">bundle ID</th>
					<th class="sort" data-order="asc" data-col="priority">priority</th>
					@if($upload_status_show)
					<th class="sort" data-order="asc" data-col="username">assigned to</th>
					@endif
					<th class="sort" data-order="asc" data-col="apple_id">apple ID</th>
					<th class="hidden-x"><span class="smallCase">i</span>Tunes URL</th>
					<th class="sort" data-order="asc" data-col="status">status</th>
					@if($upload_status_show)
						<th class="sort" data-order="asc" data-col="upStatus">upload status</th>
					@endif
					<th>action</th>
					<th class="sort" data-order="asc" data-col="is_live">live</th>
						<th class="sort" data-order="asc" data-col="updated_at">Modified</th>
				</tr>
			</thead>
			<tbody>
			<?php $c=0;$firstID=0; ?>
			@include("app.tablelist",compact("apps"))
			</tbody>
			@if($totalApps)
				<tfoot data-max="{{ceil($totalApps/$pageSize)}}" id="paginate" data-offset="0" data-limit="{{$pageSize}}">
	              <tr>
		              <td colspan="13">
		                      <div class="hidden-x pagination pagination-centered hide-if-no-paging"></div>
		                      <!--<button id="loadMore">More</button> -->
		                      <div class="customPagination">
		                              <button id="pagePrev" disabled="disabled">&lt;</button>
		                              <button id="pageNext"
		                              {{ceil($totalApps/$pageSize)<=1?"disabled":""}}>&gt;</button>
		                              <span id="" class="">
	                                      <span id="currentPage">1</span>
	                                      <span id=""> of </span>
	                                      <span id="totalPage"> {{ceil($totalApps/$pageSize)}} </span>
	                                      <img id="loadingImage" class="hidden-x" src="{{asset('img/loading/3.gif')}}" width="20-pc" >
		                              </span>
		                      </div>
		              </td>
	              </tr>
				</tfoot>
			@endif
		</table>
@include("app.edit",array('rp'=>$rp,'ac'=>$act,'pr'=>$pr,'root'=>$root,'ac'=>$account,'usr'=>$usr))
@include("app.clone",array('rp'=>$rp,'ac'=>$act,'pr'=>$pr,'root'=>$root,'ac'=>$account,'usr'=>$usr))