@extends("master")
@section("content")
<script type="text/javascript">
	$(function(){
		
	})
</script>
	<h2 class="capitalCase col-xs-offset-4 ">rep variable</h2>
	<div class="row top-buffer-large">
		<div class="col-xs-6">
			<div class="row">
					{{Form::open(array("url"=>"rep/save"))}}
						<div class="col-xs-3">
							<input type="text" name="table_name" placeholder="table name" class="axBlank form-control">
						</div>
						<div class="col-xs-3">
							<input type="text" name="column"  placeholder="column name" class="axBlank form-control">
						</div>
						<div class="col-xs-3">
							<input type="text" name="variable"  placeholder="variable" class="axBlank form-control">
						</div>
						<input type="submit" value="submit" class="btn btn-default hide-submit">
					{{Form::close()}}
			</div>
			<div class="row strong border-bottom capitalCase top-buffer-large padding-small">
				<div class="col-xs-3">variable name</div>
				<div class="col-xs-3">table</div>
				<div class="col-xs-3">column</div>
				<div class="col-xs-3">delete</div>
			</div>
			@foreach($rep as $u)
			<div class="row top-buffer-small cursor-pointer" data-userID="{{$u->id}}">
				<div class="col-xs-3">{{$u->variable}}</div>
				<div class="col-xs-3">{{$u->table_name}}</div>
				<div class="col-xs-3">{{$u->column}}</div>
				<div class="col-xs-3">
					<a href="rep/delete/{{$u->id}}" class="confirm">
						<span class="glyphicon glyphicon-remove-circle ui-text-red"></span>
						delete
					</a>
				</div>
			</div>
			@endforeach
		</div>
	</div>
@stop