<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	{{HTML::style("css/bootstrap.min.css")}}
	{{HTML::style("css/main.css")}}
	<title>under maintenance</title>
</head>
<body>
	<div class="col-xs-offset-2 top-buffer-largest">
		<img src="{{asset('img/maintain.png')}}" alt="under maintenance">
	</div>
</body>
</html>