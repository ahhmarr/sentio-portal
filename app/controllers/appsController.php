<?php
use Senportal\AppAction;
use Senportal\ItunesAccountManager;
use Senportal\Mailer\AppMailer;
use Senportal\StatusEventManager;
class AppsController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected $appHelper,$itunesAccount,$appMailer,$statusManager;
	public  function __construct(AppAction $appHelper,
		ItunesAccountManager $itunesAccount, AppMailer $appMailer
		,StatusEventManager $statusManager)
	{
		$this->beforefilter('auth');
		$this->appHelper=$appHelper;
		$this->itunesAccount=$itunesAccount;
		$this->appMailer=$appMailer;
		$this->statusManager=$statusManager;
		// $this->appMailer->maxStatusAccountWise();
	}
	public function index()
	{
		$ut=Auth::user()->account_type;
		$apps=DB::table("app as ap")
				->select("ap.*",'sku.bundle_id as bun',"rp.project_name","rp.free_n_pro",
					"it.account","it.bundlePrefix","usr.username","usr.name as 'nameUser'"
					,"us.color as usColor","us.upload_status as upStatus")
				->leftJoin("root_project as rp","ap.root_project_id","=","rp.id")
				->leftJoin("itunes as it","ap.itunes_account_id","=","it.id")
				->leftJoin("user as usr","ap.assign_to","=","usr.id")
				->leftJoin("app_sku_details as sku",function($join){
					$join->on("sku.app_id","=","ap.id");
					$join->on("sku.free_pro","=",DB::raw("1"));
				})
				->leftJoin("upload_status as us ","us.id","=","ap.upload_status")
				->leftJoin("statuses as stats","ap.app_status","=","stats.order")
				->whereNull("ap.deleted_at");
				
		if($ut==2 )
			{
				// app needs to be in upload or reupload queue too
				$apps=$apps->where("ap.assign_to","=",Auth::user()->id)
							->where(function($query)
							{
								$query->where("ap.app_status",3)
									->orWhere("ap.app_status",19);
							});
			}
		// app should be assigned to any upload user
		if($ut==4 )
			{
				$apps=$apps->where("usr.account_type","=","2");
			}
		if($ut==6 )//art user
			{
				// $apps=$apps->where("ap.app_status","=","17");

			}
		$filter=Apps::filter($apps);
		$apps=$filter["object"];

		// filter ends
		//removing pending assig and dev status for normal table
		if(!$filter["FilterParams"]["devFilter"])
			{
				$apps=$apps->where("ap.app_status","!=",16)
							->where("ap.app_status","!=",17);
			}
		//sorting accoriding to their date if no filter is provided
		if(!$filter["FilterParams"]["fSort"])
			$apps=$apps->orderBy("ap.id","desc");
		$usr=User::where('account_type','=','2')
				->orWhere("account_type","=","3")
				->get();
		$totalApps=$apps;
		// v1.11 accout coloring depending upon the choice
		$itunesAccount=($this->itunesAccount->getAccountWithColors())
						?$this->itunes_select_account_specific()
						:$this->itunes_select();
		$accounts=Itune::whereNull('deleted_at')
						->orderBy('order')
						->orderBy('account')
						->get();
		$data=array(
			'rp'=>$this->_selectRoot(),
			'pr'=>$this->_priority(),
			'act'=>$itunesAccount,
			'apps'=>$apps,
			'usr'=>$usr,
			'root'=>RootProject::whereNull("deleted_at")->get(),
			'rootFilter'=>$this->_selectRoot("filterRP","filterRP"),
			'account'=>$accounts,
			"status"=>$this->status(),
			'ituneMax'	=> $this->ituneMax(),
			'ut'		=> $ut,
			'totalApps'		=> count($totalApps->get())
			
			);
		$data=array_merge($data,$filter["FilterParams"]);
		//v1.8 for upload status cell display 
		@$filtr=$filter['FilterParams']['filterR'];
		$data['upload_status_show']=false;
		if($filtr=="filterQueue" || $filtr=="filterReQueue" || 
			$filtr=="filterGraphic" || $filtr=="filterFlight" || $filtr=="filterUpl" 
			|| $ut==2)
		{
			$data["upload_status_show"]=true;
		}
		
		// for ajax
		$ajax=Input::get("isAjax");
		$key=Input::get("key");
		$stFilter=Input::get("status_filter");
		$offset=Input::get("offset");
		$limit=Input::get("limit");
		$pageSize=30;
		//when it users presees assign to me
		if($stFilter)
		{
			$stf=explode("|",$stFilter);
			$apps=$apps->where(function($query) use($stf)
			{
				foreach($stf as $v)
				{
					$query->orWhere("ap.app_status",$v);
				}
			});
			
		}
		if($ajax)
		{
			
			if($key)
			{
				// $apps=$apps->where("ap.app_name","LIKE","%$key%");
				$apps=$apps->where(function($query) use ($key)
				{
					$query->orWhere("ap.app_name","LIKE","$key")
						  ->orWhere("ap.apple_id","LIKE","%$key%")
						  ->orWhere("ap.graphics","LIKE","%$key%")
						  ->orWhere("ap.app_info_name","LIKE","%$key%");
				});
			}
			elseif(!$key && !$stFilter)
			{
				$apps=$apps->skip($offset)->take($limit);
			}
			$data["apps"]=$apps->get();
			return View::make("app.tablelist",$data);
		}
		
		$data["pageSize"]=$pageSize;
		// return everything if devfilter is sleected
		if($data["devFilter"])
		{
			$apps=$apps->get(); //only 30 records

		}
		else
		{
			$apps=$apps->take($pageSize)->get(); //only 30 records

		}
		$data["apps"]=$apps;
		return View::make("app.index")->with($data);

	}
	
	public function save()
	{

		$nItune=Input::get("itunes_account_id");
		$appName=Input::get("app_name");
		if(!$appName)
			return $this->index();
		$appStatus=Input::get("appStatus");
		$rp=RootProject::where("id","=",Input::get("root_project_id"))->first();
		$rc=ProjectCell::where("project_id","=",Input::get("root_project_id"))
						 ->get();
		$iap=Iap::where("project_id","=",Input::get("root_project_id"))->get();
		$it=Itune::where("id","=",Input::get("itunes_account_id"))->first();
		// $bundleID=$this->appToBundle($appName);
		$appName='';
		$apNm=$this->duplicateName(Input::get("app_name"));
		/*no bundle id should be same if the name has a no then 
		so does the bundle id*/
		$bundleID=$this->appToBundle($apNm);

		$apSt=($appStatus)?$appStatus:0.1; //if no explicit status given then add app
		$apSt=(!$nItune)?17:$apSt; //dev if no accoutns specified ie unassigned
		$data=array(
			'app_name'=>$apNm,
			'root_project_id'=>Input::get("root_project_id"),
			"priority"=>Input::get("priority")?:0,
			"itunes_account_id"=>Input::get("itunes_account_id"),
			"assigned_user_id"=>Input::get("assigned_user_id"),
			"assign_to"=>Input::get("assign_to")?:0,
			"apple_id"=>Input::get("apple_id"),
			"itunes_url"=>Input::get("itunes_url"),
			"rows"=>$rp->rows,
			"cols"=>$rp->cols,
			"report_text"=>$rp->report_text,
			"app_status"=>$apSt
			);
			$r=Apps::create($data);
			//adding app status history
			$itID=Input::get("itunes_account_id");
			$appID=$r->id;
			$this->change_app_status($apSt,$r->id);
			//copying project cell values to app cells
			$value=array();
			foreach ($rc as $r2) 
			{
				
				$value[]=array(
						"app_id"	=>$r->id,
						"row_index"	=>$r2->cell_row,
						"col_index"	=>$r2->cell_col,
						"value"		=>$r2->value,
						"style"		=>($r2->style)?$r2->style:'color:#000;'
							);
			}
		//creating gku details
			$sku=array(
						array(
							"app_id"=>$r->id,
							"app_name" =>$appName,
							"sku" =>'.sku',
							"bundle_id"=>$bundleID,
							"price_tier"=>"free",
							"enable_iad"=>"yes"
							),
						array(
							"app_id"=>$r->id,
							"app_name" =>"$appName Pro",
							"sku" =>'Pro.sku',
							"bundle_id"=>$bundleID,
							"price_tier"=>"1",
							"enable_iad"=>"no",
							"free_pro" =>2
							)
						);
		//game center details
			$appName2=Input::get("app_name");
			$game=array(
				array(
						"app_id"=>$r->id,
						"app_name" =>$appName,
						"group_name" =>$appName2.' Group',
						"lead_name" =>".Leaderboard",
						"lead_id" =>".Leaderboard",
						"lang" =>"$appName2 Leaderboard"
					),
				array(
						"app_id"=>$r->id,
						"app_name" =>"$appName Pro",
						"group_name" =>$appName2.' Group',
						"lead_name" =>".Leaderboard",
						"lead_id" =>".Leaderboard",
						"lang" =>"$appName2 Leaderboard",
						"free_pro" =>2
					));
		
		//iap details
			$iapDet=array();
			foreach ($iap as $i) 
			{
				$iapDet[]=array(
						"app_id"=>$r->id,
						"app_name" =>"$appName",
						"type" =>$i->type,
						"ref_name" =>$this->appToBundle($i->product_id),
						"prod_id" =>$this->appToBundle($i->product_id),
						"price_tier" =>"$i->price_tier",
						"dis_name" =>($i->display_name),
						"desc" =>"$i->description"
					);
				$iapDet[]=array(
						"app_id"=>$r->id,
						"app_name" =>"$appName Pro",
						"type" =>$i->type,
						"ref_name" =>$this->appToBundle($i->product_id),
						"prod_id" =>$this->appToBundle($i->product_id),
						"price_tier" =>"$i->price_tier",
						"dis_name" =>"$i->display_name",
						"desc" =>"$i->description",
						"free_pro" =>2
					);
			}
		$this->_specialVars($appID,$itID);
		DB::transaction(function() use($data,$sku,$game,$iapDet,$value,$itID,$appID){
			foreach ($sku as $sk) {
				SkuDetails::create($sk);
			}
			foreach ($game as $gm) {
				GameDetails::create($gm);
			}
			foreach ($iapDet as $iap) {
				AppIap::create($iap);
			}
			foreach ($value as $v) {
				AppReportCells::create($v);
			}
		});
		// v1.8 delete iap 
		$this->deletIapIfNotInherit($nItune,$appID);
		//v1.8.3 cloning graphic details if its a clone app
		$isClone=Input::get("clone");
		if($isClone)
		{
			$cloneAppID=Input::get("cloneAppID");
			//copying graphic details
			$cloneDetails=["graphic_designer","gd_assign","graphic_assign",
							"graphics","aso_resch","resch_assigned","aso",
							"app_info_name","app_pro_info_name",
							"app_info_complete"];
			$origApp=Apps::find($cloneAppID);
			if($origApp)
				{
					foreach($cloneDetails as $key)
					{
						$r->$key=$origApp->$key;
					}
					$r->save();

				}
		}
		//mailing for redundant statuses
		$this->checkMaxAppStatus($r);
		return Redirect::to("app");
	}
	public function deletIapIfNotInherit($itunesID,$appID)
	{
		$it=Itune::find($itunesID);
		if($it)
		{
			if(!$it->iap_inherit)
			{
				AppIap::where("app_id",$appID)->delete();
			}
		}
	}
	public function del($id)
	{
		Apps::find($id)
				->delete();
		AppStatusHistory::where("app_id","=",$id)->delete();
		return Redirect::to("app")->with("success","app deleted successfully");
	}
	public function view()
	{
		$id=Input::get("appID");
		$ap=Apps::leftJoin("user","app.assign_to","=","user.id")
				->leftJoin("itunes","app.itunes_account_id","=","itunes.id")
				->where("app.id","=",$id)
				->first();
		$app=Apps::find($id);
		$rp=RootProject::withTrashed()->where('id','=',$ap->root_project_id)->first();
		//only those ulpload user who has corrsponding itunes
		$UUser=User::select(DB::raw("distinct user.id"),"user.*")
					->leftjoin("upload_itunes as ui",function($join) use($ap){
						$join->on("ui.upload_id","=","user.id");
						$join->on("ui.itunes_id","=",DB::raw($ap->itunes_account_id));
					})
					->where('user.account_type','=','2')
					->where("ui.email","!=","")
					->get();
		//getting all the iTunes user
		$it=User::where("account_type","=","3")
				->get();
		$iap=$app->iap()
				->select("*",
						DB::raw("case when app_iap_details.type='1' then 'Non-Consumable' else 'Consumable' end as 'con'"))
				->orderBy("free_pro","asc")
				->get();
		$vl=$this->appReportCellGet($id);
		$status=$this->status();
		$sap=$app->sku()->orderBy("free_pro","asc")->first();
		$history=AppStatusHistory::select("app_status_history.*","st.status","usr.username",DB::raw("app_status_history.status as status_id"))
									->leftjoin("statuses as st ","st.order","=","app_status_history.status")
									->leftjoin("user as usr","usr.id","=","app_status_history.user_id")
									->where("app_status_history.app_id","=",$id)
									->whereNull("st.deleted_at")
									->whereNull("usr.deleted_at")
									->get();
		$sdk=MainXl::select("xl_sdk.id","xl_sdk.sdk_name")
					->leftJoin("xl_sdk","xl_main.sdk_id","=","xl_sdk.id")
					->leftJoin("app","app.free_sdk","=","xl_main.sdk_id")
					->leftJoin("app as app2","app2.pro_sdk","=","xl_main.sdk_id")
					->where("xl_main.root_project_id",$ap->root_project_id)
					->where(function($query){
						$query->whereNull("app.id");
						$query->whereNull("app2.id");
					})
					->whereNull("app.id")
					->whereNull("app2.id")
					->groupBy("xl_main.sdk_id")
					->get();
		$sdkHeading=MainXl::select("xl_main.heading_id","xl_heading.header_name")
							->join("xl_heading","xl_heading.id","=","xl_main.heading_id")
							->where("xl_main.root_project_id",$ap->root_project_id)
							->groupBy("xl_main.heading_id")
							->get();
		$sdkRow=array();
		
		$pro_sdk=$ap->pro_sdk;
		$free_sdk=$ap->free_sdk;

		$sdkRows=MainXl::select("xl_main.*","xl_sdk.sdk_name")
							->leftJoin("xl_sdk","xl_sdk.id","=","xl_main.sdk_id")
							->where("xl_main.root_project_id",$ap->root_project_id)
							->where(function($query)use ($pro_sdk,$free_sdk){
								$query->where("xl_main.sdk_id",$free_sdk);
								if($pro_sdk)
								{
									$query->orWhere("xl_main.sdk_id",$pro_sdk);
								}
							})
							->get();
		//setting the rows to proper heading order
		
		foreach($sdkRows as $row)
			{
				foreach($sdkRows as $col)
				{
					if($row->heading_id==$col->heading_id)
						$sdkRow[$row->sdk_id][$col->heading_id]=$row->value;
				}
			}

			
		// v1.10 show only those users in root project default users lists
		$uploadUsersU=$this->getUploadUsers($rp,$ap);
		$data=array(
			"id" 			=> $id,
			"sku"			=> $app->sku()->orderBy("free_pro","asc")->get(),
			"game"			=> $app->game()->orderBy("free_pro","asc")->get(),
			"iap"			=> $iap,
			"ap"			=> $ap,
			"rp"			=> $rp,
			"UUser"			=> $UUser,
			"uploadUsersU"	=> $uploadUsersU,
			"apc"			=> str_replace(" ", "", $sap->bundle_id),
			"apRep"			=> $vl["value"],
			"sty" 			=> $vl["style"],
			"status"		=> $status,
			"it"			=> $it,
			"history"		=> $history,
			"sdk"			=> $sdk,
			"utype"			=> Auth::user()->account_type,
			"sdkHeading"	=> $sdkHeading,
			"sdkRows"		=> $sdkRows,
			"sdkRow"		=> $sdkRow,
			"sdkName"		=> array(
								SDKXl::where("id",$free_sdk)->pluck("sdk_name"),
								SDKXl::where("id",$pro_sdk)->pluck("sdk_name")
								)
			);
		return View::make("app.view")->with($data);
	}
	function itunes_select($unassign=0)
	{
		$it=Itune::whereNull('deleted_at')
						->orderBy('order')
						->orderBy('account')
						->get();
		$pr=RootProject::get();
		$status=$this->status();
		$proj_color=array();
		$html='<select name="itunes_account_id" class="form-control axBlank custom hidden-x"
			  data-status="default">';
			foreach ($it as $i) {
				$html.='<option value="'.$i->id.'">'.$i->account.'</option>';
			}
		$html.='</select>';
		$proj_color["default"]=$html;
		foreach ($pr as $p) {
			$m=$this->tot($p->id);
			$mx=$m[1];
			$html='<select name="itunes_account_id" 
			class="form-control axBlank custom hidden-x" 
					data-projID="'.$p->id.'" data-status="proj">';
			$html.=(!$unassign)?"<option value='0'>unassigned</option>":"";
			foreach ($it as $i) {
				$order=(isset($mx[$i->id]["max"]))?$mx[$i->id]["max"]:0;
				$mx[$i->id]["max"]=(isset($mx[$i->id]["max"]))?
									$mx[$i->id]["max"]:0;
				$color=($i->full)?'#000;color:#fff;':$status[$order]["color"];
				$txt="";
				if($order==5)
				{
					$o=Apps::select(DB::raw("count(id) as 'tot' "))
						->whereItunesAccountId($i->id)
						->whereAppStatus($order)
						->whereRootProjectId($p->id)
						->first();
					$txt=" ----- ".$o->tot;
				}
				$html.='<option value="'.$i->id.'" style="background-color:'.$color.'">';
				$html.=$i->account.$txt;
				$html.="</option>";
				/*look for pending assignment as if the app is in pending assignment 
				for a root project then the color will not be set to add app as 
				pending assignment status is 16 */
			}
			$html.='</select>';
			$proj_color[$p->id]=$html;
		}
		
		return $proj_color;
	}
	function itunes_select_account_specific($unassign=0)
	{
		
		$pr=RootProject::get();
		$proj_color=array();
		$itunes=Itune::whereNull('deleted_at')
					->orderBy('order')
					->orderBy('account')
					->get();
		$html='<select name="itunes_account_id" class="form-control axBlank custom hidden-x"
			  data-status="default">';
			foreach ($itunes as $i) {
				$html.='<option value="'.$i->id.'">'.$i->account.'</option>';
			}
		$html.='</select>';
		$proj_color["default"]=$html;
		foreach ($pr as $p) {
			// getting colored itunes account
			$select=$this->getItunesAccountColorsForAccountSpecific($p->id);
			$html='<select name="itunes_account_id" 
			class="form-control axBlank custom hidden-x" 
					data-projID="'.$p->id.'" data-status="proj">';
			$html.=$select."</select>";
			$proj_color[$p->id]=$html;
		}
		return $proj_color;
	}

	function getItunesAccountColorsForAccountSpecific($projectID)
	{
		$statuses=[0.1,1,2,3,19,4,20,21,6,8,9];
		$itunes=Itune::whereNull('deleted_at')
						->orderBy('order')
						->orderBy('account')
						->get();
		$color="";$select="<option value=''>Unassigned</option>";
		foreach ($itunes as $itune) {
			$isApp=Apps::whereItunesAccountId($itune->id)
				->where(function($query) use ($statuses){
					foreach($statuses as $status)
					{
					$query=$query->orWhere("app_status",$status);
					}
				})->first();
		//v1.13 if app store notice is yes then yellow for all green
			$red="#ff0102";$yellow="#f2e800";$green="#26a522";
			if($isApp || 
				$this->_rootProjectHasAppWithStatus($projectID,7,$itune->id))
			{
				$color=$red;
			}
			else if(!$isApp && $itune->app_notice){
				$color=$yellow;
			}
			else{
				$color=$green;
			}
			$select.='<option value="'.$itune->id.'" 
						style="background-color:'.$color.'">';
			$select.=$itune->account;
			$select.="</option>";
		}
		return $select;
	}

	/*to check whether a root project has 
	an app with the specified status*/
	function _rootProjectHasAppWithStatus($rpID,$status,$itunesID=null)
	{
		
		$apps=Apps::whereRootProjectId($rpID)
					->whereAppStatus($status);
		$apps=($itunesID && $apps)?$apps->whereItunesAccountId($itunesID):$apps;
		$apps=$apps->count();
		return $apps;
	}
	function itunes_select_account($unassign=0)
	{
		$it=Itune::get();
		$pr=RootProject::get();
		$status=$this->status();
		$proj_color=array();
		$html='<select name="itunes_account_id" class="form-control axBlank custom hidden-x"
			  data-status="default">';
			foreach ($it as $i) {
				$html.='<option value="'.$i->id.'">'.$i->account.'</option>';
			}
		$html.='</select>';
		$proj_color["default"]=$html;
		foreach ($pr as $p) {
			$m=$this->tot($p->id);
			$mx=$m[1];
			$html='<select name="itunes_account_id" 
			class="form-control axBlank custom hidden-x" 
					data-projID="'.$p->id.'" data-status="proj">';
			$html.=(!$unassign)?"<option value='0'>unassigned</option>":"";
			foreach ($it as $i) {
				$order=(isset($mx[$i->id]["max"]))?$mx[$i->id]["max"]:0;
				$mx[$i->id]["max"]=(isset($mx[$i->id]["max"]))?
									$mx[$i->id]["max"]:0;
				$color=($i->full)?'#000;color:#fff;':$status[$order]["color"];
				$txt="";
				if($order==5)
				{
					$o=Apps::select(DB::raw("count(id) as 'tot' "))
						->whereItunesAccountId($i->id)
						->whereAppStatus($order)
						->whereRootProjectId($p->id)
						->first();
					$txt=" ----- ".$o->tot;
				}
				$html.='<option value="'.$i->id.'" style="background-color:'.$color.'">';
				$html.=$i->account.$txt;
				$html.="</option>";
				/*look for pending assignment as if the app is in pending assignment 
				for a root project then the color will not be set to add app as 
				pending assignment status is 16 */
			}
			$html.='</select>';
			$proj_color[$p->id]=$html;
		}
		
		return $proj_color;
	}
	public function getUploadUsers($rp,$ap)
	{
		$itunesID=$ap->itunes_account_id;
		$uploadUser=User::whereAccountType(2);
		if($rp->default_user)
			$uploadUser=$uploadUser->whereId($rp->default_user);
		$uploadUser=$uploadUser->get();
		foreach($uploadUser as $up)
		{
			$up->hasEmail=false;
			$hasEmail=UploadItunes::whereUploadId($up->id)
								->whereItunesId($itunesID)
								->first();
			if($hasEmail)
			{
				if($hasEmail->email)
					$up->hasEmail=true;
			}
		}
		return $uploadUser;
	}
	public function changeApp()
	{
		$appID=Input::get("appID");
		if(Input::get("app_name"))
		{
			$ap=Apps::where("id","=",$appID)->first();
			$ap->app_name=Input::get("app_name");
			$ap->save();
		}
		if(Input::get("field"))
		{
			$field=Input::get("field");
			foreach ($field as $fld) {
				$part=explode("_~@~_",$fld);
				$table=$part[0];
				$id=$part[1];
				$col=$part[2];
				$val=$part[3];
				DB::table($table)
					->where("id",$id)
					->update(array($col=>$val));
			}
		}
		$app=Apps::where("id","=",$appID)->first();
		$app->save();
		// dd(Input::get());
		return Redirect::to("app");
	}
	protected function _selectRoot($name="root_project_id",$id="rp_select")
	{
		$rootProject=RootProject::where('enable','=','1')
								->orderBy('primary','DESC')
								->orderBy('project_name','ASC')
								->get();
		$divider=true;
		$blank=($id=="rp_select")?"axBlank":"";
		$html="<select name=\"$name\" class=\"form-control $blank\" id=\"$id\">";
		$html.="<option value=''>root project</option>";
		foreach ($rootProject as $r) {
			$selected=(Input::get("filterRP")==$r->id && $id!="rp_select")?"selected":"";
			if(!$r->primary && $divider)
			{
				$html.='<option value="">----------------</option>';
				$divider=false;
			}
			$html.="<option value='$r->id' $selected>$r->project_name</option>";
		}
		$html.="</select>";
		return $html;
	}
	protected function _priority()
	{
		$html='<select name="priority" class="form-control hidden-x">';
		$html.="<option value=''>priority</option>";
		for($x=1;$x<=5;$x++)
			$html.='<option value="'.$x.'">'.$x."</option>";
		$html.="</select>";
		return $html;
	}
	protected function _select($table,$show,$default,$attrib="")
	{
		$db=DB::table($table)->select("id","$show as show")->whereNull('deleted_at')->get();
		$html="<select $attrib>";
		$html.="<option value=''>$default</option>";
		foreach ($db as $d) {
			$html.='<option value="'.$d->id.'">'.$d->show."</option>";
		}
		$html.="</select>";
		return $html;
	}
	public function appCellGet($appID)
	{
		$ap=AppCells::where("app_id","=",$appID)->get();
		$ac=array();
		$row=$col=0;
		foreach ($ap as $row) {
			$ac[$row->attrib][$row->col_index]=$row->value;
		}
		return $ac;
	}
	protected function appReportCellGet($appID)
	{
		$ap=AppReportCells::where("app_id","=",$appID)->get();
		$ac=array('value'=>array(),'style'=>array());
		$row=$col=0;
		foreach ($ap as $row) {
			$ac["value"][$row->row_index][$row->col_index]=$row->value;
			$ac["style"][$row->row_index][$row->col_index]=$row->style;
		}
		return $ac;
	}
	function changeReportCell()
	{
		$appID=Input::get("appID");
		$rowIndex=Input::get("rowIndex");
		$colIndex=Input::get("colIndex");
		$appCell=AppReportCells::where("app_id","=",$appID)
						  ->where("row_index","=",$rowIndex)
						  ->where("col_index","=",$colIndex)
						  ->first();
		if($appCell)
			{
				$appCell->value=Input::get("value");
				$appCell->style=Input::get("style");
				$appCell->save();
			}
		else
		{
			AppReportCells::insert(array(
					"app_id"=>$appID,
					"row_index"=>$rowIndex,
					"col_index"=>$colIndex,
					"value"=>Input::get("value")
				));
		}
	}
	function exportToXl($action=null)
	{
		$objPHPExcel = new PHPExcel();
		$app=Input::get("app");
		$c=0;$d=1;
		$sheet=array();
		foreach ($app as $appID) 
		{
			$d++;
			$ap=Apps::leftJoin("itunes","app.itunes_account_id","=","itunes.id")
				->leftJoin("root_project","app.root_project_id","=","root_project.id")
				->where("app.id","=",$appID)->first();
				//naming the sheet
			
			$sheetName='AppID - '.$ap->account;
			// $bund=str_replace(" ","","$ap->bundlePrefix.$ap->app_name");
			$sy=SkuDetails::where("app_id",$appID)
							->where("free_pro","1")
							->pluck("bundle_id");
			$bund=$ap->bundlePrefix.".".$sy;
			if(array_search($sheetName, $sheet)===false)
			{
				
				$myWorkSheet = new PHPExcel_Worksheet($objPHPExcel,$sheetName);
	        	$objPHPExcel->addSheet($myWorkSheet,$c++);	
				$sheet[]=$sheetName;
			}
			
			//heading
	        $objPHPExcel->setActiveSheetIndexByName($sheetName)
	        			->setCellValue('B1', 'Name len<30')
			            ->setCellValue('C1', 'Progress')
			            ->setCellValue('D1', 'Bundle ID');
			$objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
	        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
	        $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
	        $row=$objPHPExcel->getActiveSheet()->getHighestDataRow()+1;	
	        $objPHPExcel->getActiveSheet()
	        			->setCellValue("A$row",$ap->app_name)
	        			->setCellValue("B$row",strlen($ap->app_name))
	        			->setCellValue("C$row","x")
	        			->setCellValue("D$row",$bund)
	        			->setCellValue("F$row","x")
	        			->setCellValue("G$row",$ap->app_name." Dev")
	        			->setCellValue("H$row","x")
	        			->setCellValue("I$row",$ap->app_name." AdHoc")
	        			->setCellValue("J$row","x")
	        			->setCellValue("K$row",$ap->app_name." AppStore");
	        if($ap->free_n_pro)
	        {
				$d++;
				$proName=str_replace(" ", "","$ap->app_name Pro");
				$sx=SkuDetails::where("app_id",$appID)
							->where("free_pro","2")
							->first();
				$bund=$ap->bundlePrefix.".".trim($sy).trim($sx->app_name);
	        	$objPHPExcel->getActiveSheet()
	        			->setCellValue("A".($row+1),$ap->app_name.' pro')
	        			->setCellValue("B".($row+1),(strlen($ap->app_name)+4))
	        			->setCellValue("C".($row+1),"x")
	        			->setCellValue("D".($row+1),$bund)
	        			->setCellValue("F".($row+1),"x")
	        			->setCellValue("G".($row+1),$ap->app_name." Pro Dev")
	        			->setCellValue("H".($row+1),"x")
	        			->setCellValue("I".($row+1),$ap->app_name." Pro AdHoc")
	        			->setCellValue("J".($row+1),"x")
	        			->setCellValue("K".($row+1),$ap->app_name." Pro AppStore");
	        }

        
		}
		$objPHPExcel->setActiveSheetIndexByName($sheetName)
	        			->setCellValue('A1', $d);
		$objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $fileName="appID".date("y-m-d-h-i-s").".xls";
		$objWriter->save(base_path()."/public/docs/$fileName");
		// $file = base_path().'/public/docs/hello.xls'; 
		return Response::download("docs/$fileName");

	}
	public function exportToXlSDK()
	{
		$objPHPExcel = new PHPExcel();
		$app=Input::get("app");
		$c=0;
		$objPHPExcel->getActiveSheet()
	        			->setCellValue("B1","Revmob")
	        			->setCellValue("C1","Chartboost id")
	        			->setCellValue("D1","Chartboost sig")
	        			->setCellValue("E1","flurry")
	        			->setCellValue("F1","askingpoint")
	        			->setCellValue("G1","Playhaven Token")
	        			->setCellValue("H1","Playhaven Secret")
	        			->setCellValue("I1","Admob")
	        			->setCellValue("J1","AdColony ID")
	        			->setCellValue("K1","AdColony Key")
	        			->setCellValue("L1","TapJoy Connect")
	        			->setCellValue("M1","TapJoy Secret")
	        			->setCellValue("N1","AppFlood Key ** Bundle Ids in AA")
	        			->setCellValue("O1","AppFlood Sec")
	        			->setCellValue("AA1","Package Name");
	        			///
		$objPHPExcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("C1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("D1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("E1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("F1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("G1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("H1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("I1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("J1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("K1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("L1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("M1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("N1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("O1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("AA1")->getFont()->setBold(true);
	        			//
		foreach ($app as $appID) {
			$ap=Apps::leftJoin("itunes","app.itunes_account_id","=","itunes.id")
						->leftJoin("root_project","app.root_project_id","=","root_project.id")
						->where("app.id","=",$appID)->first();
			$row=$objPHPExcel->getActiveSheet()->getHighestRow()+1;	
			$objPHPExcel->getActiveSheet()
	        			->setCellValue("A$row",$ap->app_name)
	        			->setCellValue("B$row","x")
	        			->setCellValue("C$row","x")
	        			->setCellValue("D$row","x")
	        			->setCellValue("E$row","x")
	        			->setCellValue("F$row","x")
	        			->setCellValue("G$row","x")
	        			->setCellValue("H$row","x")
	        			->setCellValue("I$row","x")
	        			->setCellValue("J$row","x")
	        			->setCellValue("K$row","x")
	        			->setCellValue("L$row","x")
	        			->setCellValue("M$row","x")
	        			->setCellValue("N$row","x")
	        			->setCellValue("O$row","x")
	        			->setCellValue("AA$row",$this->appToBundle($ap->app_name,$ap->bundlePrefix));
	       if($ap->free_n_pro)
	       {
			$row=$objPHPExcel->getActiveSheet()->getHighestRow()+1;	
	       	$ap->app_name.=" Pro";
	       	$objPHPExcel->getActiveSheet()
	        			->setCellValue("A$row",$ap->app_name)
	        			->setCellValue("B$row","x")
	        			->setCellValue("C$row","x")
	        			->setCellValue("D$row","x")
	        			->setCellValue("E$row","x")
	        			->setCellValue("F$row","x")
	        			->setCellValue("G$row","x")
	        			->setCellValue("H$row","x")
	        			->setCellValue("I$row","x")
	        			->setCellValue("J$row","x")
	        			->setCellValue("K$row","x")
	        			->setCellValue("L$row","x")
	        			->setCellValue("M$row","x")
	        			->setCellValue("N$row","x")
	        			->setCellValue("O$row","x")
	        			->setCellValue("AA$row",$this->appToBundle($ap->app_name,$ap->bundlePrefix));
	       }
		}
		$objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $fileName="SDK".date("y-m-d-h-i-s").".xls";
		$objWriter->save(base_path()."/public/docs/$fileName");
		// $file = base_path().'/public/docs/hello.xls'; 
		return Response::download("docs/$fileName");
	}
	// this will return botht the full qulified bundle id or just the name in camel case
	private function appToBundle($appName,$bundlePrefix=null)
	{
		// $appName=ucwords($appName);
		$appName=trim($appName);
		$appName=str_replace(" ", "", $appName);
		return ($bundlePrefix)?$bundlePrefix.'.'.$appName:$appName;

	}
	function assign()
	{
		$assignTo=Input::get("assign_to");
		$reupload=Input::get("reupload");
		/*$freeSDK=Input::get("free_sdk");
		$proSDK=Input::get("pro_sdk");*/
		$appID=Input::get("appID");
		$ap=Apps::where("id","=",$appID)->first();
		$usr=User::where("id","=",$assignTo)->first();
		//set status for 2 only in the case of upload user
		if($usr->account_type==2)
			{
				$ap->app_status=(!$reupload)?3:19;
				$this->change_app_status((!$reupload)?3:19,$appID);//reupload if checked
				/*saving upload user ID separately for re assigning once done with graphic 
				fixed*/
				$ap->upload_user=$assignTo;
				
			}
		$ap->assign_to=$assignTo;
		$ap->unassign=0;
		$ap->save();
		if($usr->account_type==2)
		{
			// sending mail to upload user on assignment ie app set to integrate
			$this->appHelper->sendEmailOnStatusChange(3,$appID,1);
		}
		//trying to save $UPLOADER_EMAIL
		$app=AppReportCells::where("app_id","=",$appID)
					->where("value","=",'$UPLOADER_EMAIL')
					->first();
		if($app)
		{
			//getting the email of the corresponding itunes user for that upload user
			$itunesID=$ap->itunes_account_id;
			$uploadID=$assignTo;
			$up=UploadItunes::where("upload_id","=",$uploadID)
						  ->where("itunes_id","=",$itunesID)
						  ->first();	
			$app->value=$up->email;
			$app->save();
			
		}	
		Session::set("success","successfully assigned");
	}
	//this will update appdetails
	function update()
	{
		$ap=Apps::find(Input::get("appID"));
		$ap->root_project_id=Input::get("root_project_id");
		$ap->itunes_account_id=Input::get("itunes_account_id");
		$ap->app_name=Input::get("app_name");
		$ap->assign_to=Input::get("assignedTo");
		$ap->auto_process=Input::get("autoProcess");
		//find user type
		if(Input::get("assignedTo"))
		{
			$user=User::where("id","=",Input::get("assignedTo"))->first();
			$newStatus=($user->account_type==2)?3:0.1;
			//trying to avoid repetive status changes & delete deleted status
			if($ap->app_status!=$newStatus && ($ap->app_status!=8 && $ap->app_status!=9))
			{
				$ap->app_status=$newStatus;
				$this->change_app_status($ap->app_status,Input::get("appID"));
			}
			Debugbar::addMessage($user->account_type);
			if($user->account_type==2)
			{
				/*saving upload user ID separately for re assigning once done with graphic 
				fixed*/
				$ap->upload_user=Input::get("assignedTo");
			}
		}
		$ap->assing_date=date('Y-m-d h:i:s');
		$ap->apple_id=Input::get("appleID");
		$ap->itunes_url=Input::get("itunesURL");
		$ap->Papple_id=(Input::get("PappleID"))?Input::get("PappleID"):0;
		$ap->Pitunes_url=(Input::get("PitunesURL"))?Input::get("PitunesURL"):'';
		$ap->priority=Input::get("priority");
		$ap->unassign=0;
		$ap->save();
		$this->_specialVars($ap->id,$ap->itunes_account_id);
		//delting iaps if the itunes account has iap to No v1.8
		$it=Itune::find($ap->itunes_account_id);
		if($it)
		{
			if(!$it->iap_inherit)
			{
				AppIap::where("app_id",$ap->id)->delete();
			}
		}
		return Redirect::to('app')->with("success","successfully updated");

	}
	function complete($id)
	{
		$ap=Apps::find($id);
		$st=$ap->app_status;
		$ap->priority=0;
		$ap->assign_to=0;
		$ap->app_status=4;
		// updating status to uplaoded or reuploaded 
		$this->change_app_status(($st!=19)?'99':'98',$id);
		//adding app status history to In review
		$this->change_app_status($ap->app_status,$id);
		$ap->save();
		return Redirect::to('app');

	}
	function inComplete($id)
	{
		$ap=Apps::find($id);
		$ap->app_status=0;
		//adding app status history
		$this->change_app_status($ap->app_status,$id);
		$ap->save();
		return Redirect::to('app');

	}
	public function unassign()
	{
		$id=Input::get("appID");
		$appleID=Input::get("appleID");
		$itunesURL=Input::get("itunesURL");
		$PappleID=(Input::get("PappleID"))?Input::get("PappleID"):0;
		$PitunesURL=(Input::get("PitunesURL"))?Input::get("PitunesURL"):'';
		$provProf=Input::get("provProf");
		if($provProf)
			$provProf="https://".$provProf;
		$fbID=Input::get("fbID");
		$nxtPeer=Input::get("nxtPeer");
		$nxtSSo=Input::get("nxtSSo");
		//unaasigning iTunes user account from an app
		$ap=Apps::where("id","=",$id)->first();
		//if delete than deleted else waiting for upload
		// v1.8
		$upload_status=false;
		if($ap->app_status==8) //delete
			$ap->app_status=9; //deleted
		elseif($ap->app_status==1) // metadata
		{
			$ap->app_status=19; //reupload queue
			$ap->upload_status=1; //integrate
			$upload_status=true;
			// v1.9 send mail to users since integrate and reupload queue
			$this->appHelper
			->sendEmailOnStatusChange(19,$id,1);
		}
		else
			$ap->app_status=2;//waiting for upload
		//adding app status history
		$this->change_app_status($ap->app_status,$id);
		if($upload_status)
		{	
			$this->change_app_status(0,$id,"Upload Status : Integrate");
		}
		$ap->app_status=($ap->app_status!=8 || $ap->app_status!=9)
						?$ap->app_status:9; 
		$ap->prov_profile=$provProf;
		$ap->nxt_key=$nxtPeer;
		$ap->nxt_sso=$nxtSSo;
		$ap->fbID=$fbID;
		$ap->assign_to=$ap->upload_user;
		$ap->apple_id=$appleID;
		$ap->itunes_url=$itunesURL;
		$ap->Papple_id=$PappleID;
		$ap->Pitunes_url=$PitunesURL;
		$ap->save();
		return Redirect::to('app');
	}
	public function delIap($id)
	{
		AppIap::where("id","=",$id)->first()->delete();
		return Redirect::to("app")->with("success","successfully deleted");

	}
	public function status()
	{
		$ar=array();
		$st=Status::orderBy("appear_order")
					->get();
		foreach ($st as $s) {
			/*if($s->order==11)
				continue;*/
			$order=$s->order;
			$ar[$order]["status"]=$s->status;
			$ar[$order]["color"]=$s->color;
			$ar[$order]["color_order"]=$s->color_order;
			
		}
		return $ar;

	}
	function changeStatus()
	{
		$app=Input::get("appID");
		$status=Input::get("status");
		$up=Input::get("up");
		foreach ($app as $appID) {
			$ap=Apps::where("id","=",$appID)->first();
			$ap->app_status=$status;
			//adding app status history
			$this->change_app_status($ap->app_status,$appID);
			if($status==4 && $up=="yes") //if in-review 
			{
				$this->change_app_status(99,$appID);
			}
			$ap->save();
		}
	}
	function statusList()
	{
		$ap=Apps::select("app.*","it.account")
				 ->leftJoin("itunes as it","it.id","=","app.itunes_account_id")
				 ->get();
		$tot=$this->tot(Input::get("projectID"));
		$rp=$this->rpTable();
		$data=array(
			"status"	=> $this->status(),
			"ap"		=> $ap,
			"rp"		=> RootProject::get(),
			"rpOption"	=> $this->rootProjectList(),
			"it"		=> Itune::get(),
			"tot"		=> $tot[0],
			"max"		=> $tot[1],
			"rpH"		=> $rp[0] ,
			"itTot"		=> $rp[1] ,
				
			);
		return View::make("app.statusList",$data);
	}
	function rootProjectList()
	{
		$option="<option value=''>Select Project</option>";$divider=true;
		$rp=RootProject::orderBy("primary","DESC")
					->orderBy("project_name")
					->where("enable",1)->get();
		foreach($rp as $r)
		{
			if(!$r->primary && $divider)
			{
				$divider=false;
				$option.="<option value=''>---------</option>";
			}
			$option.="<option value='$r->id'>$r->project_name</option>";
		}
		return $option;
	}
	function project()
	{
		$projectID=Input::get("projectID");
		$ap=Apps::select("app.*","it.account")
				 ->leftJoin("itunes as it","it.id","=","app.itunes_account_id")
				 ->get();
		$tot=$this->tot($projectID);
		$data=array(
			"status"	=> $this->status(),
			"ap"		=> $ap,
			"rp"		=> RootProject::withTrashed()->get(),
			"it"		=> Itune::get(),
			"tot"		=> $tot[0],
			"max"		=> $tot[1],
			"projectID" => $projectID
			);
		return View::make("app.project",$data);
	}
	function tot($projectID)
	{
		$max=$min=0;
		// $projectID=isset(Input::get("projectID"))?Input::get("projectID"):$projectID;
		$itunes=array();
		$arr=array();
		$t=Apps::select(DB::raw("count(id) as 'total' "),"itunes_account_id as itunes","app_status")
			  ->where("root_project_id","=",$projectID)
			  ->orderBy("app_status")
			  ->groupBy("itunes_account_id","app_status")
			  ->get();
			  
		foreach ($t as $to) 
		{
			
			$arr[$to->app_status][$to->itunes]=$to->total;
			$app_status=$to->app_status;
			if($app_status==11)//11- legal
				continue;
			if($app_status==5 && isset($itunes[$to->itunes]["max"]))
			{
				//only if the max is not unassigned
				if($itunes[$to->itunes]["max"])
					{
						$app_status=0;	
					}
			}
			if(!isset($itunes[$to->itunes]["max"]))
			{
				//light green
				$cnt=Apps::select("itunes_account_id")
							->where(function($query){
								$query->where("app_status","!=","0");
								$query->where("app_status","!=","5");
							})
							->where("root_project_id","=",$projectID)
							->where("itunes_account_id","=",$to->itunes)
							->groupBy("app_status")
							->get();
				$cn=Apps::select("itunes_account_id")
							->where(function($query){
								$query->where("app_status","=","0");
								$query->orWhere("app_status","=","5");
							})
							->where("root_project_id","=",$projectID)
							->where("itunes_account_id","=",$to->itunes)
							->groupBy("app_status")
							->get();
				if(!count($cnt) && count($cn)==2)
					$app_status=15;
				$itunes[$to->itunes]["max"]=$app_status;	
			}
			
			if($itunes[$to->itunes]["max"]< $app_status)
				$itunes[$to->itunes]["max"]=($app_status);

		}
		
		
		return array($arr,$itunes);
	}
	function duplicateName($appName,$dup=0)
	{
		// var_dump("called with app_name".$appName);
		$newApp=($dup)?$appName.$dup:$appName;
		if(Apps::where("app_name","=",$newApp)->first())
		{
			$dup++;
			return $this->duplicateName($appName,$dup);
		}
		else
		{
				// var_dump("returning $newApp");
				// exit;
				return $newApp;
		}
	}
	function ituneMax()
	{
		$arr=array();
		$ap=Apps::all();
		foreach ($ap as $app) {
			$app_status=$app->app_status;
			//managing green color app flag

			if($app_status==5)
			{
				$app_status=0;
			}
			else if($app_status<5)
			{
				$app_status--;
			}
			else if($app_status>5)
			{
				$app_status++;
			}
			if(!isset($arr[$app->itunes_account_id]["max"]))
			{
				$arr[$app->itunes_account_id]["max"]=$app_status;
			}
			if($arr[$app->itunes_account_id]["max"] < $app_status )
			{
				$arr[$app->itunes_account_id]["max"]=$app_status;	
			}
		}
		return $arr;
	}
	
	function reportText()
	{
		$ap=Apps::where("id","=",Input::get("appID"))->first();
		$ap->report_text=Input::get("report_text");
		$ap->save();
	}
	function change_app_status($status,$appID,$txt=false)
	{

		$userID=Auth::user()->id;
		$data=array(
				'app_id'	=> $appID,
				'user_id'	=> $userID,
				);
		if($txt)//if just text is specified in case of status
			$data["text_history"]=$txt;
		else
			$data["status"]=$status;
		//removing duplicate status entry
		$st=AppStatusHistory::whereAppId($appID)
							->orderBy("created_at","DESC")
							->first();
		if($st)
		{
			$prevStatus=$st->status;
			if(($st->status && $st->status==$status) ||
				($st->text_history && $st->text_history==$txt) )
			{
				// echo "duplicate found";
				return "duplicate found";
			}
		}
		AppStatusHistory::create($data);
		$ap=Apps::find($appID);
		$ap->status_updated_at=date("y-m-d h:i:s A");
		$ap->save();
		// v1.12 plist management when changed to reupload metadata
		$this->statusManager->copyOrRemovePlist($status,$ap);
		$txt=str_replace(" ","",$txt);
		$txt=trim($txt);
		$txt=strtolower($txt);

		if($status==3)//v1.8 upload 
		{
			$ap=Apps::find($appID);
			$ap->upload_status=1; //Integrate
			$ap->save();
			$this->change_app_status(0,$appID,"Upload Status : Integrate");
			// v1.9 sending email on upload status change
			$this->appHelper->sendEmailOnStatusChange(3,$appID,1);
		}
		/*when 'uploaded to app store' is clicked by the upload 
		user and the status is set to review, then app is 
		supposed to be uploaded and hence 99*/
		else if($txt=="uploadstatus:upload")
		{
			$ap=Apps::find($appID);
			$ap->app_status=4; //In review
			$ap->save();
			$this->change_app_status(99,$appID);//set to app uploaded
			$this->change_app_status(4,$appID);//set to in review
			// send mail when an app is set tp upload_user
			$this->appHelper->sendEmailOnStatusChange(3,$appID,4);
		}
		if($status)
		{
			$this->appHelper
			->sendEmailOnStatusChange($status,$appID,$ap->upload_status);
		}
		
		//upload app if upload status : upload
	}
	function view_app_status($id)
	{
		$ap=AppStatusHistory::where('id','=',$id)->first();
		return $ap->status;
	}
	function appValid()
	{
		$rp=Input::get("rp");
		$it=Input::get("it");
		$ap=Apps::select("app_name")
				  ->where("root_project_id","=",$rp)
				  ->where("itunes_account_id","=",$it)
				  ->where("app_status","=",0)
				  ->first();
		return  $ap;
	}
	protected function rpTable()
	{
		$app=Apps::select(DB::raw("count(app.itunes_account_id) as 'total'"),
								"rp.project_name as 'name' ","it.account",
								"it.id as it_id","rp.id as rp_id")
				->join("root_project as rp","rp.id","=","app.root_project_id")
				->join("itunes as it ","it.id","=","app.itunes_account_id")
				->groupBy("app.root_project_id","app.itunes_account_id")
				->get();
		$a=$it=$itName=$tot=array();
		foreach($app as $r)
		{
			$a[$r->rp_id][$r->it_id]=$r->total;
			if(isset($tot[$r->it_id]))
				$tot[$r->it_id]+=(int)$r->total;
			else
				$tot[$r->it_id]=$r->total;
		}
		return array($a,$tot);

				
	}
	function updateKeys()
	{
		$field=Input::get("field");
		$appID=Input::get("appID");
		$val=Input::get("val");
		$ap=Apps::where("id",$appID)
				->first();
		if($field=="boxFb")
			$ap->fbID=$val;
		else if($field=="boxKey")
			$ap->nxt_key=$val;
		else if($field=="boxSso")
			$ap->nxt_sso=$val;
		else if($field=="boxProf")
			$ap->prov_profile="https://".$val;
		else if($field=="gDesigner")
			$ap->graphic_designer=$val;
		else if($field=="graphics")
			$ap->graphics=$val;
		else if($field=="aspRsch")
			$ap->aso_resch=$val;
		else if($field=="aso")
			$ap->aso=$val;
		else if($field=="upload_note")
			$ap->upload_note=$val;
		else if($field=="grapAssign")
			{
				$this->appHelper->setMailFlag(false);
				$ap->graphic_assign=$val;
				if($val)
				{
					$this->change_app_status(16,$ap->id,"Graphics Complete");
					//v1.12 sending mail to ASO user if graphics complete
					$this->appHelper->setMailFlag(true);
					$this->appHelper
						->sendEmailOnStatusChange("graphics complete",$ap->id,false);
				}
				$this->appHelper->setMailFlag(true);
			}
		else if($field=="apInforComplete")
			{
				$ap->app_info_complete=$val;
				$this->appHelper->setMailFlag(false);
				if($val)
				{
					$this->change_app_status(16,$ap->id,"ASO Complete");
				}
				$this->appHelper->setMailFlag(true);
			}	
		else if($field=="gAssign")
			{
				$this->appHelper->setMailFlag(false);
				$ap->gd_assign=$val;
				if($val)
				{
					$this->change_app_status(16,$ap->id,"Graphics Assigned");
				}
				$this->appHelper->setMailFlag(true);
			}
		else if($field=="asoAssign")
				{
					$this->appHelper->setMailFlag(false);
					$ap->resch_assigned=$val;
					if($val)
					{
						$this->change_app_status(16,$ap->id,"ASO Assigned");
					}
					$this->appHelper->setMailFlag(true);
				}
		else if($field=="appNmPro")
			{
				AppReportCells::where("app_id",$appID)
						->where("value",'$APPNAME_PRO')
						->orWhere("value",$ap->app_pro_info_name)
						->update(array("value"=>$val));
				$ap->app_pro_info_name=$val;

			}
		else if($field=="appNmFree")
			{
				AppReportCells::where("app_id",$appID)
						->where("value",'$APPNAME_FREE')
						->orWhere("value",$ap->app_info_name)
						->update(array("value"=>$val));
				$ap->app_info_name=$val;

			}
		
		$ap->save();
		//switching to pending assignment from dev
		if($field=="gAssign" || $field=="grapAssign" || 
			$field=="asoAssign" ||	$field=="apInforComplete"  )
		{
			$this->_toPendingAssign($ap);
		}
		
		return $this->_getHistory($appID);
	}
	function _toPendingAssign($ap)
	{
		if($ap->graphic_assign && $ap->app_info_complete && $ap->app_status=="17" ) 
			{ //setting to pending assignemnt if complete status
				//[v1.11.2] setting mail send to false 
				$this->appHelper->setMailFlag(false);
				$this->change_app_status(16,$ap->id);
				$this->appHelper->setMailFlag(true);
				$ap->app_status=16;
				$ap->save();
			}	
	}
	function appInfoUpdate()
	{

	}
	function assignIT()
	{
		$appID=Input::get("appID");
		$itID=Input::get("itID");
		$ap=Apps::find($appID);
		$ap->itunes_account_id=$itID;
		$ap->app_status="0.1";
		// assigning to the user maria
		$ap->assign_to=22;
		//asigging ends

		//changing root project report text
		$ap->report_text=RootProject::where("id",$ap->root_project_id)
						->pluck("report_text");
		$ap->save();
		//V1.8 delete iap if inherit if false for the given it ac
		$this->deletIapIfNotInherit($ap->itunes_account_id,$ap->id);

		//v1.8.3 update upload report from root proejct
		$rc=ProjectCell::where("project_id","=",$ap->root_project_id)
						 ->get();
		foreach ($rc as $r2) 
			{
				
				$appReportCell=AppReportCells::where("app_id",$ap->id)
											->where("row_index",$r2->cell_row)
											->where("col_index",$r2->cell_col)
											->first();
				if($appReportCell)
				{
					$appReportCell->value=$r2->value;
					$appReportCell->style=($r2->style)?:'color:#000;';
					$appReportCell->save();
				}
			}
		//upload reassigning done
		
		$apNm=$ap->app_name;
		$this->_specialVars($appID,$itID);						
		$this->change_app_status("0.1",$appID);
		
		Session::flash("success","sucessfully assigned itunes account");
		//mailing for redundant statuses
		$this->checkMaxAppStatus($ap);
		return Apps::find($appID);
	}
	protected function _specialVars($appID,$itID=0)
	{
		$ap=Apps::find($appID);
		$it=($itID)?Itune::find($itID):new Itune;
		$apNm=$ap->app_name;
		$special=array(
			'$ITUNES_ACCOUNT'=>array("val"=>$it->account, "it"=>1),
			'$APPNAME_FREE'=>array("val"=>$ap->app_info_name, "it"=>0),
			'$APPNAME_PRO'=>array("val"=>$ap->app_pro_info_name, "it"=>0),
			'$BUNDLEID_FREE'=>array("val"=>$this->appToBundle($apNm,$it->bundlePrefix), "it"=>1),
			'$BUNDLEID_PRO'=>array("val"=>$this->appToBundle($apNm.'Pro',$it->bundlePrefix), "it"=>1),
			'$URL_FREE'=>array("val"=>$ap->itunes_url, "it"=>0),
			'$URL_PRO'=>array("val"=>$ap->Pitunes_url, "it"=>0),
			'$APPLEID_FREE'=>array("val"=>$ap->apple_id, "it"=>0),
			'$APPLEID_PRO'=>array("val"=>$ap->Papple_id, "it"=>0),
			'$LEADERBOARDID'=>array("val"=>$this->appToBundle($apNm.'.Leaderboard',"grp.".$it->bundlePrefix),"it"=>1)
		);
		foreach($special as $key=>$val)
		{
			if($val["val"])
			{
				if($val["it"])
				{
					if($itID)
					{
						AppReportCells::where("app_id",$appID)
								->where("value",$key)
								->update(array("value"=>$val["val"]));
					}
 
				}
				else
				{
					AppReportCells::where("app_id",$appID)
								->where("value",$key)
								->update(array("value"=>$val["val"]));
				}
				
			}
		}
		
	}
	function gFixed()
	{
		$appID=Input::get("appID");
		$ap=Apps::find($appID);
		if(!$ap->upload_user)
		{
			return json_encode(array("error"=>"No Previous Upload User selected"));
		}
		else
		{
			$ap->app_status=3; //upload queue
			$ap->assign_to=$ap->upload_user; //re assigning ref line:848
			$this->change_app_status(3,$appID);
			$ap->save();
		}
	}
	protected function _getHistory($appID)
	{
		$history=AppStatusHistory::select("app_status_history.*","st.status","usr.username",DB::raw("app_status_history.status as status_id"))
									->leftjoin("statuses as st ","st.order","=","app_status_history.status")
									->leftjoin("user as usr","usr.id","=","app_status_history.user_id")
									->where("app_status_history.app_id","=",$appID)
									->whereNull("st.deleted_at")
									->whereNull("usr.deleted_at")
									->get();
		return View::make("app.statusHistory",array('history'=>$history));
	}
	public function pdf($id)
	{
		$ap=Apps::find($id);
		$pd=App::make("dompdf");
		$css=File::get(public_path()."/css/pdf.css");
		$apx=$this->appReportCellGet($id);
		$apRep=$apx["value"];
		$sty=$apx["style"];
		$pdf=1;
		$utype=Auth::user()->account_type;
		$html=(string)View::make("app/pdf",compact('ap','apRep','sty','id','utype','pdf'));
		// dd(($html));
		$pd->loadHTML($html);
		return $pd->stream();
	}
	function updateAppStats()
	{
		$appID=Input::get("appID");
		$name=Input::get("name");
		$val=Input::get("val");
		$status=Input::get("status");
		$assignTo=Input::get("assignTo");
		$ap=Apps::find($appID);
		$ap->$name=$val;
		if($assignTo)
			$ap->assign_to=$assignTo;
		$ap->save();
		// v1.9 sending mail on status change
		if($name=="upload_status")
		{
			$this->appHelper
			->sendEmailOnStatusChange($ap->app_status,$appID,$val);
		}

		$status=trim($status);
		if($status && is_numeric($status))
		{
			
			$this->change_app_status($status,$appID);
			return 'numeric status';
		}
		elseif($status && is_string($status))
		{
			echo $this->change_app_status(0,$appID,$status);
			return "0,$appID,$status";
		}

	}
	/**
	 *
	 * check if created  or assigned app has redundant
	 * status in that itunes account
	 * @param    NUll
	 * @return   Null
	 *
	 */
	public function checkMaxAppStatus($app)
	{
		/*v1.11.1 send diff mail if account wise 
		colring is specified*/
		if($this->appMailer->maxStatusAccountWise())
		{
			return;
		}
		$appStatus=$app->app_status;
		$itunesAccount=$app->itunes_account_id;
		$rp=$app->root_project_id;
		$statusToSkip=["unassign"=>0,
						"release"=>5,
						"legal"=>11];
		$data=[];
		// check if itunes account has full 
		$isFull=false;
		if($itunesAccount)
		{
			$checkFull=Itune::find($itunesAccount);
			if($checkFull)
				$isFull=$checkFull->full;
		}
		$isSkipStatus=array_search($appStatus, $statusToSkip);
		if(($isSkipStatus===false && $itunesAccount) || $isFull)
		{
			// any app with same accoutn  except itself
			$ap=Apps::whereItunesAccountId($itunesAccount)
						->whereRootProjectId($rp)
						->where("id","!=",$app->id)
						->where("app_status","!=",5)
						->where(function($query) use ($statusToSkip)
						{
							foreach($statusToSkip as $key=>$value)
							{
								$query=$query->where("app_status","!=",$value);
							}
						})
						->get();
			if($isFull)
				{
					$ap=[$app];
				}
			if($ap)
			{
				foreach($ap as $a)
				{
					if($a->app_status=="0.1")
						$a->status="add app";
					else
					{
						$a->status=Status::whereOrder((string)$a->app_status)
														->first();
						$a->status=$a->status->status;
					}
					$data[]=$a;
				}
			}
		}
		if($data)
		{
			return Mail::queue('emails.appRed', compact('data'), function($message)
			{
			    $message->to('dsstudios12@gmail.com', 'dsstudios')
			    		->subject('Sentio Portal: App Overflow problem');
			});
			// return View::make("emails.appRed",compact("data"));
		    			// ->cc('dsstudios12@gmail.com', 'dsstudios')
		}
		// return $data;
	}
}