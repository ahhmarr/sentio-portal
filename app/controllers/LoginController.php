<?php 
class LoginController extends BaseController{

	public function index()
	{
		$auth=Auth::attempt(array(
             'username'=>Input::get("username"),
             'password'=>Input::get("password")
			));
		if($auth)
			{
				return Redirect::to('/app');
			}
		else
			{
				return Redirect::to('/')
						->with('error','Invalid username/password')
						->withInput();
			}
	}
	public function logout()
	{
		Auth::logout();
		return Redirect::to('/')
				->with('success','Successfully logged out')
				->withInput();
	}
	public function backup()
	{
		$name="senport_db_".date("m_d_Y___h_i_s_A").".sql.gz";
		// $path=$_SERVER["DOCUMENT_ROOT"]."/senPort/db/$name";
		$path="/home/dsstudios/sentioPortal_db/$name";
		$command="mysqldump -u new_root -pmycroft -h localhost averex_senport | gzip > $path";
		system($command);
		// echo $command;
	}
}