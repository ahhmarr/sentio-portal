<?php

class SdkController extends BaseController {

	public  function __construct()
	{
		$this->beforefilter('auth');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
        $data=array(
        	'rp' 		=> RootProject::get(),
        	'selectRP'	=> ''
        	);
        return View::make('sdk.index',$data);
	}
	function selectXL($selectRP,$file=null)
	{
		$tab=$head=$sdk=$id=array();
		if(!$file)
		{
			$file=MainXl::where("root_project_id",$selectRP)
			->orderBy("created_at","desc")
			->first();
		}
		else
		{
			$file->file_import_id=$file->id;
		}
		if(!isset($file->file_import_id))
			return array('tab'=>$tab,'id'=>$id,'head'=>$head,'sdk'=>$sdk);
		$table=MainXl::select("xl_main.*","xl_sdk.sdk_name","xl_heading.header_name","app.free_sdk","app2.pro_sdk")
					->where("xl_main.file_import_id",$file->file_import_id)
					->where("xl_main.root_project_id",$selectRP)
					->leftJoin("xl_sdk","xl_sdk.id","=","xl_main.sdk_id")
					->leftJoin("xl_heading","xl_heading.id","=","xl_main.heading_id")
					->leftJoin("app","app.free_sdk","=","xl_main.sdk_id")
					->leftJoin("app as app2","app2.pro_sdk","=","xl_main.sdk_id")
					->get();
		foreach ($table as $row)
		{
			$tab[$row->header_name][$row->sdk_name]=$row->value;
			$id[$row->header_name][$row->sdk_name]=$row->id;
			if(array_search($row->header_name,$head)===false)
				$head[]=$row->header_name;
			if(array_search($row->sdk_name,$sdk)===false)
				$sdk[]=$row->sdk_name;

		}
		return array('tab'=>$tab,'id'=>$id,'head'=>$head,'sdk'=>$sdk);
	}
	function import()
	{
		$fileID=$this->_moveImport();
		$selectRP=Input::get("rootProject");
		if(!$fileID)
		{
			return Redirect::to("sdk")->with("error","Malformed XL");
		}
		else if(isset($fileID->fail))
		{
			return Redirect::to("sdk");
		}
		else
		{
			$table=$this->selectXL($selectRP,$fileID);
			$data=array(
	        	'rp' 			=>	RootProject::get(),
	        	'sdkResponse'	=>	$table["tab"],
	        	'id'			=>	$table["id"],
	        	'head'			=>	$table["head"],
	        	'sdk'			=>	$table["sdk"],
	        	'selectRP'		=>	Input::get("rootProject")
	        	);
		}
		
		return View::make("sdk.index",$data);
	}
	protected function removeSpace($table)
	{
		//finding space in header
		$match=array_filter($table[0][0],function($val){
			return (trim($val)=="");
		});
		$spaces=(array_keys($match));
		//finding space in the first row
		$_match=array_filter($table[1][0][0],function($_val){
			return ($_val=="");
		});
		$_spaces=(array_keys($_match));
		//common space location
		$common=array_intersect($spaces, $_spaces);
		//sanitizing the array in a serial key order
		return (array_values($common));
	}
	protected function _moveImport()
	{
		$file=(Input::file("import"));
		$importM=FileXl::select("id")
					 ->orderBy("id","desc")
					 ->first(); 
		$importMaxID=(isset($importM->id))?$importM->id+1:1;
		$fileName=$file->move('docs/',"import$importMaxID.xls");
		//file name saving
		$importFileID=FileXL::create(
			array('file_name'=>$fileName)
			);
		$table=$this->_readData($fileName); //need to pass the file name
		if(!$table)
			return false;
		$data["head"]=$table[0][0];
		$data["body"]=$table[1];
		//new sdk check
        $sdks=$this->_getSDK($data["body"],$importFileID);
        if(!$sdks["sdk"])
        	{
              $obj=new stdClass();
              $obj->fail=1;
              return $obj;
        	}
		//start transaction
		$space=$this->removeSpace($table);
        //new heading check w
        $headings=$this->_getHeading($data["head"]);
        //check for duplicacy
        $isDup=$this->_checkDup($data["body"]);
        if(!$isDup)
        {
    		$obj=new stdClass();
              $obj->fail=1;
              return $obj;
        }
        //insert SDk & heading
        DB::table("xl_sdk")
			->insert($sdks["sdkInsert"]);
		DB::table("xl_heading")
					->insert($headings["headingQuery"]);
        //save tabular details
        $th=$this->_saveTabular($headings["heading"],$sdks["sdk"],$data["body"],Input::get("rootProject"),$importMaxID,$space);
        if(!$th)
        	{
              $obj=new stdClass();
              $obj->fail=1;
              return $obj;
        	}
		return $importFileID;
	}
	/*this will check wether duplicate values are present of not*/
	protected function _checkDup($body)
	{
		
		$flag=true;
		$val=0;
		for($x=0;$x<count($body);$x++)
			{
				
				foreach($body[$x] as $b)
				{
					
					foreach($b as $key=>$value)
					{
						if($key==0)//escaping the sdk name column
							continue;
						if(MainXl::where("value","=",$value)->first() && $value)
						{
							Session::flash("error","duplicate value $value");
							$flag=false;
							break;
						}
						
					}
					
				}
			}
			
		return $flag;
	}
	/*this will take heading and sdk along with the table body 
	and will save into the database*/
	protected function _saveTabular($head,$sdk,$body,$rp,$file_import,$space)
	{
		Debugbar::addMessage($space);
		$val=array();
		for($x=0;$x<count($body);$x++)
			{
				
				foreach($body[$x] as $b)
				{
					$sdkName=$b[0];
					$c=-1;
					foreach($b as $key=>$value)
					{
						if($key==0 || in_array($key,$space))
							{
								continue;}
						$c++;

						//getting heading and sdk id
						if(!isset($head[$c]))
							{
								continue;
							}
						$headID=HeadingXL::select("id")
											->where("header_name","=",$head[$c])
											->first();

						$sdkID=SDKXl::select("id")
										->where("sdk_name","=",$sdkName)
										->first();
						if(!$headID || !$sdkID)
							continue;
						if(trim($value)=="x")
						{
							$value="";
						}
						if(MainXl::where("value","=",$value)->first() && $value)
						{
							Session::flash("error","duplicate value $value");
							return false;
						}
						$val[]=array(
							'file_import_id' 	=> $file_import,
							'sdk_id'		 	=> $sdkID->id,
							'heading_id'	 	=> $headID->id,
							'value'			 	=> $value,
							'root_project_id'	=> $rp,
							);
					}
					
				}
			}
			if($val)
			{
				$r=DB::table("xl_main")
				->insert($val);
			}
		return 1;
	}
	/*this will save all the new heading 
	and will return all the heading in the xls*/

	protected function _getHeading($head)
	{
		$newHead=$headi=array();
		foreach ($head as $key => $v)
		{
			if(!trim($v))
				continue;
			$headi[]=$v;
			if(!HeadingXl::where("header_name","=",$v)->first() && $v && trim($v)!="SDK App Name")
			{
				$newHead[]=array(
					"header_name"=>$v
					);
			}
		}
		/*if($newHead){DB::table("xl_heading")
					->insert($newHead);}*/
		return array("heading"=>$headi,"headingQuery"=>$newHead);;
	}
	/*this will save all the new sdk
	and will return all the heading in the xls*/

	protected function _getSDK($body,$importFileID)
	{
		$newSDK=$sdk=array();
		$sdkName="";
		for($x=0;$x<count($body);$x++)
			{
				foreach($body[$x] as $b)
				{
					$sdkName=$b[0];
					if(!trim($b[0]))
						continue;
					$sdk[]=$sdkName;
					if(SDKXl::where("sdk_name","=",$sdkName)->first())
					{
						Session::flash("error","Duplicate SDK $sdkName");
						return array("sdk"=>false);
					}
					if($sdkName)
					{
						$newSDK[]=array(
							"sdk_name"			=>$sdkName,
							"import_file_id"	=>$importFileID
							);
					}
				}
			}
		/*if($newSDK)
		{
			DB::table("xl_sdk")
			->insert($newSDK);
		}*/
		return array("sdk"=>$sdk,"sdkInsert"=>$newSDK);;
	}
	public function _readData($fileName)
	{
		
		$inputFileName = $fileName;
		//  Read your Excel workbook
		try {
		    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		    $objPHPExcel = $objReader->load($inputFileName);
		} 
		catch(Exception $e) {
		    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();
		$head=$body=array();
		$head=$sheet->rangeToArray("A1:".$highestColumn."1",NULL,TRUE,FALSE);
		for ($row = 2; $row <= $highestRow; $row++){ 
		    //  Read a row of data into an array
		    $body[] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
		                                    NULL,
		                                    TRUE,
		                                    FALSE);
		   
		}
		//checking first cell validation
		return ($head[0][0]=="SDK App Name")?array($head,$body):false;
	}
	public function rpXL()
	{
		$selectRP=Input::get("rootProject");
		$table=$this->selectXL($selectRP);
		if(!$table["head"])
			return 'no previous file found';
		$data=array(
        	'sdkResponse'	=>	$table["tab"],
        	'id'			=>	$table["id"],
        	'head'			=>	$table["head"],
        	'sdk'			=>	$table["sdk"],
        	);
		Debugbar::addMessage($data["sdkResponse"]);
		return View::make("sdk.table",$data);
	}
	public function updateXL()
	{
		$id=Input::get("id");
		$r=MainXl::where("id",$id)
					->first();
		$r->value=Input::get("val");
		$r->save();
	}
	public function create()
	{
        return View::make('sdk.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('sdks.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('sdks.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
