<?php
use Senportal\ConfigManager;
class ConfigController extends Controller {

	/**
	 * Display a listing of the resource.
	 * GET /config
	 *
	 * @return Response
	 */
	protected $config;

	public function  __construct(ConfigManager $config)
	{
		$this->config=$config;
	}
	public function index()
	{
		$config=$this->config->getConfigValues();
		return View::make("config.index",compact("config"));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /config/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /config
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /config/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /config/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /config/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$this->config->changeConfigurations(Input::get("col"),
			Input::get("value"),$id);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /config/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}