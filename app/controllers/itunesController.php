<?php 
use Senportal\AppAction;
class ItunesController extends BaseController{

	protected $appAction;
	public function __construct(AppAction $appAction)
	{
		$this->beforefilter('auth');
		$this->appAction=$appAction;
	}
	public function index()
	{
		return View::make('itunes.index')
				->with('list',Itune::get());
	}
	public function save()
	{
		$account=Input::get('account');
		$bundlePrefix=Input::get("_xrtx_bundlePrefix_xrtx");
		if(Itune::wherebundleprefix($bundlePrefix)->first())
		{
			return Redirect::to('itunes')
					->with('error','duplicate bundle prefix');
		}
		$it=Itune::create(array(
			'account'=>$account,
			'bundlePrefix'=>$bundlePrefix
			));
		// v1.12 send email on itunes created
		$this->appAction->sendEmailOnStatusChange("Itunes Account",0,0);
		return Redirect::to('itunes')
			->with('success','successfully added');
	}
	public function view()
	{

	}
	public function delete($id)
	{
		Itune::find($id)
				->delete();
		return Redirect::to('itunes')
						->with("success","successfully deleted");
	}
	public function edit()
	{

	}
	public function toggle()
	{
		$type=Input::get("type");
		if($type=="full")
			$col="full";
		elseif($type=="iap")
			$col="iap_inherit";
		elseif($type=="app_notice")
			$col="app_notice";
		if(!$col)
			return App::abort(500,"Invalid credential");
		$it=Itune::where("id","=",Input::get("id"))
					->first();
		$it->$col=Input::get("flag");
		$it->save();
	}
}
