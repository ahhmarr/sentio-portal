<?php

class StatusesController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public  function __construct()
	{
		// $this->beforefilter('auth');
		set_time_limit(360); //60 seconds = 1 minute
	}
	public function index()
	{
        $data=array(
        	'st'	=> Status::all(),
        	'rp'	=> $rp
        					
        	);
        return View::make('statuses.index',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('statuses.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		Status::create(
			array(
				"status"			=> Input::get("status"),
				"color"				=> Input::get("color"),
				"order"				=> Input::get("order"),
				));
		return Redirect::to("status");
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('statuses.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('statuses.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}
	public function delete($id)
	{
		Status::find($id)->delete();
		return Redirect::to("status");
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	function filter()
	{
		$rp=Input::get("rp");
		$it=Input::get("it");
		$up=Input::get("up");
		$uq=Input::get("uq");
	}
	function _isLive()
	{
		$aps=Apps::all();
		foreach($aps as $ap)
		{
			$url=$ap->itunes_url;
			$resp=$this->getResponseCode($url);

			if($resp==200)
			{
				$ap->is_live=DB::raw("NOW()");
			}
			else
			{
				$ap->is_live=DB::raw("NULL");
			}
			$ap->save();
			if($resp)
				var_dump("itunes url: $ap->itunes_url status : $resp");
		}
	}
	function getResponseCode($url)
	{
		// $url=urlencode($url);	
		$options = array (CURLOPT_RETURNTRANSFER => true, // return web page
	    CURLOPT_HEADER => true, // don't return headers
	    CURLOPT_FOLLOWLOCATION => true, // follow redirects
	    CURLOPT_ENCODING => "", // handle compressed
	    CURLOPT_USERAGENT => "test", // who am i
	    CURLOPT_AUTOREFERER => true, // set referer on redirect
	    CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
	    CURLOPT_TIMEOUT => 120, // timeout on response
	    CURLOPT_MAXREDIRS => 10 ); // stop after 10 redirects

	    $ch = curl_init ( $url );
	    curl_setopt_array ( $ch, $options );
	    $content = curl_exec ( $ch );
	    $err = curl_errno ( $ch );
	    $errmsg = curl_error ( $ch );
	    $header = curl_getinfo ( $ch );
	    $httpCode = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );

	    curl_close ( $ch );

	    $header ['errno'] = $err;
	    $header ['errmsg'] = $errmsg;
	    $header ['content'] = $content;
	    return $header ['http_code'];
	}
	public function isLive()
	{
		$ax=$appErr=[];
		/*if not itunes url or no apple id then is live status set to not live
			if itunes url exists then check for status*/
		/*$apx=Apps::where(DB::raw("trim(apple_id)"),"=","")
			  ->orWhere(DB::raw("trim(itunes_url)"),"=","")
			  ->orWhere(DB::raw("trim(apple_id)"),"=","0")
			  ->orWhereNull("apple_id")
			  ->orWhereNull("itunes_url")
				->update([
					"is_live"=>DB::raw("NULL")
					]);*/

		// getting all the apps with both apple aid and itunes url
		$apps=Apps::all();
		foreach($apps as $ap)
		{
		     if(!$ap->itunes_url || !$ap->apple_id)
	    	{
		    	$appErr[]=$ap;
		    	$ap->is_live=DB::raw("NULL"); //null-skip 1-not live  2-live
		    	$ap->save();
		    	continue;
		    }
		    $file = $ap->itunes_url;
			$file_headers = @file_get_contents($file);
			preg_match("/Description/",$file_headers,$mt);
			if(isset($mt[0]))
			{
				$ap->is_live=2; // live
			}
			else
			{
				$ap->is_live=1; //not live
			}
			$ap->save();
			// checking release cycle starts 
			if($ap->app_status==5)
			{
				$aph=AppStatusHistory::whereAppId($ap->id)
						->whereStatus(5)
						->orderBy("created_at","DESC")
						->first();
				if(!$aph)
				{
					AppStatusHistory::create(array(
						"app_id" => $ap->id,
						"user_id"=> Auth::user()->id,
						"status"=>5
						));
					$upd=$ap->created_at;
				}
				else
				{
					$upd=$aph->created_at;
				}
				$d1=new DateTime();
				$d2=new DateTime($upd);
				$interval=$d1->diff($d2);
				$hrs=$interval->format("%h");
				//app released for 2 days and still not on itunes
				if($hrs>2 && ($ap->is_live==1 || !$ap->is_live))
				{
					$ax[]=$ap;
				}
					
			}
			// checking release cycle ends
		}
		$this->sendMail(compact("ax","appErr"));
		return 1;
	}
	
	
	public function sendMail($a)
	{
		// dd(phpinfo());
		// dsstudios12@gmail.com
		extract($a);
		if($ax)
		{
			Mail::queue('emails.appError', array("ax"=>$ax), function($message)
			{
			    $message->to('ahmar.siddiqui@gmail.com', 'dsstudios')
			    		->cc('dsstudios12@gmail.com', 'dsstudios')
			    		->subject('APP Live Error');
			});
		}
		if($appErr)
		{
			Mail::queue('emails.appInvalid', array("appErr"=>$appErr), function($message)
			{
			    $message->to('ahmar.siddiqui@gmail.com', 'dsstudios')
			    		->cc('dsstudios12@gmail.com', 'dsstudios')
			    		->subject('APP Metadata Missing');
			});
		}
		return 1;
	}

}
