<?php 
use Senportal\RootProjectHelper;
class RootsController extends BaseController{

	protected $rootProject;
	public function __construct(RootProjectHelper $rootProject)
	{
		$this->beforefilter('auth');
		$this->rootProject=$rootProject;
	}
	public function index($id=null)
	{
		$cell=array();
		$style=array();
		$c=ProjectCell::where('project_id','=',$id)->get();
		foreach ($c as $l) {
			$cell[$l->cell_row.''.$l->cell_col]=$l->value;
			$style[$l->cell_row.''.$l->cell_col]=$l->style;
		}
		$txt='<input type="text" class="border-none cell-box">';
		$pro=RootProject::find($id);
		$data=array(
			'list'=>RootProject::where('enable','=','1')
								->orderBy("primary","DESC")
								->orderBy("project_name")->get(),
			'active'=>$pro,
			'iap'=>Iap::where("project_id",'=',$id)->get(),
			'bundlePrefix'=>$this->bundlePrefix(),
			'proj'=>$pro,
			'textBox'=>$txt,
			'cell'=>$cell,
			'style'=>$style,
			'uploadUsers'=>User::whereAccountType(2)->get()

			);
		return View::make('root.index',$data);
	}
	public function bundlePrefix()
	{
		$ans=Itune::get();
		$opt='';
		foreach ($ans as $d) {
			$opt.='<option value="'.$d->account.'">'.$d->account.'</option>';
		}
		return $opt;
	}
	public function view($id)
	{
		
		

	}
	public function edit()
	{

	}
	public function disable($id)
	{
		$r=RootProject::find($id);
		$r->enable=0;
		$r->save();
		return Redirect::to('root');


	}
	public function editName()
	{
		$r=RootProject::find(Input::get("id"));
		$r->project_name=Input::get("name");
		$r->save();
		return Redirect::to('root');
	}
	public function save()
	{
		$exists=RootProject::where("project_name","=",Input::get('rootProjectName'))->first();
		if($exists)
			return Redirect::to("root")
							->with("error","Duplicate project name");
		$r=RootProject::create(
			array('project_name'=>Input::get('rootProjectName'))
			);
		//v1.12 saving default cell values
		$this->rootProject->saveDefaultCellValues($r->id);
		return Redirect::to('root/'.$r->id)
						->with("success","success fully created project ".Input::get('rootProjectName'));
	}
	public function delete()
	{

	}
	public function rowCols()
	{
		$projID=Input::get("projID");
		$rows=Input::get("rows");
		$cols=Input::get("cols");
		$rp=RootProject::find($projID);
		$rp->rows=$rows;
		$rp->cols=$cols;
		$rp->save();
		$this->updateCells();
		return 'called';
	}
	/*
	this will be responsible of deleteing/adjusting 
	various cell with their values
	*/
	// col add col>3 col+1 

	// col remove col=3 del col

	// row add row>3 row+1

	//row rem row=3 del row
	protected function updateCells()
	{
		$projID=Input::get("projID");
		$row=Input::get("row_index");
		$col=Input::get("col_index");
		switch(Input::get("action"))
		{
			case 'row-add'		:  ProjectCell::where('project_id','=',$projID)
											->where('cell_row','>=',$row-1)
											->update(array('cell_row'=>DB::raw('cell_row+1')));
									break;
			case 'row-remove'	:  ProjectCell::where('project_id','=',$projID)
											->where('cell_row','=',$row)
											->delete();
									ProjectCell::where('project_id','=',$projID)
											->where('cell_row','>',$row)
											->update(array('cell_row'=>DB::raw('cell_row-1')));
									break;
			case 'col-add'		:   ProjectCell::where('project_id','=',$projID)
											->where('cell_col','>=',$col+1)
											->update(array('cell_col'=>DB::raw('cell_col+1')));
									break;
			case 'col-remove'	:   ProjectCell::where('project_id','=',$projID)
											->where('cell_col','=',$col)
											->delete();
									ProjectCell::where('project_id','=',$projID)
											->where('cell_col','>',$col)
											->update(array('cell_col'=>DB::raw('cell_col-1')));
									break;
		}
	}
	public function saveCell()
	{
		$projID=Input::get("projID");
		$row=Input::get("row");
		$col=Input::get("col");
		$value=Input::get("value");
		$style=Input::get("style");
		$rp=ProjectCell::where('project_id','=',$projID)
						->where('cell_row',"=",$row)
						->where('cell_col',"=",$col)
						->first();

		if($rp)
		{
			$rp->value=$value;
			$rp->style=$style;
			$rp->save();
			return 1;
		}
		ProjectCell::create(
			array(
					'project_id'=>$projID,
					'cell_row' =>$row,
					'cell_col'=>$col,
					'value'=>$value,
					'style'=>$style
				)
			);
		
		return 1;
	}
	public function updateCols($projID,$rows)
	{
		
	}
	public function changeIAP()
	{
		$iap=Input::get("iap");
		$id=Input::get("id");
		$v=($iap)?1:0;
		$r=RootProject::where("id","=",$id)->first();
		$r->iap=$v;
		$r->save();
	
	}
	public function changeFreePro()
	{
		$iap=Input::get("iap");
		$id=Input::get("id");
		$v=($iap)?1:0;
		$r=RootProject::where("id","=",$id)->first();
		$r->free_n_pro=$v;
		$r->save();
	
	}
	public function changeGame()
	{
		$iap=Input::get("flag");
		$id=Input::get("projectID");
		$v=($iap)?1:0;
		$r=RootProject::find($id);
		$r->game_center=$v;
		$r->save();
	
	}
	function updateRootText()
	{
		$rp=RootProject::where("id","=",Input::get("projID"))->first();
		$rp->report_text=Input::get("value");
		$rp->save();
	}
	public function toggle()
	{
		$type=Input::get("type");
		$flag=Input::get("flag");
		$projID=Input::get("projID");
		$pr=RootProject::find($projID);
		if(!$pr)
			App::abort(500,"Invalid Flag value");
		$pr->$type=$flag;
		$pr->save();
	}
	public function updateFields($id)
	{
		$rp=RootProject::findOrFail($id);
		$field=Input::get("field");
		$val=Input::get("val");
		if(!$field)
			return;
		$rp->$field=$val;
		$rp->save();
		return $rp;
	}
	function uploadPlist($id)
	{
		$file=Input::file("plist");
		if(is_null($file))
			return;
		// validate plist extension
		// save it to a location
		if($path=$file->move("docs",time().$file->getClientOriginalName())){
			$project=RootProject::find($id);
			$project->plist_name=$file->getClientOriginalName();
			$project->plist_path=$path;
			$project->save();
			return Response::json(["path"=>asset("$path"),
					"name"=>$project->plist_name]);
		}
	}
	function removePlist($id){
		$project=RootProject::find($id);
		//delete the file from docs folder
		if(File::exists($project->plist_path))
			File::delete($project->plist_path);
		$project->plist_name=DB::raw("NULL");
		$project->plist_path=DB::raw("NULL");
		$project->save();
	}
} 