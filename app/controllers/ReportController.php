<?php 
class ReportController extends BaseController{
	
	public  function __construct()
	{
		$this->beforefilter('auth');
	}

	public function index()
	{
		$rep=ReportLabels::all();
		$data=array(
			'rep'	=> $rep
			);
		return View::make("rep.index",$data);
	}
	public function save()
	{	
		//dd('hello');
		$table_name=Input::get('table_name');
		$column=Input::get('column');
		$variable=Input::get('variable');
		ReportLabels::create(array(
			'table_name'	=>$table_name,
			'column'		=>$column,
			'variable'		=>$variable,
			));
		
		return Redirect::to("rep")->with('message','successfully added');
	}
	public function view()
	{

	}
	public function delete($id)
	{
		// dd($id);
		ReportLabels::find($id)->delete();
		return Redirect::to("rep")->with('message','successfully deleted');
	}
	public function edit()
	{

	}

}
