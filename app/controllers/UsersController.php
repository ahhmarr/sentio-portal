<?php

class UsersController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	public  function __construct()
	{
		$this->beforefilter('auth');
		
	}
	public function index()
	{
		$usr=User::select("user.*","user_type.*","user.id as id")
					->leftJoin("user_type","user_type.type","=","user.account_type")
					->get();
		$data=array(
			'usr' 			=> $usr,
			'userType'		=> UserType::all()
			);
		return View::make("user.index",$data);
	}
	public function create()
	{
		$userExist=User::where("username","=",Input::get("username"))->first();
		if($userExist)
			return Redirect::to("user")->with("error","Duplicate::username");
		User::create(
			array(
					"username"		=>	Input::get("username"),
					"account_type"	=>	Input::get("user_type"),
					"password"		=>	Hash::make(Input::get("password")),
					"email"			=>	Input::get("email"),
					"rec_email"		=>	Input::get("rec_email")
				)
			);
		return Redirect::to("user")->with("success","Successfully created User ".Input::get("username"));
	}
	public function update()
	{
		$usr=User::where("id","=",(Input::get("userID")))->first();
		// dd($usr);
		$userExist=User::where("username","=",Input::get("username"))
						->where("id","!=",Input::get("userID"))->first();
		if($userExist)
			return Redirect::to("user")->with("error","Duplicate::username");
		$usr->account_type=Input::get("account_type");
		if(Input::get("password"))
			$usr->password=Hash::make(Input::get("password"));
		$usr->username=Input::get("username");
		$usr->email=Input::get("email");
		$usr->rec_email=Input::get("rec_email");
		$msg=($usr->save())?"successfully updated":"error";
		return Redirect::to("user")->with("success",$msg);

	}
	public function delete($id)
	{
		User::find($id)->delete();
		return Redirect::to("user")->with("success","deleted");

	}
	public function uploadUser()
	{
		$uploadID=Input::get("uploadID");
		$up=Itune::select("itunes.*","ui.email")
					->leftJoin("upload_itunes as ui",function($join) use($uploadID){
						$join->on("ui.itunes_id","=","itunes.id");
						$join->on("ui.upload_id","=",DB::raw($uploadID));
					})->get();
					
		$data=array(
			"usr" => User::where("id","=",$uploadID)->first(),
			"up" => $up
			);
		return View::make("user.upload",$data);
	}
	public function changeUploadUSer()
	{
		$uploadID=Input::get("uploadID");
		$itunesID=Input::get("itunesID");
		$email=Input::get("email");
		$user=UploadItunes::where("upload_id","=",$uploadID)
						->where("itunes_id","=",$itunesID)
						->first();
		if($user)//if record exists then update
		{
			$user->email=$email;
			$flag=$user->save();
		}
		else 
		{
			$flag=UploadItunes::create(
				array(
					"upload_id" 	=> $uploadID,
					"itunes_id"		=> $itunesID,
					"email"			=> $email
					)
				);
		}
		return var_dump($flag);
	}
	public function backup()
	{
		$name="senPort_db_".date("M-Y-d_h_i_s_a")."__.sql.gz";
		$path="/var/www/$name";
		$command="mysqldump -u root -p123456 averex_senPort | gzip > ".$path;
		echo $command;
		system($command);
	}

}