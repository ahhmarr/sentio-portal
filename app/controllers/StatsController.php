<?php
use Carbon\Carbon;
class StatsController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	public  function __construct()
	{
		$this->beforefilter('auth');
	}
	public function index()
	{
		$up=$this->totalUploaded();
		$rup=$this->totalUploaded(98);
		$timeDetails=$this->dateList();
		list($thisMonth,$thisYear,$prevMonth,$prevYear,$weekStart,$weekEnd,
			$lastWeekStart,$lastWeekEnd)=$timeDetails;
		Debugbar::addMessage($up[0]);
		$filter=Apps::filter(DB::table("app")->first())["FilterParams"];
		$progress=$this->progress();
		$data=array(
			'progress'		=> $progress,
			'cProgress'		=> $this->currentlyProgress(),
			'ut'			=> Auth::user()->account_type,
			'devFilter'		=> 0,
			'hid'			=> "",
			'root'			=> [],
			'rootFilter'	=> "",
			'account'		=> [],
			'filterR'		=> false,
			'GrAs'			=> false,
			'GrCom'			=> false,
			'AsA'			=> false,
			'AsCom'			=> false,
			'devFilterV'	=> false,
			'list'			=> $this->status(),
			'listH'			=> $this->getTotalStatusInHistory(),
			'status'		=> $this->statusArray(),
			'uploadH' 		=> $up[0],
			'uploadUsers' 	=> $up[1],
			'username' 		=> $up[2],
			'ruploadH' 		=> $rup[0],
			'ruploadUsers' 	=> $rup[1],
			'rusername' 	=> $rup[2],
			'weekStart'		=> $weekStart,
			'weekEnd'		=> $weekEnd,
			'lastWeekEnd'	=> $lastWeekEnd,
			'lastWeekStart'	=> $lastWeekStart,
			'thisMonth'		=> $thisMonth,
			'thisYear'		=> $thisYear,
			'prevMonth'		=> $prevMonth,
			'prevYear'		=> $prevYear,
			'timeDetails'	=> $timeDetails
				);
		$data=array_merge($data,$filter);
		return View::make('stats.index')->with($data);

	}
	function dateList()
	{
		$date=new DateTime();
		$today=strtolower($date->format("D"));
		//week days calculation
		$last=($today!="sun")?'last':'';
		$thisWeekFormat=new DateTime("$last sunday 0:0:0");
		$lastWeekStart=clone $thisWeekFormat;
		$lastWeekStart->sub(new DateInterval("P7D"));
		$lastWeekStart=$lastWeekStart->format("Y-m-d h:i:s A");
		$weekStart=$thisWeekFormat->format("Y-m-d h:i:s A");
		$thisWeekFormat=new DateTime("sat 23:59:59");
		$lastWeekEnd=clone $thisWeekFormat;
		$lastWeekEnd->sub(new DateInterval("P7D"));
		$lastWeekEnd=$lastWeekEnd->format("Y-m-d h:i:s A");
		$weekEnd=$thisWeekFormat->format("Y-m-d h:i:s A");

		
		$prevDate=new DateTime("first day of last month");
		$arr=array();
		$month=$date->format("m");
		$year=$date->format("Y");
		$prevMonth=$prevDate->format("m");
		$prevYear=$prevDate->format("Y");
		
		return array(
			$month,$year,$prevMonth,$prevYear,$weekStart,$weekEnd,$lastWeekStart,$lastWeekEnd
			);
	}
	protected function status()
	{
		
		list($month,$year,$prevMonth,$prevYear,$weekStart,$weekEnd,
			$lastWeekStart,$lastWeekEnd)=$this->dateList();
		$arr=array();
		
		$prevMonth=date("m",strtotime("first day of last month"));
		/*Note::v1.12 count  uploaded and 
		reuploaded apps only when they are assign to a user*/
		$allTime=Apps::select(DB::raw("count(id) as 'allTime'"),
						"app_status as status")
						->whereRaw("(((app_status='3' or app_status='19') 
										and assign_to!=0)
									or (app_status!='3' and app_status!='19'))")
						->groupBy("app_status")
						->get();
		foreach ($allTime as $ap) {
			$arr[$ap->status]["allTime"]=$ap->allTime;
		}
		//adding upload statuses too
		$upload_statuses=UploadStatuses::all();
		foreach($upload_statuses as $up)
		{
			$c=Apps::select(DB::raw("count(id) as ustatus"),"app_status as status")
						->where(function($query)
						{
							$query->where("app_status",3)
									->orWhere("app_status",19);
						})
						->where("upload_status",$up->id)
						->where("assign_to","!=",0)
						->first();
			$arr["us_".$up->id]["allTime"]=$c->ustatus;
			// upload queue
			$c=Apps::select(DB::raw("count(id) as x_ustatus"),"app_status as status")
						->where("app_status",3)
						->where("upload_status",$up->id)
						->where("assign_to","!=",0)
						->first();
			$arr["us_".$up->id]["allTime_upload"]=$c->x_ustatus;

			// Re upload queue keep in mind assign too is mandatory
			$d=Apps::select(DB::raw("count(id) as ustatus"),"app_status as status")
						->where("app_status",19)
						->where("upload_status",$up->id)
						->where("assign_to","!=",0)
						->first();
			$arr["us_".$up->id]["allTime_reupload"]=$d->ustatus;
		}
		$thisMonth=Apps::select(DB::raw("count(id) as 'thisMonth'"),"app_status as status")
						->where(DB::raw("YEAR(status_updated_at)"),"=",$year)
						->where(DB::raw("MONTH(status_updated_at)"),"=",$month)
						->groupBy("app_status")
						->get();
		foreach ($thisMonth as $ap) {
			$arr[$ap->status]["thisMonth"]=$ap->thisMonth;
		}
		$lastMonth=Apps::select(DB::raw("count(id) as 'lastMonth'"),"app_status as status")
						->where(DB::raw("YEAR(status_updated_at)"),"=",$prevYear)
						->where(DB::raw("MONTH(status_updated_at)"),"=",$prevMonth)
						->groupBy("app_status")
						->get();
		foreach ($lastMonth as $ap) {
			$arr[$ap->status]["lastMonth"]=$ap->lastMonth;
		}
		$thisWeek=Apps::select(DB::raw("count(id) as 'thisWeek'"),"app_status as status")
						->where(DB::raw("(status_updated_at)"),">=",$weekStart)
						->where(DB::raw("(status_updated_at)"),"<=",$weekEnd)
						->groupBy("app_status")
						->get();
		foreach ($thisWeek as $ap) {
			$arr[$ap->status]["thisWeek"]=$ap->thisWeek;
		}
		$lastWeek=Apps::select(DB::raw("count(id) as 'lastWeek'"),"app_status as status")
						->where(DB::raw("(status_updated_at)"),">=",$lastWeekStart)
						->where(DB::raw("(status_updated_at)"),"<=",$lastWeekEnd)
						->groupBy("app_status")
						->get();
		foreach ($lastWeek as $ap) {
			$arr[$ap->status]["lastWeek"]=$ap->lastWeek;

		}
		// last week ends
		
		//99 for uploaded begins
			$allTime=AppStatusHistory::select(DB::raw("count(id) as 'allTime'"))
						->where('status','=',99)
						->first();
			$thisMonth=AppStatusHistory::select(DB::raw("count(id) as 'thisMonth'"))
						->where(DB::raw("YEAR(created_at)"),"=",$year)
						->where(DB::raw("MONTH(created_at)"),"=",$month)
						->where('status','=',99)
						->first();
			$thisWeek=AppStatusHistory::select(DB::raw("count(id) as 'thisWeek'"))
						->where(DB::raw("(created_at)"),">=",$weekStart)
						->where(DB::raw("(created_at)"),"<=",$weekEnd)
						->where('status','=',99)
						->first();
			$lastWeek=AppStatusHistory::select(DB::raw("count(id) as 'lastWeek'"))
						->where(DB::raw("(created_at)"),">=",$lastWeekStart)
						->where(DB::raw("(created_at)"),"<=",$lastWeekEnd)
						->where('status','=',99)
						->first();
			$lastMonth=AppStatusHistory::select(DB::raw("count(id) as 'lastMonth'"))
						->where(DB::raw("YEAR(created_at)"),"=",$prevYear)
						->where(DB::raw("MONTH(created_at)"),"=",$prevMonth)
						->where('status','=',99)
						->first();
			$arr["99"]["allTime"]=$allTime->allTime;
			$arr["99"]["thisMonth"]=$thisMonth->thisMonth;
			$arr["99"]["lastMonth"]=$lastMonth->lastMonth;
			$arr["99"]["thisWeek"]=$thisWeek->thisWeek;
			$arr["99"]["lastWeek"]=$lastWeek->lastWeek;

		//for upload ends
			//98 for reuploaded begins
			$allTime=AppStatusHistory::select(DB::raw("count(id) as 'allTime'"))
						->where('status','=',98)
						->first();
			$thisMonth=AppStatusHistory::select(DB::raw("count(id) as 'thisMonth'"))
						->where(DB::raw("YEAR(created_at)"),"=",$year)
						->where(DB::raw("MONTH(created_at)"),"=",$month)
						->where('status','=',98)
						->first();
			$thisWeek=AppStatusHistory::select(DB::raw("count(id) as 'thisWeek'"))
						->where(DB::raw("(created_at)"),">=",$weekStart)
						->where(DB::raw("(created_at)"),"<=",$weekEnd)
						->where('status','=',98)
						->first();
			$lastWeek=AppStatusHistory::select(DB::raw("count(id) as 'lastWeek'"))
						->where(DB::raw("(created_at)"),">=",$lastWeekStart)
						->where(DB::raw("(created_at)"),"<=",$lastWeekEnd)
						->where('status','=',98)
						->first();
			$lastMonth=AppStatusHistory::select(DB::raw("count(id) as 'lastMonth'"))
						->where(DB::raw("YEAR(created_at)"),"=",$prevYear)
						->where(DB::raw("MONTH(created_at)"),"=",$prevMonth)
						->where('status','=',98)
						->first();
			$arr["98"]["allTime"]=$allTime->allTime;
			$arr["98"]["thisMonth"]=$thisMonth->thisMonth;
			$arr["98"]["lastMonth"]=$lastMonth->lastMonth;
			$arr["98"]["thisWeek"]=$thisWeek->thisWeek;
			$arr["98"]["lastWeek"]=$lastWeek->lastWeek;

		//for reupload ends

			//20 for approved aso fix begins
			$allTime=AppStatusHistory::select(DB::raw("count(id) as 'allTime'"))
						->where('status','=',20)
						->first();
			$thisMonth=AppStatusHistory::select(DB::raw("count(id) as 'thisMonth'"))
						->where(DB::raw("YEAR(created_at)"),"=",$year)
						->where(DB::raw("MONTH(created_at)"),"=",$month)
						->where('status','=',20)
						->first();
			$thisWeek=AppStatusHistory::select(DB::raw("count(id) as 'thisWeek'"))
						->where(DB::raw("(created_at)"),">=",$weekStart)
						->where(DB::raw("(created_at)"),"<=",$weekEnd)
						->where('status','=',20)
						->first();
			$lastWeek=AppStatusHistory::select(DB::raw("count(id) as 'lastWeek'"))
						->where(DB::raw("(created_at)"),">=",$lastWeekStart)
						->where(DB::raw("(created_at)"),"<=",$lastWeekEnd)
						->where('status','=',20)
						->first();
			$lastMonth=AppStatusHistory::select(DB::raw("count(id) as 'lastMonth'"))
						->where(DB::raw("YEAR(created_at)"),"=",$prevYear)
						->where(DB::raw("MONTH(created_at)"),"=",$prevMonth)
						->where('status','=',20)
						->first();
			

		//for approved asofix ends

			
		return $arr;
	}
	public function getTotalStatusInHistory()
	{
		list($month,$year,$prevMonth,$prevYear,$weekStart,$weekEnd,
			$lastWeekStart,$lastWeekEnd)=$this->dateList();
		$statuses=Status::all()->toArray();
		$statuses[]=["status"=>"Uploaded","order"=>99];
		$statuses[]=["status"=>"Re Uploaded","order"=>98];
		$arr=[];
		foreach($statuses as $status){
			$allTime=AppStatusHistory::select(DB::raw("count(id) as 'allTime'"))
						->where('status','=',$status["order"])
						->first();
			$thisMonth=AppStatusHistory::select(DB::raw("count(id) as 'thisMonth'"))
						->where(DB::raw("YEAR(created_at)"),"=",$year)
						->where(DB::raw("MONTH(created_at)"),"=",$month)
						->where('status','=',$status["order"])
						->first();
			$thisWeek=AppStatusHistory::select(DB::raw("count(id) as 'thisWeek'"))
						->where(DB::raw("(created_at)"),">=",$weekStart)
						->where(DB::raw("(created_at)"),"<=",$weekEnd)
						->where('status','=',$status["order"])
						->first();
			$lastWeek=AppStatusHistory::select(DB::raw("count(id) as 'lastWeek'"))
						->where(DB::raw("(created_at)"),">=",$lastWeekStart)
						->where(DB::raw("(created_at)"),"<=",$lastWeekEnd)
						->where('status','=',$status["order"])
						->first();
			$lastMonth=AppStatusHistory::select(DB::raw("count(id) as 'lastMonth'"))
						->where(DB::raw("YEAR(created_at)"),"=",$prevYear)
						->where(DB::raw("MONTH(created_at)"),"=",$prevMonth)
						->where('status','=',$status["order"])
						->first();
			$arr[$status["order"]]["allTime"]=$allTime->allTime;
			$arr[$status["order"]]["thisMonth"]=$thisMonth->thisMonth;
			$arr[$status["order"]]["lastMonth"]=$lastMonth->lastMonth;
			$arr[$status["order"]]["thisWeek"]=$thisWeek->thisWeek;
			$arr[$status["order"]]["lastWeek"]=$lastWeek->lastWeek;
		}
		return $arr;
	}
	function statusArray()
	{
		$arr=array();
		$status=Status::get();
		foreach ($status as $st) {
			$arr[$st->order]=$st->status;
		}
		$us=UploadStatuses::all();
		foreach ($us as $u) {
			$arr["us_".$u->id]=$u->upload_status;
		}
		return $arr;
	}
	function totalUploaded($stc=99)
	{
		//total uploaded by user count starts
		list($thisMonth,$thisYear,$prevMonth,$prevYear,$weekStart,$weekEnd,
			$lastWeekStart,$lastWeekEnd)=$this->dateList();
		//total uploaded by user ends
		// this month starts
		$usr=User::select(DB::raw("user.id as 'hoka_this_month',count(distinct ah.app_id) as 'total',user.id as 'id' "))
				->leftJoin("app_status_history as ah","ah.user_id","=","user.id")
				->where("user.account_type","2")
				->where("ah.status",$stc)
				->where(DB::raw("YEAR(ah.created_at)"),"=",$thisYear)
				->where(DB::raw("MONTH(ah.created_at)"),"=",$thisMonth)
				->groupBy("user.id")
				->get();
		$uploadUser=array();
		$uploadUser["total"]["thisMonth"]=0;
		foreach($usr as $r)
		{
			$uploadUser["total"]["thisMonth"]+=$r->total;
			$uploadUser[$r->id]["thisMonth"]=$r->total;
		}
		// this month ends

		// today starts
		$usr=User::select(DB::raw("count(distinct ah.app_id) as 'total',user.id as 'id' "))
				->leftJoin("app_status_history as ah","ah.user_id","=","user.id")
				->where("user.account_type","2")
				->where("ah.status",$stc)
				->where(DB::raw("date(ah.created_at)"),"=",DB::raw("date(NOW())"))
				->groupBy("user.id")
				->get();
		$uploadUser["total"]["today"]=0;
		foreach($usr as $r)
		{

			$uploadUser["total"]["today"]+=$r->total;
			$uploadUser[$r->id]["today"]=$r->total;
		}
		// today ends


		// prev month starts
		$usr=User::select(DB::raw("user.id as 'hoka_last_month',count(distinct ah.app_id) as 'lastMonth',user.id as 'id' "))
				->leftJoin("app_status_history as ah","ah.user_id","=","user.id")
				->where("user.account_type","2")
				->where("ah.status",$stc)
				->where(DB::raw("YEAR(ah.created_at)"),"=",$prevYear)
				->where(DB::raw("MONTH(ah.created_at)"),"=",$prevMonth)
				->groupBy("user.id")
				->get();
		
		$uploadUser["total"]["prevMonth"]=0;
		foreach($usr as $r)
		{
			$uploadUser["total"]["prevMonth"]+=$r->lastMonth;
			$uploadUser[$r->id]["prevMonth"]=$r->lastMonth;
		}
		// prev month ends

		// this week starts
		$usr=User::select(DB::raw("user.id as 'hoka_this_week',count(distinct ah.app_id) as 'total',user.id as 'id' "))
				->leftJoin("app_status_history as ah","ah.user_id","=","user.id")
				->where("user.account_type","2")
				->where("ah.status",$stc)
				->where(DB::raw("(ah.created_at)"),">=",$weekStart)
				->where(DB::raw("(ah.created_at)"),"<=",$weekEnd)
				->groupBy("user.id")
				->get();
		
		$uploadUser["total"]["thisWeek"]=0;
		foreach($usr as $r)
		{
			$uploadUser["total"]["thisWeek"]+=$r->total;
			$uploadUser[$r->id]["thisWeek"]=$r->total;
		}
		// this week ends

		// last week starts
		$usr=User::select(DB::raw("user.id as 'hoka_last_week',count(distinct ah.app_id) as 'total',user.id as 'id' "))
				->leftJoin("app_status_history as ah","ah.user_id","=","user.id")
				->where("user.account_type","2")
				->where("ah.status",$stc)
				->where(DB::raw("(ah.created_at)"),">=",$lastWeekStart)
				->where(DB::raw("(ah.created_at)"),"<=",$lastWeekEnd)
				->groupBy("user.id")
				->get();
		
		$uploadUser["total"]["lastWeek"]=0;
		foreach($usr as $r)
		{
			$uploadUser["total"]["lastWeek"]+=$r->total;
			$uploadUser[$r->id]["lastWeek"]=$r->total;
		}
		// last week ends

		// all time starts
		$usr=User::select(DB::raw("count(distinct ah.app_id) as 'total',user.id as 'id',user.username as 'name' "))
				->leftJoin("app_status_history as ah","ah.user_id","=","user.id")
				->where("user.account_type","2")
				->where("ah.status",$stc)
				->groupBy("user.id")
				->get();
		$allTimeUploadUser=$username=array();
		$uploadUser["total"]["allTime"]=0;
		foreach($usr as $r)
		{
			$uploadUser["total"]["allTime"]+=$r->total;
			$uploadUser[$r->id]["allTime"]=$r->total;
			$allTimeUploadUser[]=$r->id;
			$username[$r->id]=$r->name;
		}
		// all time ends

		return array($uploadUser,$allTimeUploadUser,$username);
	}
	function progress()
	{
		$progress=$this->returnStatus();
		return $progress;
		
	}
	protected function returnStatus()
	{
		$progesses=Progress::orderBy("order","ASC")->get();
		$prog=[];
		foreach($progesses as $progress)
		{
			$from=($progress->from_status==99)?"Uploaded":
					Status::whereOrder($progress->from_status)->first()->status;
			$to=($progress->to_status==99)?"Uploaded":
					Status::whereOrder($progress->to_status)
					->first()->status;
			extract($this->timeframe($progress->from_status,$progress->to_status));
			$prog[]=[
				"from" => $from,
				"to" => $to,
				"today" => $today,
				"thisWeek" => $thisWeek,
				"lastWeek" => $lastWeek,
				"thisMonth" => $thisMonth,
				"lastMonth"=>$lastMonth,
				"lastTwoMonths" => $lastTwoMonths
			];
		}
		return $prog;
	}
	public function timeframe($from_status,$to_status)
	{
	    $t=date('Y-m-d');
		$today=$this->calculateTimelyProgress($from_status,$to_status,
			"date(created_at)='$t'");
		$thisMonth=$this->calculateTimelyProgress($from_status,$to_status,
			"Month(created_at)=Month(NOW()) and Year(created_at)=Year(Now())");
		$lastMonth=$this->calculateTimelyProgress($from_status,$to_status,
			"MONTH(CURRENT_DATE - INTERVAL 1 MONTH) and  
			Year(created_at)= YEAR(CURRENT_DATE - INTERVAL 1 MONTH)");
		$thisWeek=$this->calculateTimelyProgress($from_status,$to_status,
			"YEARWEEK(created_at)=YEARWEEK(NOW())");
		$lastWeek=$this->calculateTimelyProgress($from_status,$to_status,
			"YEARWEEK (created_at) = YEARWEEK( CURRENT_DATE -INTERVAL 1 WEEK ) ");
		$lastTwoMonths=$this->calculateTimelyProgress($from_status,$to_status,
			"month(created_at)=MONTH(CURRENT_DATE - INTERVAL 2 MONTH) and  
			Year(created_at)= YEAR(CURRENT_DATE - INTERVAL 2 MONTH)");
		return compact('today','thisMonth','lastMonth','thisWeek',
			'lastWeek','lastTwoMonths');
	}

	protected function calculateTimelyProgress($from_status,$to_status,$from,$to=null)
	{
		// counting apps with specified statuses in th given time frame
		$transitions=AppStatusHistory::where(function($query) 
			use ($to_status)
		{
			$query->where("status",$to_status)
				  ->orWhere("text_history",$to_status);	
		})->whereRaw($from)->get();
		$count=0;
		foreach ($transitions as $transition) 
		{
			$is=$this->getPrevious($transition->app_id,
					$from_status,$transition->id,$from);
			if($is)
			{
				$count++;
			}
		}
		return $count;
	}
	protected function getPrevious($appID,$status,$id,$raw)
	{
		$flag=false;
		$aph=AppStatusHistory::whereAppId($appID)
							->where("id","<",$id)
							->where(function($query) use($status)
							{
								$query->where("status",$status)
									  ->orWhere("text_history",$status);
							})
							->orderBy("created_at","DESC")
							->count();
		if($aph)
		{
			$flag=true;
		}
		return $flag;
	}
	public function currentlyProgress()
	{
		$currentlyProgress=[];
		foreach(["Graphics Complete","ASO Complete"] as $value){
			$currentlyProgress[$value]=
				$this->calculateCurrentlyProgress($value);
		}
		return $currentlyProgress;
	}
	public function calculateCurrentlyProgress($status)
	{
		$timeQueries=$this->timeQueries();
		$progress=[];
		foreach($timeQueries as $time=>$raw){
			$progress[$time]=AppStatusHistory::where(function($query) 
				use ($raw,$status)
			{
				$query->where("status",$status)
						->orWhere("text_history",$status);
			})->whereRaw($raw)->count();
		}
		return $progress;
	}
	protected function timeQueries(){
		return [
			"today"			=> "date(created_at)=DATE(NOW())",
			"thisMonth"		=> "Month(created_at)=Month(NOW()) and 
								Year(created_at)=Year(Now())",
			"lastMonth"		=> "MONTH(CURRENT_DATE - INTERVAL 1 MONTH) and  
								Year(created_at)= 
								YEAR(CURRENT_DATE - INTERVAL 1 MONTH)",
			"lastTwoMonths" => "month(created_at)=
								MONTH(CURRENT_DATE - INTERVAL 2 MONTH) and  
								Year(created_at)= 
								YEAR(CURRENT_DATE - INTERVAL 2 MONTH)",
			"thisWeek"		=> "YEARWEEK(created_at)=YEARWEEK(NOW())",
			"lastWeek"		=> "YEARWEEK (created_at) = 
								YEARWEEK( CURRENT_DATE -INTERVAL 1 WEEK ) "
		];
	}

}