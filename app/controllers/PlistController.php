<?php

class PlistController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /plist
	 *
	 * @return Response
	 */
	public function index()
	{
		$geo=GeoIP::getlocation();
		// return Config::get("senport.plist_log_path");
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /plist/create
	 *
	 * @return Response
	 */
	public function create()
	{

	}

	/**
	 * Store a newly created resource in storage.
	 * POST /plist
	 *
	 * @return Response
	 */
	public function store()
	{
		$name=Input::get("name");
		$geo=GeoIP::getlocation();
		$ip=$geo["ip"];
		$user=Auth::user()->id;
		$location=$geo["city"]." ".$geo["country"];
		$info=serialize($geo);
		$plist=Plist::create([
				"file_name"	=> $name,
				"user_id"	=> $user,
				"ip"		=> $ip,
				"location"	=> $location,
				"info"	=> $info,
			]);
		$logFile=Config::get("senport.plist_log_path");
		$content=$plist->created_at->toDateTimeString()
					."\t".str_pad($plist->user->username,15," ")
					." \t".str_pad($plist->file_name,30," ")
					."\t".str_pad($plist->ip,20," ")."\t".str_pad($plist->location,40," ")
					."\n";
		File::prepend($logFile,$content);
		// dd(Auth::user()->id);
		return $plist->user->username;
	}

	/**
	 * Display the specified resource.
	 * GET /plist/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /plist/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /plist/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /plist/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}