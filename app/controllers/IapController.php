<?php 
class IapController extends BaseController{
	
	public  function __construct()
	{
		$this->beforefilter('auth');
	}

	public function index()
	{
		
	}
	public function save()
	{	
		//dd('hello');
		$iapType=Input::get('iapType');
		$iapProdID=Input::get("prod2");
		$iapPriceTier=Input::get("iapPriceTier");
		$iapDisplayName=Input::get("iapDisplayName");
		$iapDescription=Input::get("iapDescription");
		$projectID=Input::get("projectID");
		
		Iap::create(array(
			'project_id'=>$projectID,
			'description'=>$iapDescription,
			'display_name'=>$iapDisplayName,
			'product_id'=>$iapProdID,
			'price_tier'=>$iapPriceTier,
			'type'=>$iapType
			));
		
		return Redirect::to("root/$projectID")->with('message','successfully added');
	}
	public function view()
	{

	}
	public function delete($id,$projID)
	{
		// dd($id);
		Iap::find($id)->delete();
		return Redirect::to("root/$projID")->with('message','successfully deleted');
	}
	public function edit()
	{

	}
	public function changeIAP()
	{
		$id=Input::get("id");
		$iap=Input::get("iap");
		$v=($iap)?1:0;
		$r=RootProject::find($id)->get();

		$r->iap=$v;
		$r->save();
		return 'iap='.$r->iap;
		



	}
}
