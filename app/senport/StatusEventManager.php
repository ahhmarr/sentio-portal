<?php namespace Senportal;
use Log;
use Apps;
use File;
use Config;
class StatusEventManager{
	
	public function copyOrRemovePlist($status,$ap){
		// copy when status changed to reupload metadata
		/*check whether plist exists if yes then 
		get the itunes it and save it with tht filename*/
		$newFileName=Config::get("senport.plist_path")."/".
								$ap->apple_id.".plist";
		if($status==1){
			// copy the file witht the name <apple_id>.plist
			
			
			$file_path=$ap->rootProject->plist_path;
			if(!$file_path || !$ap->apple_id)
			{
				Log::info("skipped copying plist as apple 
					id or plist file missing");
				return;
			}
			$copy=File::copy($file_path,$newFileName);
		}
		elseif($status==5){
			File::delete($newFileName);
		}
	}
}
