<?php namespace Senportal;
use Log;
use User;
use Mail;
use Apps;
use Status;
class AppAction{

	//status id 
	//user type
	//user id

	// protected statusChangeConfig;
	protected $mailFlag;
	public function __construct()
	{
		// add app and reupload metadata
		$this->itunesArray=[0.1,1];
		// pending assignment,waiting for upload,testflight,
		$this->manager=[16,2,"Itunes Account"];
		$this->manager_upload=[3];
		// integrate,upload,
		$this->upload_user=[1,4];
		// upload : graphic bug 
		$this->art_upload=[2];
		// approved,approved aso fix 
		//v1.12 Graphics Complete -21
		$this->aso=[20,21,"graphics complete"];
		$this->AdminEmail=$this->getAdminEmail();
		$this->setMailFlag(true);
	}
	public function setMailFlag($flag=true)
	{
		$this->mailFlag=$flag;
	}
	protected function getAdminEmail()
	{
		
		$users=User::whereAccountType(1)->get();
		$email=[];
		foreach($users as $user)
		{
			if($user->rec_email && $user->email)
				$email[]=$user->email;	
		}
		return $email;
	}
	
	public function sendEmailOnStatusChange($status,$appID,$upload_status)
	{
		// dd("status $status upload status $upload_status");
		$email_status="";$email="";
		if($status==3 || $status==19)
		{
			$email_status=\UploadStatuses::find($upload_status)
											->upload_status;
			if(array_search($upload_status, $this->manager_upload)!==false)
			{
				// var_dump("send message to manager upload ");
				$users=User::whereAccountType(7)->get();
				foreach($users as $user)
				{
					if($user->rec_email && $user->email)
						$email[]=$user->email;	
				}
			}
			else if(array_search($upload_status, $this->upload_user)!==false)
			{
				// var_dump("send message to upload user for upload ");
				$upload_user=Apps::find($appID)->upload_user;
			
				$upload_user=User::find($upload_user);
				
				// var_dump("uplaod user : $upload_user->username upload status : $email_status");
				if($upload_user)
				{
					if($upload_user->rec_email && $upload_user->email)
					{
						$email=$upload_user->email;
						// var_dump("sending mail ti $email for $email_status");
					}
				}
			}
			else if(array_search($upload_status, $this->art_upload)!==false)
			{
				// var_dump("send message to art user upload ");
				
				$arts=User::whereAccountType(6)->get();
				foreach($arts as $art)
				{
					if($art->rec_email && $art->email)
						$email[]=$art->email;
				}
			}
		}
		else if(array_search($status, $this->itunesArray)!==false)
		{
			// var_dump("send mail to itunes user");
			$email_status=Status::whereOrder($status)->first()->status;
			$itunes=User::whereAccountType(3)->get();
			foreach($itunes as $itune_user)
			{
				if($itune_user->rec_email && $itune_user->email)
					$email[]=$itune_user->email;
			}
		}
		else if(array_search($status, $this->manager)!==false)
		{
			// var_dump("send mail to manager for normal status user");
			/*v1.12 sending mail to string status as well
			ie itnunes account created*/
			$email_status=(is_int($status))?Status::whereOrder($status)
									->first()->status:$status;
			$users=User::whereAccountType(7)->get();
			foreach($users as $user)
			{
				if($user->rec_email && $user->email)
					$email[]=$user->email;
			}
		}
		else if(array_search($status, $this->aso)!==false)
		{
			// var_dump("send mail to aso user for normal status");
			/*
			v1.12 sending mail to statuses which are not in the 
			statuses table ex: sending mail on graphics complete 
			status change
			*/
			$email_status=((int)$status!=0)?Status::whereOrder($status)
								->first()->status:$status;
			$aso_users=User::whereAccountType(5)->get();
			foreach($aso_users as $aso_user)
			{
				if($aso_user->rec_email && $aso_user->email)
					$email[]=$aso_user->email;
			}
		}
		if(($email || $this->AdminEmail) && $email_status)
		{
			$this->sendEmail($email,$email_status,$this->AdminEmail);
			//sending admin emails too
		}
	}
	protected function sendMailToAdmin($msg){
		if(is_array($this->AdminEmail) && $this->AdminEmail)
		{
			$emails=$this->AdminEmail;
			Mail::queue("emails.plainMail",compact("msg"),function($message) use($emails,$msg)
			{
				$message->to($emails)->subject("Sentioportal:".$msg);
			});
		}
	}
	protected function sendEmail($email,$email_status,$cc=false)
	{
		if(!$this->mailFlag)
		{
			Log::info("Mail flag set to false");
		}
		if(is_array($email))
		{
			foreach($email as $e)
			{
				$this->sendEmail($e,$email_status,$cc);
			}
		}
		else
		{	if(strtolower($email_status)=="upload")
				$this->sendMailToAdmin("App Uploaded");
			Mail::queue("emails.eventsEmail",compact("email_status"),
					function($message) use($email,$cc,$email_status)
			{
				// var_dump("sendinf mail $email");
				$mail=($email)?$message->to($email):$message;
				// cc all the emails sent to admin users if they want to rec email
				// v1.2.1 #10 skipping ccing 'new upload task' to admin users 
				$mail=($cc && $email_status!="upload")?$mail->cc($cc):$mail;
				$mail=$mail->subject("Sentioportal : Task");
			});
		}
	}
	
}