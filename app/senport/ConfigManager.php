<?php namespace Senportal;

class ConfigManager{

	public function getConfigValues($id=1)
	{
		return (\Conf::findOrFail($id));
	}	
	public function changeConfigurations($col,$value,$id=1)
	{
		/*TO-DO : we can check if the values is an array and iterate 
		it through for changing multiple config col values*/
		if($col)
		{
			$conf=\Conf::findOrFail($id);
			$conf->$col=$value;
			$conf->save();
		}
	}
}