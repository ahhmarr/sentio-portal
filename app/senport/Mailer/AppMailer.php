<?php namespace Senportal\Mailer;

class AppMailer {

	protected $sendMail;
	public function __construct()
	{
		$this->sendMail();
	}
	public function sendMail($flag=true)
	{
		$this->sendMail=$flag;
	}
	public function maxStatusAccountWise()
	{
	 	$itune=\Input::get("redAccount");
	 	if(!$itune)
	 		return false;
	 	$account=\Itune::find($itune)->account;
	 	$this->sendMailToAdmin($account);
	 	return true;
	}
	public function sendMailToAdmin($account)
	{
		$users=\User::select("email")
					->whereAccountType(1)
					->whereRecEmail(1)
					->whereNotNull("email")
					->get();
		$userEmails=[];
		foreach($users as $user)
		{
			$userEmails[]=$user->email;
		}
		if(!$userEmails || !$this->sendMail)
			return;
		\Mail::queue('mail.acRed', compact('account'), function($message) use($userEmails)
		{
		    $message->to($userEmails)
		    		->subject('Sentio Portal: App Overflow problem');
		});
	}
}