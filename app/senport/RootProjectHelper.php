<?php namespace Senportal;

use ProjectCell;
use DB;

class RootProjectHelper{

	function __construct(){
	}

	function saveDefaultCellValues($projectID)
	{
		$cells=$this->getCellValues();
		$insert=[];
		foreach($cells as $row=>$colArray)
		{
			foreach($colArray as $col=>$value)
			{
				$insert[]=[
					"project_id"	=> $projectID,
					"cell_row"		=> $row,
					"cell_col"		=> $col,
					"value"			=> $value["value"],
					"style"			=> "color:".$value["color"]
				];
			}
		}
		DB::table("project_cells")->insert($insert);
	}
	function getCellValues(){
		$arr=[];
		$arr[1][1]=['value'=>'iTunes Account','color'=>'#F00'];
		$arr[1][2]=['value'=>'$UPLOADER_EMAIL ','color'=>'#F00'];
		$arr[1][3]=['value'=>'$ITUNES_ACCOUNT','color'=>'#F00'];
		$arr[2][1]=['value'=>'App Name','color'=>'#001EFF'];
		$arr[2][2]=['value'=>'FREE','color'=>''];
		$arr[2][3]=['value'=>'$APPNAME_FREE','color'=>''];
		$arr[4][1]=['value'=>'Bundle ID','color'=>'#001EFF'];
		$arr[4][2]=['value'=>'FREE','color'=>''];
		$arr[4][3]=['value'=>'<obtain from iTunes Connect>',
		'color'=>''];
		return $arr;
	}
}
