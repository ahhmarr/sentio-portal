<?php 
class UserTypeTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
        $data = array(
        				array("name"=>"admin","type"=>1),
                		array("name"=>"itunes","type"=>3),
                		array("name"=>"upload","type"=>2)
                	);
		DB::table('user_type')->insert($data);
	}

}