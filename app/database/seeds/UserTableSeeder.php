<?php 
class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
        $data = array
        (
        	'username' => 'admin',
         	'password' =>Hash::make('123456'),
         	'name'     => 'johny bravo',
         	'account_type' => 1
         );
		DB::table('user')->insert($data);
	}

}