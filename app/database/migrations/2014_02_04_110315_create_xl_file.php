<?php

use Illuminate\Database\Migrations\Migration;

class CreateXlFile extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("xl_file",function($table)
		{
			$table->increments("id");
			$table->string("file_name");
			$table->timestamps();
			$table->softDeletes();
		});				
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("xl_file");
	}

}