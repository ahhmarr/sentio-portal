<?php

use Illuminate\Database\Migrations\Migration;

class AddFreeSdkAndProSdkToAppTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table("app",function($table)
		{
			$table->integer("free_sdk")->nullable();
			$table->integer("pro_sdk")->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table("app",function($table)
		{
			$table->dropColumn("free_sdk");
			$table->dropColumn("pro_sdk");
		});
	}

}