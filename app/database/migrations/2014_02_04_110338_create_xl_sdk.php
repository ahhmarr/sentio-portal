<?php

use Illuminate\Database\Migrations\Migration;

class CreateXlSdk extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("xl_sdk",function($table)
		{
			$table->increments("id");
			$table->string("sdk_name");
			$table->integer("import_file_id");
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("xl_sdk");
	}

}