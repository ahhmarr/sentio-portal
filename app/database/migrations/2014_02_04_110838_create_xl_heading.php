<?php

use Illuminate\Database\Migrations\Migration;

class CreateXlHeading extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("xl_heading",function($table)
		{
			$table->increments("id");
			$table->string("header_name");
			$table->integer("import_file_id");
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("xl_heading");
	}

}