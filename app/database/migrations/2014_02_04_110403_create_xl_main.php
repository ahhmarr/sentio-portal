<?php

use Illuminate\Database\Migrations\Migration;

class CreateXlMain extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("xl_main",function($table)
		{
			$table->increments("id");
			$table->integer("sdk_id");
			$table->integer("root_project_id");
			$table->integer("file_import_id");
			$table->integer("heading_id");
			$table->string("value");
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("xl_main");
	}

}