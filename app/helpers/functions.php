<?php 
use Carbon\Carbon;
function last_modified($a)
{
	$upd=new DateTime($a->updated_at);
	$now=new DateTime();
	$diff=$now->diff($upd);
	$weeks=round($diff->days/7);
	$color="";
	if($diff->days>=48)
		$color="#EF2121";
	elseif($diff->days>=14)
		$color="#FB764A";
	return '<span style="color:'.$color.'">'.$upd->format("d/m/Y").'</span>';
}
function sh($v)
{
	$return="";
	if(isset($v))
	{
		if($v)
		{
			$return=$v;
		}
	}
	return $return;
}
function printStatsCells($list,$index,$subindex,$timeDetails)
{
	list($thisMonth,$thisYear,$prevMonth,$prevYear,$weekStart,$weekEnd,
			$lastWeekStart,$lastWeekEnd)=$timeDetails;
	$arr=[
	"thisWeek"=>["history-status"=>$index,"upload-from"=>$weekStart,"upload-to"=>$weekEnd],
	"lastWeek"=>["history-status"=>$index,"upload-from"=>$lastWeekStart,"upload-to"=>$lastWeekEnd],
	"thisMonth"=>["history-status"=>$index,"upload-month"=>$thisMonth,"upload-year"=>$thisYear],
	"lastMonth"=>["history-status"=>$index,"upload-month"=>$prevMonth,"upload-year"=>$prevYear],
	"allTime"=>["history-status"=>$index]
	];
	$fix="";
	if($index==21) //if Approved
		{ $fix.=" ( fix : ".printStatsCells($list,20,$subindex,$timeDetails)." )"; }
	if(isset($list[$index][$subindex]))
	{
		$data=" ";
		foreach($arr[$subindex] as $key=>$value)
		{
			$data.=" data-$key='$value' ";
		}
		
		$span="<span $data >".
				"<span class=''>".$list[$index][$subindex]."</span>".$fix
				."</span>";
		return $span;
	}
	else
		return "0".$fix;
}
function el($arr,$index,$subindex)
{
	if(isset($arr[$index][$subindex]))
		return "<span class='filterClass'>".$arr[$index][$subindex]."</span>";
	else
		return 0;
}