<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	if(Auth::user())
		return Redirect::to("/app");
	else
		return View::make('login');
});
Route::get('home', function()
{
	return View::make('home');
});
Route::get('dashboard','HomeController@showWelcome');
Route::get('backup','UsersController@backup');
Route::get('itunes','itunesController@index');
Route::post('itunes/fullToggle','itunesController@toggle');
Route::get('root','RootsController@index');
Route::get('root/{id}','RootsController@index');
Route::post('itunes/save','itunesController@save');
Route::post('root/save','RootsController@save');
Route::post('root/changeIAP','RootsController@changeIAP');
Route::post('root/toggle','RootsController@toggle');
Route::post('root/changeGame','RootsController@changeGame');
Route::post('root/changeFreePro','RootsController@changeFreePro');
Route::get('root/disableProject/{id}','RootsController@disable');
Route::post('root/ChangeProjectName','RootsController@editName');
Route::post('root/updateRootText','RootsController@updateRootText');
Route::post('iap/save','IapController@save');
Route::get('iap/delete/{id}/{projID}','IapController@delete');
Route::post('root/rowCols','RootsController@rowCols');
Route::post('root/saveCell','RootsController@saveCell');
Route::post('root/updateFields/{id}','RootsController@updateFields');
Route::post('root/uploadPlist/{id}','RootsController@uploadPlist');
Route::post('root/removePlist/{id}','RootsController@removePlist');
Route::post('login','LoginController@index');
Route::get('logout','LoginController@logout');
Route::get('backup','LoginController@backup');
Route::get('itunes/rem/{id}','itunesController@delete');
Route::get('app','appsController@index');
Route::post('app/ajax','appsController@index');
Route::post('app/updateAppStats','appsController@updateAppStats');
Route::post('app/exportToXl','appsController@exportToXl');
Route::post('app/exportToXlSDK','appsController@exportToXlSDK');
Route::post('app/updateKeys','appsController@updateKeys');
Route::post('app','appsController@save');
Route::post('app/filter','appsController@index');
Route::post('app/view','appsController@view');
Route::post('app/changeAppDetails','appsController@changeApp');
Route::post('app/changeReportCell','appsController@changeReportCell');
Route::post('app/update','appsController@update');
Route::post('app/assign','appsController@assign');
Route::post('app/changeStatus','appsController@changeStatus');
Route::post('app/project','appsController@project');
Route::post('app/reportText','appsController@reportText');
Route::post('app/appValid','appsController@appValid');
Route::post('app/assignIT','appsController@assignIT');
Route::post('app/gFixed','appsController@gFixed');
Route::get('app/delIap/{id}','appsController@delIap');
Route::get('app/del/{id}','appsController@del');
Route::get('app/complete/{id}','appsController@complete');
Route::post('app/unassign','appsController@unassign');
Route::get('app/inComplete/{id}','appsController@inComplete');
Route::get('app/statusList','appsController@statusList');
Route::get('app/statusMax','appsController@checkMaxAppStatus');
Route::get('stats','StatsController@index');
//user Routes
Route::get('user','UsersController@index');
Route::post('user/create','UsersController@create');
Route::post('user/update','UsersController@update');
Route::get('user/delete/{id}','UsersController@delete');
Route::post('user/uploadUser','UsersController@uploadUser');
Route::post('user/changeUploadUSer','UsersController@changeUploadUSer');
//report variable route
Route::get('rep','ReportController@index');
Route::post('rep/save','ReportController@save');
Route::get('rep/delete/{id}','ReportController@delete');

// Route::resource('status', 'StatusesController');
Route::post("status/store","StatusesController@store");
Route::get("status/delete/{id}","StatusesController@delete");
Route::get('sdk',"SdkController@index");
Route::post('sdk/import',"SdkController@import");
Route::post('sdk/updateXL',"SdkController@updateXL");
Route::post('sdk/rpXL',"SdkController@rpXL");
Route::get('sdk/read',"SdkController@readData");
Route::get("isLive","StatusesController@isLive");
Route::get("sendMail","StatusesController@sendMail");

Route::post('status/filter',"StatusesController@filter");
Route::post('app/appInfoUpdate',"appsController@updateKeys");
Route::get("pdf/{id}","appsController@pdf");
Route::group(["before"=>"auth"],function()
{
	Route::get("/config",["as"=>"config","uses"=>"ConfigController@index"]);
	Route::put("/config/{id}",["as"=>"configChange",
		"uses"=>"ConfigController@update"]);
	Route::resource("plists","PlistController");
});