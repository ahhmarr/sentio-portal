module.exports=function(grunt)
{
	grunt.initConfig({
		watch : {
			all : {
				files : ['**/*.php','**/*.js','**/*.css',],
				options : {
					livereload :true
				}
			}
		}
	});
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.registerTask('default',['watch']);
};
