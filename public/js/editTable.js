$(function() {
    $("#mainTable").on("click", "#editButton", function() {
        var th = $(this);
        var appName = th.data("appname");
        var appid = th.data("appid");
        var rpID = th.data("rpid");
        var itID = th.data("itid");
        var prty = th.data("prty");
        var asignTo = th.data("asignto");
        var appleID = th.data("appleid");
        var itURL = th.data("iturl");
        var pappleID = th.data("pappleid");
        var pitURL = th.data("piturl");
        var freenpro = th.data("freenpro");
        var autoprocess = th.data("autoprocess");
        if (freenpro == 1) {
            $(".pro-show").show();
        } else {
            $(".pro-show").hide();
        }
        $("#editEditName").html("Edit " + appName);
        $("#editAppID").val(appid);
        $("#editAppName").val(appName);
        $("#editRP").val(rpID);
        $("#editIT").val(itID);
        $("#editPrty").val(prty);
        $("#editAT").val(asignTo);
        $("#editAppleID").val(appleID);
        $("#editItunesURL").val(itURL);
        $("#editPappleID").val(pappleID);
        $("#editPitunesURL").val(pitURL);
        if (autoprocess)
            $("#editAutoProcessY").prop("checked", true);
        else
            $("#editAutoProcessN").prop("checked", true);
        $("#editAutoProcess").val(autoprocess);
        $("#editModal").modal("show");
    });
})