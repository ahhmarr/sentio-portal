	$(function(){
		$("#toggleForm").click(function()
		{
			$("#formProject").slideToggle();
		});
		$(".gameCenter").click(function()
		{
			val=($(".gameCenter:checked").val());
			url='changeGame';
			data={
				flag	   : val,
				projectID  : $("#rootReportTable").attr('data-projectID')
			};
			console.log(url);
			console.log(data);
			$.ajax({method:'post',data:data,url:url,success:function(resp){
				console.log(resp);
				
			}});
		})
		$(".iapType").focus(function()
		{
			typeHTML='<select name="iapType" class="form-control" id="iapType"><option value="0">Consumable</option><option value="1">Non Consumable</select>';
			$(this).parent().html(typeHTML);
			$("#iapType").focus();
		});
		$(".iapPriceTier").focus(function()
		{
			type='<select name="iapPriceTier" class="form-control" id="priceT">';
			option='';
			for(x=1;x<=100;x++)
			{
				option+='<option value="'+x+'">'+x+'</option>';
			}
			type+=option;
			type+='</select>';
			$(this).parent().html(type);
			$("#priceT").focus();
		});
		$("#iapY,#iapN").click(function()
		{
			// alert($("#iapY").attr('checked'));
			changeIAP($("#iapY").is(':checked')?1:0);
			if($("#iapY").is(':checked'))
				{
					$(".iap-rows").show();

				}
			else
				{
					$(".iap-rows").hide();
				}
		});
		$("input[name='freePro']").click(function()
		{
			changeFreePro($("#freeProY").is(':checked')?1:0);
		})
		// bundlePrefix='{{$bundlePrefix}}';
		$(".iapProdID").click(function()
		{
			
			bundHTML='';
			// bundHTML='<select name="prod1" id="prod1" class="form-control">'+bundlePrefix+'</select>';
			iapName='<input name="prod2" id="prod2" placeholder="iap name" class="axBlank form-control" />';
			wrap='<div class="col-xs-6">'+bundHTML+iapName+'</div>';
			$(this).parent().html(wrap);
			$("#prod2").focus();
		});
	});
	$(function(){
		$("#rootReportTable").on('click','.col-add',function()
		{
			 c=$(this).parent();
            ind=(c.index()); //col index
            console.log(ind);
            html='<td>{{$textBox}}</td>';
            html2=c.html();
            cls=c.attr('class');
            $("#rootReportTable tbody tr td:nth-child("+(ind+1)+")").after(html);
            $("#rootReportTable tbody tr:nth-child(1) td:nth-child("+(ind+2)+")").attr('class',cls).html(html2);
            updateRowCols({
			            	obj:$(this),
			            	addr:getCellAddress($(this).parent())
           				 });

			
		});
		$("#rootReportTable").on('click','.col-remove',function()
		{
			c=$(this).parent();
            ind=(c.index()); 
            adrs=getCellAddress($(this).parent());
           $("#rootReportTable tbody tr td:nth-child("+(ind+1)+")").remove();
           updateRowCols({
			            	obj:$(this),
			            	addr:adrs
           				 });
			
		});
		$("#rootReportTable").on('click','.row-add',function()
		{
			c=$(this).parent();
            col=(c.index());
            colHTML=c.html();
            colClass=c.attr('class');
            html='<tr>';
            for(x=1;x<=col;x++)
            {
            	html+='<td>{{$textBox}}</td>';
            }
            html+='<td class="'+colClass+'">'+colHTML+'</td>';
            html+='</tr>';
            console.log(col);
            tr=(c.parent().index()); //row index
            $("#rootReportTable tbody tr:nth-child("+tr+")").after(html);
			updateRowCols({
			            	obj:$(this),
			            	addr:getCellAddress($(this).parent())});
		});
		$("#rootReportTable").on('click','.row-remove',function()
		{
			adrs=getCellAddress($(this).parent());
            $(this).parent().parent().slideToggle('slow').remove();
            updateRowCols({
			            	obj:$(this),
			            	addr:adrs
           				 });
		});
	});
changeIAP=function(flag)
    {
    	url='changeIAP';
    	data={iap:flag,id:$("#rootReportTable").attr('data-projectID')};
    	console.log(url);
    	console.log(data);
    	$.ajax({method:'post',data:data,url:url,success:function(resp){
    		console.log(resp);
    		
    	}});
    }
    changeFreePro=function(flag)
    {
    	url='changeFreePro';
    	data={iap:flag,id:$("#rootReportTable").attr('data-projectID')};
    	console.log(url);
    	console.log(data);
    	$.ajax({method:'post',data:data,url:url,success:function(resp){
    		console.log(resp);
    		
    	}});
    }
	 trnc=function()
    {
    	tr=$("#rootReportTable tbody tr").length-1;
    	td=$("#rootReportTable tbody tr:nth-child(1) td").length-1;
    	
    	console.log('no of rows'+tr);
    	console.log('no of cols'+td);
    	table={'rows':tr,'cols':td};
    	return table;
    }
    updateRowCols=function(d)
    {
    	c=d.obj;
    	adr=d.addr;
    	t=trnc();
    	url='rowCols';
    	projID=$("#rootReportTable").attr('data-projectID');
    	data={	
    			rows		: 	t.rows,
    			cols		: 	t.cols,
    			projID		: 	projID,
    			row_index	: 	adr.row,
    			col_index	: 	adr.col,
    			action		: 	c.attr('class')
    		};
    	console.log(url);
    	console.log(data);
    	$.ajax({method:'post',data:data,url:url,success:function(resp){
    		console.log(resp);
    	}});
    }
    getCellAddress=function(th)
    {
    	address={col:th.index()+1,row:th.parent().index()};
    	return address;
    }
    $(function(){
    	//saving text data
    	$("#rootReportTable").on('change','.cell-box',function()
    	{
    		
    		r=getCellAddress($(this).parent());
    		cell={
    			projID : $("#rootReportTable").attr('data-projectID'),
    			row    : r.row,
    			col    : r.col,
    			style  : $(this).attr('style'),
    			value  : $(this).val() 	
    		};
    		url='saveCell';
    		data=cell;
    		console.log(url);
    		console.log(data);
    		$.ajax({method:'post',data:data,url:url,success:function(resp){
    			console.log(resp);
    			
    		}});
    	})
    });
    $(function(){
    	$(".editName").click(function()
    	{
    		$(this).parent().next().slideToggle();
    	})
    });
// plist upload handling
$(function(){
   $("#plist").change(function(e)
   {

       //validate plist extension
       if(!$(this).val())
        return;
       var file=e.target.files[0];
       var ext=(file.name.split(".").pop());
       if(ext!="plist")
       {
            alertify.error("only plist files are allowed");
            $(this).val('');
            return;
       }
       //upload it to the server
       var formdata=new FormData();
       var pID=$(this).data("pid");
       formdata.append("plist",file);
       url='uploadPlist/'+pID;
       data=formdata;
       console.log(url);
       console.log(data);
       $.ajax({method:'post',data:data,url:url,
        processData:false,
        contentType:false,
        beforeSuccess:function()
        {
          $("#loading").removeClass("invisible");
        },
        success:function(resp){
            $("#loading").addClass("invisible");
           var path=resp.path;
          console.log("path"+path);
        $("#plist").fadeOut().addClass("hidden-xx");
          $("#downloadPlist").html(resp.name).
                        attr("href",resp.path);
            $("#downloadLink").fadeIn().removeClass("hidden-xx");
       }});
       //click make it downloadable
       
       //remove when click on delete
   });
   $("#removePlist").click(function()
   {
      var pID=$(this).data("pid");
      url='removePlist/'+pID;
      data={
        id : pID
      };
      console.log(url);
      console.log(data);
      $.ajax({method:'post',data:data,url:url,success:function(resp){
        $("#plist").fadeIn().removeClass("hidden-xx").val('');
          $("#downloadLink").addClass("hidden-xx").fadeOut();
      }});
   });
   // logging while download
   $("#downloadPlist").click(function(e)
   {
     // e.preventDefault();
     // logg
     var th=$(this);
     url='/plists';
     data={
      name : $(this).html()
     };
     console.log(url);
     console.log(data);
     $.ajax({method:'post',data:data,url:url,success:function(resp){
       console.log(resp);
       // window.location = th.attr("href");
     }});
   });
});