$(function() {
    $("#mainTable").on("click", ".cloneButton", function() {
        var th = $(this);
        var appName = th.data("appname");
        var rpID = th.data("rpid");
        var itID = th.data("itid");
        var prty = th.data("prty");
        var asignTo = th.data("asignto");
        var appleID = th.data("appleid");
        var itURL = th.data("iturl");
        var pappleID = th.data("pappleid");
        var pitURL = th.data("piturl");
        var freenpro = th.data("freenpro");
        var autoprocess = th.data("autoprocess");
        var appID = th.data("appid");
        if (freenpro == 1) {
            $(".pro-show").show();
        } else {
            $(".pro-show").hide();
        }
        $("#cloneEditName").html("Clone " + appName);
        $("#cloneAppName").val(appName);
        $("#cloneRP").val(rpID);
        $("#cloneIT").val(itID);
        $("#clonePrty").val(prty);
        $("#cloneAT").val(asignTo);
        $("#cloneAppleID").val(appleID);
        $("#cloneItunesURL").val(itURL);
        $("#clonePappleID").val(pappleID);
        $("#clonePitunesURL").val(pitURL);
        $("#cloneAppID").val(appID);
        if (autoprocess)
            $("#cloneAutoProcessY").prop("checked", true);
        else
            $("#cloneAutoProcessN").prop("checked", true);
        $("#cloneAutoProcess").val(autoprocess);
        $("#cloneModal").modal("show");
    });
})