$(function() {
    $("#loadMore").click(function() {
        $quer = getFilter();
        $quer = $quer + '&isAjax=1';
        url = 'app/ajax';
        data = $quer;
        console.log(data);
        $.ajax({
            method: 'post',
            data: data,
            url: url,
            success: function(resp) {
                $("table tbody").append(resp).trigger("footable_redraw");
            }
        });
    });
});
//for manual searching field
$(function() {
    $("#watchFace").keyup(function() {
        var searchText = $("#watchFace").val();
        if(searchText)
            $("#paginate").hide();
        else
            $("#paginate").show();
        $quer = getFilter();
        $quer = $quer + '&isAjax=1' + '&key=' + searchText;
        url = 'app/ajax';
        data = $quer;
        console.log($quer);
        $.ajax({
            method: 'post',
            data: data,
            url: url,
            beforeSend: function() {
                $("#loadingImageKey").removeClass("invisible");
            },
            success: function(resp) {
                $("#loadingImageKey").addClass("invisible");
                resp=$.trim(resp);
                console.log(resp);
                if(resp)
                    {    
                        $("#mainTable tbody").html(resp);
                    }

            }
        });

    });
});
var getFilter = function(extra) {
    extra=extra || "";
    $quer = $("#filterFOrm").serialize();
    res = $quer.split("&");
    res[0] = "";
    $quer = res.join("&");
    var th =$("#paginate");
    var offset = Number(th.attr("data-offset"));
    var limit = Number(th.attr("data-limit"));
    $quer = $quer + '&isAjax=1' + '&offset=' + offset + '&limit=' + limit;
    return $quer+extra;
};
$(function() {
    var fetchRecords=function($quer,th)
    {
        if(th)
            offs(th);
        // $quer = getFilter();
        url = 'app/ajax';
        data = $quer;
        console.log(url);
        console.log(data);
        $.ajax({
            method: 'post',
            data: data,
            url: url,
            beforeSend: function() {
                $("#loadingImage").show();
            },
            success: function(resp) {
                $("#loadingImage").hide();
                resp=$.trim(resp);
                if(!resp)
                    return;
                console.log("updated");
                $("#mainTable tbody").html(resp);
            }
        });
    };
    $("#pageNext,#pagePrev").click(function() {
        offs($(this));
        fetchRecords(getFilter());
    });
    var offs=function(th)
    {
        //setting offset according to next prev
        var thID=th.attr("id");
        var page=$("#paginate");
        var offset=Number(page.attr("data-offset"));
        var limit=Number(page.attr("data-limit"));
        var max=Number(page.attr("data-max"));
        if(thID=="pageNext")
            offset += limit;
        else if(thID=="pagePrev")
            offset-=limit
        if(offset<0)
            offset=0;
        console.log(" offset  : "+offset);
        console.log(" limit  : "+limit);
        var currentPage=Math.round(offset/limit)+1;
        $("#currentPage").html(currentPage);
        if(currentPage<=max && currentPage>0)
            $("#paginate").attr("data-offset",offset);
        if(currentPage>=max)
            $("#pageNext").attr("disabled","disabled");
        else
            $("#pageNext").removeAttr("disabled","disabled");
        if(currentPage<=1)
            $("#pagePrev").attr("disabled","disabled");
        else
            $("#pagePrev").removeAttr("disabled","disabled");
    };
    var currentPage = function(no) {
        var current = $("#currentPage").html();
        current = Number(current);
        current += no;
        if (current <= 0)
            current = 1;
        $("#currentPage").html(current);
    };
    // sorting columns
    $(".sort").click(function()
    {
        var th=$(this);
        var order=th.attr("data-order");
        var newOrder=(order=="asc")?"desc":"asc";
        var col=th.data("col");
        var max=$("#paginate").data("max");
        $quer=getFilter();
        $quer+="&sort=1&col="+col+"&oder="+order;
        offset=0;
        $("#fSort").val(1);
        $("#sortCol").val(col);
        $("#sortOrder").val(order);
        $("#paginate").attr("data-offset",offset);
        //sort image in the heading
        $(".sorting_asc,.sorting_desc").removeClass("sorting_asc sorting_desc");
        th.attr("data-order",newOrder);
        th.addClass("sorting_"+order);
        fetchRecords(getFilter());
        // reset offset to 0 if asc or to max-30(limit) id descending
        //make a request
        //change the sorting icon
        // test
        //add proper cols to the th of maintable 
    });
    //click on assign to me in ituns dashboard
    $("#assignToCheck").click(function()
        {
            var extra="";
            if($(this).prop("checked"))
            {
                $("#status_filter").val("0.1|1");
                $("#assign_to_check").val("1");
                $("#paginate").hide();
            }
            else
            {
                $("#paginate").show();
                $("#status_filter").val("");
                $("#assign_to_check").val("0");
            }
            $quer=getFilter(extra);
            fetchRecords($quer);
    });
});
$(function(){
    $("body").on("click",".update_app_stats",function()
    {

        var th=$(this);
        var name=th.data("name");
        var appID=th.data("appid");    
        var val=th.data("val");
        var reload=th.data("reload");
        var status=th.data("status");
        var noconfirm=th.data("noconfirm");
        var assignTo=th.data("assignto");
        var clicked=th.attr("data-clicked");
        if(clicked)
        {
            return;
        }
        th.attr("data-clicked","1"); //once clicked so no duplicates
        if(!noconfirm)
        {
            if(!confirm("Are you Sure?"))
                return;
        }
        url='app/updateAppStats';
           data={
            name : name,
            appID : appID,
            val : val,
            status: status,
            assignTo:assignTo
           };
           console.log(url);
           console.log(data);
           var th=$(this);
           $.ajax({method:'post',beforeSend:function(){
            th.append("<img src='img/loading/3.gif' height='20' />");
           },data:data,url:url,success:function(resp){
               console.log(resp);
               // if(reload)
                $("#customFilter3").click();
                $("#filterFOrm").submit();              
           }});   
    });
});
